<?php

/** mega admin routes **/

Route::get('/', 'DashboardController@index')->name('home');
Route::resource('/team', 'TeamController');
Route::get('/video', 'VideoController@index')->name('video.index');
Route::post('/video','VideoController@store')->name('video.store');
Route::resource('/contact', 'ContactController');
Route::resource('/referal', 'ReferalController');
Route::resource('/tasks', 'TasksController');
Route::post('/tasks/activate/{id}', 'TasksController@activate')->name('tasks.activate');
Route::resource('/reviews', 'ReviewsController');
Route::get('/users','UsersController@index')->name('users.index');
Route::get('/users/{id}','UsersController@tasks')->name('users.tasks');
Route::get('/users/{id}/payments', 'UsersController@payments')->name('users.payments');
Route::get('/users/{id}/task/{task_id}','UsersController@task')->name('users.task');
Route::post('/users/{id}/task/{task_id}','UsersController@task_update')->name('users.task_update');
Route::resource('/schedule', 'ScheduleController');
Route::resource('/withdraws', 'WithdrawController');
Route::resource('/online', 'OnlineController');
