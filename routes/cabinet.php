<?php

/** cabinet routes **/

Route::get('/', 'ProfileController@index')->name('index');

Route::get('/team', 'TeamController@index')->name('team');

Route::get('/game', 'MarafonController@index')->name('game');
Route::get('/game/rules', 'MarafonController@rules')->name('game.rules');
Route::get('/game/schedule', 'MarafonController@schedule')->name('game.schedule');
Route::get('/game/tasks', 'MarafonController@tasks')->name('game.tasks');
Route::get('/game/tasks/{id}', 'MarafonController@task_show')->name('game.task_show');
Route::post('/game/tasks/{id}', 'MarafonController@task_store')->name('game.task_store');

Route::get('/bonus', 'BonusController@index')->name('bonus');

Route::get('/online', 'OnlineController@index')->name('online');

Route::get('/referal', 'ReferalController@index')->name('referal');
Route::post('/referal', 'ReferalController@promoChange')->name('referal.change');
Route::post('/referal/amount', 'ReferalController@requestPayment')->name('referal.amount');

Route::get('/balance', 'BalanceController@index')->name('balance');

Route::get('/contact', 'ContactController@index')->name('contact');

Route::get('/settings', 'SettingsController@index')->name('settings.index');
Route::post('/settings/reset', 'SettingsController@reset')->name('settings.reset');
Route::post('/settings/avatar', 'SettingsController@avatar')->name('settings.avatar');

Route::get('/otzivi', 'OtziviController@index')->name('otzivi');
