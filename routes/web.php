<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login_post');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Route::match(['GET', 'POST'], '/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/agreement', 'LandingsController@agreement')->name('agreement');
Route::get('/politics', 'LandingsController@politics')->name('politics');
Route::get('/otkaz', 'LandingsController@otkaz')->name('otkaz');

Route::group([
    'middleware' => ['marks']
], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/online', 'HomeController@trening')->name('trening');
    Route::get('/webinar','HomeController@webinar')->name('webinar');
    Route::post('/webinar','HomeController@webinar_post')->name('webinar.post');
	
    Route::get('/webinar1','HomeController@webinar1')->name('webinar1');
    Route::get('/webinar2','HomeController@webinar2')->name('webinar2');
    Route::post('/webinar1','HomeController@webinar_post')->name('webinar1.post');
	
    Route::get('/thanks','HomeController@webinar_oto')->name('webinar.oto');
    Route::post('/thanks','HomeController@webinar_oto_post')->name('webinar.oto_post');
    Route::post('/thanks','HomeController@webinar_oto_post')->name('webinar.oto_post');
    Route::post('/webinar/register','HomeController@webinar_register')->name('webinar.register');
    Route::get('/webinar/sale','HomeController@webinar_sale')->name('webinar.sale');
    Route::post('/webinar/sale','HomeController@webinar_sale_reg')->name('webinar.sale_reg');
    Route::post('/club_register','HomeController@club_register')->name('club_register');
    Route::get('/fitness', 'HomeController@fitness')->name('fitness');
    Route::get('/fitness/{link}', 'ReferalController@fitness')->name('fitness_ref');
    Route::get('/checkout/{id}', 'HomeController@checkout')->name('checkout');
    Route::get('/trening/{link}', 'ReferalController@trening')->name('trening_ref');

    Route::get('coaching', 'LandingsController@coaching')->name('coaching.index');
    Route::get('coaching/register', 'LandingsController@coaching_reg_show')->name('coaching.reg_show');
    Route::post('coaching/register', 'LandingsController@coaching_register')->name('coaching.register');

    Route::get('/av-2/sale','HomeController@av2_sale');
    Route::post('/av-2/sale','HomeController@av2_sale_reg')->name('av2_sale');

    Route::get('/av-3/sale', 'LandingsController@av3_sale');
    Route::post('/av-3/sale', 'LandingsController@av3_sale_reg')->name('av3_sale');
    Route::get('/av-4/sale', 'LandingsController@av4_sale');
    Route::post('/av-4/sale', 'LandingsController@av4_sale_reg')->name('av4_sale');

});

Route::get('/test/{id}', 'TestController@index');

//for founds
Route::get('/payment', 'PaymentsController@get')->name('payment.index');
Route::post('/payment', 'PaymentsController@post')->name('payment.check');
Route::get('/payment/fail', 'PaymentsController@fail')->name('payment.fail');
Route::get('/payment/ok', 'PaymentsController@ok')->name('payment.ok');
Route::get('/payment/wait', 'PaymentsController@wait')->name('payment.wait');
