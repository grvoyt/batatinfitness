(function($) {

    if(!getCookie()) {
        $(document).on('mouseleave', handleModal);
    }

    function handleModal(e) {
        if (e.clientY < 10) {
            $("#endmodal").fadeIn("fast", function() {
                writeCookie();
                $(document).off('mouseleave', handleModal);
            });
        }
    }


    function getCookie() {
        var name = 'shown';
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : 0;
    }

    function writeCookie() {
        // функция записывает cookie на 1 день, с которой мы не показываем окно
        var date = new Date;
        date.setDate(date.getDate() + 1);
        document.cookie = "shown=no; path=/; expires=" + date.toUTCString();
    }

    $(document).on('click','.model_clode', function() {
        $("#endmodal").fadeOut("fast");
    })


})(jQuery);
