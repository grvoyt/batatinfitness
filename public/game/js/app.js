(function($) {
	$(document).ready(function() {

        $('.try_reg').on('click', function(e) {
            e.preventDefault();
            var form = $('.form_this');
                $('#form_this_tarif').val( $(this).data('tarif') );
                form[0].submit();
        })

		$('.video_act1').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});

		$(document).on('click', '.video_act', function(e) {
			e.preventDefault();
			var youtube_id = youtube_parser( $(this).attr('href') );
			$('#video_show').find('iframe').attr('src', 'https://www.youtube.com/embed/'+youtube_id+'?rel=0');
			$('#video_show').fadeIn();
		});

		$('#video_show').on('click', function(e) {
			if(e.target.nodeName !== "IFRAME") {
				$('#video_show').fadeOut();
				$('#video_show').find('iframe').attr('src', '');
			}

		})  ;

		$(document).on('click','.video_show_clode', function(e) {
			$('#video_show').fadeOut();
			$('#video_show').find('iframe').attr('src','');
		});

		$(".scrollto").on('click', function (e) {
	        e.preventDefault();
	        var id  = $(this).attr('href');
	        $('body,html').animate({scrollTop: $(id).offset().top - 20}, 300);
	    });

		$(document).on('submit','#main_form',function(e) {
			e.preventDefault();
			var form = $(this);
			var email = form.find('[type=email]').val();

			if( form.find('[name=name]').val() == '' || form.find('[name=name]').val() < 4 ) {
				alert('Введите имя');
				return false;
			}

			if( email == '' || !validateEmail(email)) {
				alert('Введите правильный email')
				return false;
			}

			form[0].submit();
		});

		if($(window).width() <= 500) {
            $('#whats').html( $('#whats').text().split(' ').join('<br>') );
        }
	});

	window.validateEmail = function (email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	window.youtube_parser = function (url) {
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match&&match[7].length==11)? match[7] : false;
	}

})(jQuery);
