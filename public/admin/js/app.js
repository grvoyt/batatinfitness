(function($) {
    $(document).ready(function(e) {
        $(document).on('click', '.dropdown-click', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $(this).parent().find('.dropdown').slideToggle();
        });

        $(document).on('click','.menu_button', function(e) {
            $('menu').toggleClass('show');
            $('.content_body').toggleClass('slide');
        })
    });

})(jQuery);
