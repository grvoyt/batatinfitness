(function() {
    var current_date;
    var target_date = new Date().getTime() + (1000*60*30); // СѓСЃС‚Р°РЅРѕРІРёС‚СЊ РґР°С‚Сѓ РѕР±СЂР°С‚РЅРѕРіРѕ РѕС‚СЃС‡РµС‚Р°
    if(!$.cookie('target_date')) $.cookie('target_date',target_date, { expires: 1 });
    var days, hours, minutes, seconds; // РїРµСЂРµРјРµРЅРЅС‹Рµ РґР»СЏ РµРґРёРЅРёС† РІСЂРµРјРµРЅРё

    getCountdown();

    window.timer = setInterval(function () { getCountdown(); }, 1000);

    function getCountdown(){

        current_date = new Date().getTime();

        var target_date =  parseInt( $.cookie('target_date') );
        if( (target_date - current_date) <= 0 ) {
            clearInterval(window.timer);
            return false;
        }

        var seconds_left = (target_date - current_date) / 1000;

        days = pad( parseInt(seconds_left / 86400) );
        seconds_left = seconds_left % 86400;

        hours = pad( parseInt(seconds_left / 3600) );
        seconds_left = seconds_left % 3600;

        minutes = pad( parseInt(seconds_left / 60) );
        seconds = pad( parseInt( seconds_left % 60 ) );

        // СЃС‚СЂРѕРєР° РѕР±СЂР°С‚РЅРѕРіРѕ РѕС‚СЃС‡РµС‚Р°  + Р·РЅР°С‡РµРЅРёРµ С‚РµРіР°
        var html = [];
        if(hours == '00') {
            $('#time_hour').hide();
        } else {
            $('#time_hour').find('.target').text(hours);
        }
        $('#time_minute').find('.target').text(minutes);
        $('#time_second').find('.target').text(seconds);
    }

    function pad(n) {
        return (n < 10 ? '0' : '') + n;
    }
})();