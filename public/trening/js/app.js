(function($) {
	$(document).ready(function() {
		$(document).on('click', '.video_act', function(e) {
			e.preventDefault();
			var youtube_id = youtube_parser( $(this).attr('href') );
			$('#video_show').find('iframe').attr('src', 'https://www.youtube.com/embed/'+youtube_id+'?rel=0');
			$('#video_show').fadeIn();
		});

		$('#video_show').on('click', function(e) {
			if(e.target.nodeName !== "IFRAME") {
				$('#video_show').fadeOut();
				$('#video_show').find('iframe').attr('src', '');
			}

		})  ;

		$(document).on('click','.video_show_clode', function(e) {
			$('#video_show').fadeOut();
			$('#video_show').find('iframe').attr('src','');
		});

		$(".scrollto").on('click', function (e) {
	        e.preventDefault();
	        var id  = $(this).attr('href');
	        $('body,html').animate({scrollTop: $(id).offset().top - 20}, 300);
	    });

	    $('#carousel').slick({
            infinite:true,
            arrows: true,
            dots: true,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 3,
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        dots:false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1,
                    }
                }
            ]
		});
	});

window.youtube_parser = function (url) {
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match&&match[7].length==11)? match[7] : false;
	}
})(jQuery);