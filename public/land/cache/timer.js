(function() {
    var current_date;
    var target_date = new Date().getTime() + (1000*60*5); // установить дату обратного отсчета
    var days, hours, minutes, seconds; // переменные для единиц времени

    var countdown = document.getElementById("countdown"); // получить элемент тега

    if(!countdown) return false;

    getCountdown();

    window.timer = setInterval(function () { getCountdown(); }, 1000);

    function getCountdown(){

        current_date = new Date().getTime();

        if( (target_date - current_date) == 0 ) {
            clearInterval(window.timer);
            return false;
        }

        var seconds_left = (target_date - current_date) / 1000;

        days = pad( parseInt(seconds_left / 86400) );
        seconds_left = seconds_left % 86400;

        hours = pad( parseInt(seconds_left / 3600) );
        seconds_left = seconds_left % 3600;

        minutes = pad( parseInt(seconds_left / 60) );
        seconds = pad( parseInt( seconds_left % 60 ) );

        // строка обратного отсчета  + значение тега
        var html = [];
        if(days !== '00') html.push(days);
        if(hours !== '00') html.push(hours);
        html.push(minutes);
        html.push(seconds);
        countdown.innerHTML = html.join(':');
    }

    function pad(n) {
        return (n < 10 ? '0' : '') + n;
    }
})();
