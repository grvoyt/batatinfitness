@extends('admin.layouts.app')
@section('title', "Баланс")
@section('description', "Баланс" )
@section('content')
    <h1>Различные настройки</h1>

    <section>
        @foreach($settings as $setting)
        <div class="section_item">
            <div class="section_item_wrap">
                <h5 style="margin-bottom: 15px;">{{ $keys[$setting->key]['title'] }}</h5>
                <form action="{{ route('admin.video.store') }}" method="POST" class="section_line">
                    @csrf
                    <input type="hidden" name="key" value="{{ $setting->key }}">
                    <div class="auth_form_group" style="margin-bottom: 15px;">
                        <input type="text" name="value" value="{{ $setting->value }}">
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Добавить</button>
                    </div>
                </form>
            </div>
        </div>
        @endforeach

    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
