<menu>
    <ul>
        <li>
            <a href="{{ route('admin.users.index') }}" class="{{ is_active('admin.users.index') }}">
                <i class="fas fa-user-friends"></i> Клиенты
            </a>
        </li>
        <li>
            <a href="{{ route('admin.referal.index') }}" class="{{ is_active('admin.referal.index') }}">
                <i class="fas fa-wallet"></i> Кэшбэк
            </a>
        </li>
        <li>
            <a href="{{ route('admin.withdraws.index') }}" class="{{ is_active('admin.withdraws.index') }}">
                <i class="fas fa-wallet"></i> Выводы
            </a>
        </li>
        <li>
            <a href="{{ route('admin.team.index') }}" class="{{ is_active('admin.team.index') }}">
                <i class="fas fa-user-friends"></i> Наша команда
            </a>
        </li>
        <li>
            <a href="{{ route('admin.online.index') }}" class="{{ is_active('admin.online.index') }}">
                <i class="fas fas fa-cogs"></i> Вебинары
            </a>
        </li>
        <li>
            <a href="{{ route('admin.tasks.index') }}" class="{{ is_active('admin.tasks.index') }}">
                <i class="fas fa-tasks"></i> Задания
            </a>
        </li>
        <li>
            <a href="{{ route('admin.schedule.index') }}" class="{{ is_active('admin.schedule.index') }}">
                <i class="fas fa-tasks"></i> Расписание
            </a>
        </li>
        <li>
            <a href="{{ route('admin.reviews.index') }}" class="{{ is_active('admin.reviews.index') }}">
                <i class="fas fa-comment-alt"></i> Отзывы
            </a>
        </li>
        <li>
            <a href="{{ route('admin.contact.index') }}" class="{{ is_active('admin.contact.index') }}">
                <i class="fas fas fa-cogs"></i> Контакты
            </a>
        </li>
    </ul>
</menu>
