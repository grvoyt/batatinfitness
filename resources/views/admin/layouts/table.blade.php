<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin/js/jszip.min.js') }}"></script>
<script src="{{ asset('admin/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('admin/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin/js/buttons.print.min.js') }}"></script>
<script>
    $('table').DataTable({
        order: [[0,'asc']],
        stateSave: true,
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
        ],
        pageLength: 30,
        language: {
            url: "{{ asset('/admin/js/Russian.json') }}"
        }
    }).on('init', function() {
        $('.loading').fadeOut(function() {
            $('.loading').hide();
        });
    })
</script>
