<header>
    <div class="header_left">
        @auth<button class="menu_button"><span><div></div></span><p>Меню</p></button>@endif
        <div class="header_logo">Админка</div>
    </div>
    @auth
    <div class="header_right">
        <div class="profile">
            <div class="profile_name"><i class="far fa-user"></i> {{ Auth::user()->name }}</div>
            <a href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i> {{ __('main.logout') }}</a>
        </div>
    </div>
    @endif
</header>
