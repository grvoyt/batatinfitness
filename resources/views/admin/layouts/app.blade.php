@include('admin.layouts.head')
<body>
@include('admin.layouts.header')
@include('admin.layouts.menu')
<div class="content_body">
    @yield('content')
    @include('admin.layouts.footer')
</div>
<!-- scripts -->
<script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="//unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('admin/js/jquery.nice-select.min.js') }}"></script>
@stack('js')

<script src="{{ asset('admin/js/all.js') }}?{{ time() }}"></script>
<script src="{{ asset('admin/js/app.js') }}?{{ time() }}"></script>

</body>
</html>
