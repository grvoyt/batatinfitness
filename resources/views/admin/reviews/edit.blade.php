@extends('admin.layouts.app')
@section('title', "Изменить $review->name")
@section('description', "Изменить $review->name" )
@section('content')
    <h1>Изменить {{ $review->name }}</h1>
    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.reviews.update', $review->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="name"
                               value="{{ old('name') ?? $review->name }}" required placeholder="Название">

                        @error('name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="video_id"
                               value="{{ old('video_id') ?? $review->video_id }}" required placeholder="Ссылка на Youtube">

                        @error('video_id')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Изменить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
