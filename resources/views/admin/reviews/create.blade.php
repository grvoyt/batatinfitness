@extends('admin.layouts.app')
@section('title', "Добавление в команду")
@section('description', "Добавление в команду" )
@section('content')
    <h1>Добавление в команду</h1>
    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.reviews.store') }}">
                    @csrf

                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="name"
                               value="{{ old('name') }}" required placeholder="Название">

                        @error('name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="video_id"
                               value="{{ old('video_id') }}" required placeholder="Ссылка на Youtube">

                        @error('video_id')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
