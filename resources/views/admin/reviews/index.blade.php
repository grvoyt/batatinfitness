@extends('admin.layouts.app')
@section('title', "Отзывы")
@section('description', "Отзывы" )
@section('content')
    <h1>Отзывы</h1>
    <a href="{{ route('admin.reviews.create') }}" class="button"><i class="fas fa-user-plus"></i> Добавить</a>
    <section  style="margin-top:30px">
        @foreach($reviews as $review)
            <div class="section_item">
                <div class="section_item_wrap">
                    <div class="section_with_button">
                        <h5>{{ $review->name }}</h5>
                        <div class="section_with_button--buttons">
                            <a href="{{ route('admin.reviews.edit', $review->id) }}" class="button">
                                <i class="fas fa-external-link-square-alt"></i> Изменить
                            </a>
                            <form action="{{ route('admin.reviews.destroy', $review->id) }}" method="POST">
                                @csrf
                                @method("DELETE")
                                <div class="auth_form_group">
                                    <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Удалить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
