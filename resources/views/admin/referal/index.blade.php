@extends('admin.layouts.app')
@section('title', "Кэшбэки")
@section('description', "Кэшбэки" )
@section('content')
    <div class="loading">
        <img src="{{ asset('admin/img/loading.svg') }}" alt="">
    </div>
    <h1>Кэшбэки</h1>

    <section>
        <div class="section_item">
            <div class="section_item_wrap">
                <table id="example" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Имя</th>
                        <th>Email</th>
                        <th>Всего привлек</th>
                        <th>Всего оплатили</th>
                        <th>Сумма оплат</th>
                        <th>Активация</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $counts[$user->id] ?? 0 }}</td>
                            <td>{{ $payed[$user->id]->count ?? 0 }}</td>
                            <td>{{ $payed[$user->id]->amount ?? 0 }}</td>
                            <td>
                                @if($user->is_partner)
                                    <p style="color:green">активен</p>
                                @else
                                <a style="color:red;text-decoration: underline" href="{{ route('admin.referal.show',$user->id) }}">вкл партнера</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif

    @push('js')
        @include('admin.layouts.table')
    @endpush
@endsection
