@extends('admin.layouts.app')
@section('title', "Оплаты $user->name")
@section('description', "Оплаты $user->name" )
@section('content')
    <h1>Оплаты {{ $user->name }}</h1>
    <a class="button mb30" href="{{ route('admin.users.index') }}">Назад</a>
    <section>
        @forelse($payments as $payment)
        <div class="section_item w70">
            <div class="section_item_wrap">
                <p class="mb20"><b># {{ $payment->payment_id }}</b></p>
                <p class="mb20"><b>Статус:</b>
                    <span style="color:{{ $payment->status ? 'green' : 'red' }}">
                        {{ $payment->status ? 'Оплачен' : 'Не оплачен' }}
                    </span>
                </p>
                @if($payment->amount)<p class="mb20"><b>Сумма:</b> {{ $payment->amount }} руб.</p>@endif
                <p class="mb20"><b>Дата создания:</b> {{ $payment->created_at }}</p>
                @if($payment->order_time)<p class="mb20"><b>Дата оплаты:</b> {{ $payment->order_time }}</p>@endif
                @if($payment->user_card)<p class="mb20"><b>Карта:</b> {{ $payment->user_card }}</p>@endif
                @if(!$payment->status)
                    <p class="mb20"><b>Ссылка на оплату:</b> {{ $payment->url }}</p>
                @endif
            </div>
        </div>
        @empty
            <div class="section_item w70">
                <div class="section_item_wrap">
                    <p>Нет платежей</p>
                </div>
            </div>
        @endforelse
    </section>
@endsection
