@extends('admin.layouts.app')
@section('title', "Контакты")
@section('description', "Контакты" )
@section('content')
    <h1>Добавить контакт</h1>
    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <form action="{{ route('admin.contact.store') }}" method="POST">
                    @csrf
                    <div class="auth_form_group">
                        <select name="" id="">
                            <option value="1"><p><i class="fab fa-sign-in-alt"></i></p></option>
                            <option value="2"><i class="fab fa-sign-in-alt"></i></option>
                            <option value="3"><i class="fab fa-sign-in-alt"></i></option>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
