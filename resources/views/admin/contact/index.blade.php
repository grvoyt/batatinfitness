@extends('admin.layouts.app')
@section('title', "Контакты")
@section('description', "Контакты" )
@section('content')
    <h1>Контакты</h1>
    <a href="{{ route('admin.contact.create') }}" class="button"><i class="fas fa-user-plus"></i> Добавить</a>
    <section  style="margin-top:30px">
        @foreach($contacts as $contact)
            <div class="section_item">
                <div class="section_item_wrap">
                    <i class="fab {{ $contact->icon }}"></i>
                    {{ $contact->name }}
                    {{ $contact->link }}
                    <a href="{{ route('admin.contact.edit', $contact->id) }}" class="button">Изменить</a>
                </div>
            </div>
        @endforeach
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
