@extends('admin.layouts.app')
@section('title', "Команда")
@section('description', "Команда" )
@section('content')
    <div class="loading">
        <img src="{{ asset('admin/img/loading.svg') }}" alt="">
    </div>
    <h1>Реф. подругу</h1>
    <section>
        <div class="section_item">
            <div class="section_item_wrap">
                <table id="example" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>Приведено</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->count }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif

    @push('js')
        @include('admin.layouts.table')
    @endpush
@endsection
