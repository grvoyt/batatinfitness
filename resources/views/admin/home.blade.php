@extends('admin.layouts.app')
@section('title', "Админка главная")
@section('description', "Админка главная" )
@section('content')
    <h1>Различные настройки</h1>
    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <h3>Общие показатели</h3>

                <div class="section_lined mb30">
                    <p class="section_text">Оплачено всего на:</p>
                    <div class="section_lined_right rubl">{{ $payed_sum }}</div>
                </div>
                <div class="section_lined mb30">
                    <p class="section_text">Выставлено счетов всего:</p>
                    <div class="section_lined_right ">{{ $payments_count }}</div>
                </div>
                <div class="section_lined mb40">
                    <p class="section_text">Выставлено счетов за сегодня:</p>
                    <div class="section_lined_right ">{{ $payments_count_today }}</div>
                </div>
                <div class="section_lined mb40">
                    <p class="section_text">Игроков зарегистрировано:</p>
                    <div class="section_lined_right ">{{ $users_count }}</div>
                </div>
            </div>
        </div>
    </section>
@endsection
