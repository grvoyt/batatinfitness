@extends('admin.layouts.app')
@section('title', "Команда")
@section('description', "Команда" )
@section('content')
    <h1>Команда</h1>
    <a href="{{ route('admin.team.create') }}" class="button"><i class="fas fa-user-plus"></i> Добавить</a>
    <section  style="margin-top:30px">
        @foreach($teachers as $teacher)
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="team_wrapper">
                    <div class="team_item">
                        <div class="team_item_img" style="background-image: url('{{ asset($teacher->img) }}')"></div>
                        <div class="team_item_descr">
                            <p class="title">{{ $teacher->name }}</p>
                            <p class="subtext">{{ $teacher->position }}</p>
                            <p class="subtext">{{ $teacher->description }}</p>
                            <a style="margin-top: 10px;" href="{{route('admin.team.edit', $teacher->id)}}" class="button">Изменить</a>
                            <form style="display: inline-block;" action="{{ route('admin.team.destroy',$teacher->id) }}" method="POST">
                                @csrf
                                @method("DELETE")
                                <button class="button" type="submit">Удалить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
