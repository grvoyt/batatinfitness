@extends('admin.layouts.app')
@section('title', "Добавление в команду")
@section('description', "Добавление в команду" )
@section('content')
    <h1>Добавление в команду</h1>
    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <div class="section_img js-avatar" style="background-image: url({{ asset($user->info->img ?? 'img/no-avatar.jpg') }})"></div>
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.team.store') }}">
                    @csrf
                    <input type="file" accept="image/*" name="img" id="avatar">
                    <label for="avatar"><i class="fas fa-cloud-download-alt"></i> Выбрать аватар</label>

                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="name"
                               value="{{ old('name') }}" required placeholder="Имя">

                        @error('name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="position"
                               value="{{ old('position') }}" required placeholder="Должность">

                        @error('position')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="description"
                               value="{{ old('description') }}" required placeholder="Описание">

                        @error('description')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    @push('js')
        <script>
            $('#avatar').on('change', function(e) {
                var file = $(this).prop('files')[0];
                getBase64(file).then(function(imgBase64) {
                    $('.js-avatar').attr('style','background-image: url('+imgBase64+')');
                });
            });

            function getBase64(file) {
                return new Promise((res,rej) => {
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        res(reader.result)
                    };
                    reader.onerror = function (error) {
                        rej(error);
                    };
                });
            }
        </script>

    @endpush
@endsection
