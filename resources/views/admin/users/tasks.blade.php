@extends('admin.layouts.app')
@section('title', "Клиенты")
@section('description', "Клиенты" )
@section('content')
    <h1>Задание {{ $user->name }}</h1>

    <section>
        @forelse($tasks as $task)
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="section_with_button">
                    <h4>{{ $task->info->name }}</h4>
                    <div class="section_with_button--buttons">
                        <a href="{{ route('admin.users.task', [$user->id,$task->task_id]) }}" class="button">
                            <i class="fas fa-external-link-square-alt"></i> Посмотреть
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @empty
            <div class="section_item">
                <div class="section_item_wrap">
                    <p class="section_text">Не выполненых заданий</p>
                </div>
            </div>
        @endforelse
    </section>

    @push('js')
        @if(session('success'))
            <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
        @endif
    @endpush
@endsection
