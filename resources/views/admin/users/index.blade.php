@extends('admin.layouts.app')
@section('title', "Клиенты")
@section('description', "Клиенты" )
@section('content')
    <div class="loading">
        <img src="{{ asset('admin/img/loading.svg') }}" alt="">
    </div>
    <h1>Клиенты</h1>

    <section>
        <div class="section_item">
            <div class="section_item_wrap">
                <table id="example" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Имя</th>
                        <th>email</th>
                        <th>Telegram</th>
                        <th>Ссылки</th>
                        <th>Оплаты</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $socials[$user->id]->telegram ?? '' }}</td>
                            <td>
                                <a href="{{ route('admin.users.tasks', $user->id) }}" class="button">Задания</a>
                            </td>
                            <td>
                                <a href="{{ route('admin.users.payments', $user->id) }}" class="button">Оплаты</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif

    @push('js')
        @include('admin.layouts.table')
    @endpush
@endsection
