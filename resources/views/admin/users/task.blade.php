@extends('admin.layouts.app')
@section('title', "Клиенты")
@section('description', "Клиенты" )
@section('content')
    <h1>Задание {{ $user->name }}</h1>

    <section>
        <div class="section_item">
            <div class="section_item_wrap">
                <h3>{{ $task->info->name }}</h3>
                <p class="section_text">Статус: <b>{{ $task->status ? "Одобрено" : "Не одобрено" }}</b></p>
                <p class="section_text">{{ $task->answer }}</p>
                <img style="width: 100%;margin-bottom: 20px;" src="{{ asset($task->img) }}" alt="">
                @if(!$task->status)
                <form method="POST" action="{{ route('admin.users.task_update', [$user->id,$task->task_id]) }}">
                    @csrf
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Одобрить</button>
                    </div>
                </form>
                @else
                    <a class="button" href="{{ route('admin.users.tasks',$user->id ) }}"> Назад</a>
                @endif
            </div>
        </div>
    </section>
@endsection
