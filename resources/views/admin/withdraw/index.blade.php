@extends('admin.layouts.app')
@section('title', "Клиенты")
@section('description', "Клиенты" )
@section('content')
    <div class="loading">
        <img src="{{ asset('admin/img/loading.svg') }}" alt="">
    </div>
    <h1>Выводы</h1>
    <div class="section_buttons mb20">
        <a class="button" href="{{ route('admin.withdraws.index',['show' => 'all']) }}">Показать все</a>
        <a class="button" href="{{ route('admin.withdraws.index',['show' => 'accept']) }}">Все одобренные</a>
        <a class="button" href="{{ route('admin.withdraws.index',['show' => 'reject']) }}">Все отклоненные</a>
    </div>

    <section>
        <div class="section_item">
            <div class="section_item_wrap">
                <table id="example" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Имя</th>
                        <th>Сумма</th>
                        <th>Статус</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($withdraws as $withdraw)
                        <tr>
                            <td>{{ $withdraw->id }}</td>
                            <td>{{ $withdraw->user->name }}</td>
                            <td>{{ $withdraw->amount }}</td>
                            <td>{{ $status[$withdraw->status] }}</td>
                            <td>
                                <a href="{{ route('admin.withdraws.show', $withdraw->id) }}" class="button">Посмотреть</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>



    @push('js')
        @include('admin.layouts.table')
        @if(session('success'))
            <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
        @endif
    @endpush
@endsection
