@extends('admin.layouts.app')
@section('title', "Информация о выводе №".$withdraw->id)
@section('description', "Информация о выводе №".$withdraw->id )
@section('content')
    <h1>Информация о выводе №{{ $withdraw->id }}</h1>
    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <h3>Клиент</h3>
                <p>Статус: {{ $status[$withdraw->status] }}</p>
                <p>Имя: {{ $client->name }}</p>
                <p>Email: {{ $client->email }}</p>
                <p class="mb20">Сумма: {{ $withdraw->amount }}</p>
                @if($withdraw->status > 0)
                <p class="subtext">Комментарий: {{ $withdraw->message }}</p>
                @endif
                @if($withdraw->status == 0)
                <form id="withdraw_action" method="POST" enctype="multipart/form-data" action="{{ route('admin.withdraws.update',$withdraw->id) }}">
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="withdraw_action_id" name="action">
                    <div class="auth_form_group">
                        <textarea required name="text" id="" cols="30" rows="10" placeholder="Комментарий">{{ $task->text ?? old('text') }}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="button" class="button js-formAction" data-action="1"><i class="fas fa-sign-in-alt"></i> одобрить</button>
                        <button type="button" class="button js-formAction" data-action="0"><i class="fas fa-sign-in-alt"></i> отказать</button>
                    </div>
                </form>
                    @push('js')
                        <script>
                            $('.js-formAction').on('click', function(e) {
                                e.preventDefault();
                                $('#withdraw_action_id').val($(this).data('action'));
                                $('#withdraw_action').trigger('submit');
                            })
                        </script>
                    @endpush
                @endif
            </div>
        </div>
    </section>

@endsection
