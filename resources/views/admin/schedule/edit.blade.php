@extends('admin.layouts.app')
@section('title', "Добавление задания")
@section('description', "Добавление задания" )
@section('content')
    <h1>Добавление задания</h1>

    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.schedule.update', $schedule['id'] ) }}">
                    @csrf
                    @method('PUT')
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control datepicker-here"
                               name="date"
                               data-time="{{ $schedule['date'] }}"
                               value="" required placeholder="Время вебинара">

                        @error('date')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="title"
                               value="{{ $schedule['title'] }}"  placeholder="Заголовок">

                        @error('title')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="host"
                               value="{{ $schedule['host'] }}"  placeholder="Спикер">

                        @error('host')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <textarea required name="text" id="" cols="30" rows="10" placeholder="Текст задания">{{ $schedule['text'] }}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Изменить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
    @include('datapicker')
    @push('js')
        <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace( 'text' );
        </script>
    @endpush
@endsection
