@extends('admin.layouts.app')
@section('title', "Расписание")
@section('description', "Расписание" )
@section('content')
    <h1>Расписание</h1>
    <a href="{{ route('admin.schedule.create') }}" class="button"><i class="fas fa-user-plus"></i> Добавить</a>
    <section style="margin-top: 30px;">
        @foreach($schedules as $schedule)
            <div class="section_item">
                <div class="section_item_wrap">
                    <div class="section_with_button">
                        <h4>{{ Carbon::parse($schedule->date)->format('H:i d.m.Y') }}</h4>
                        <div class="section_with_button--buttons">
                            <a href="{{ route('admin.schedule.edit', $schedule->id) }}" class="button">
                                <i class="fas fa-external-link-square-alt"></i> Изменить
                            </a>
                            <form action="{{ route('admin.schedule.destroy', $schedule->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="auth_form_group">
                                    <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Удалить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
