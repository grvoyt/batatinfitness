@extends('admin.layouts.app')
@section('title', "Добавление вебинара")
@section('description', "Добавление вебинара" )
@section('content')
    <h1>Добавление вебинара</h1>

    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.online.store') }}">
                    @csrf
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="title"
                               value="{{ old('title') }}" required placeholder="Название задания">

                        @error('title')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="link"
                               value="{{ old('link') }}" placeholder="Ссылка на youtube">

                        @error('link')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
