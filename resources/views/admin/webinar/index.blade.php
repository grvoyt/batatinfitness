@extends('admin.layouts.app')
@section('title', "Вебинары")
@section('description', "Вебинары" )
@section('content')
    <h1>Прямые эфиры</h1>
    <a href="{{ route('admin.online.create') }}" class="button"><i class="fas fa-user-plus"></i> Добавить</a>

    <section style="margin-top: 30px;">
        @foreach($webinars as $webinar)
            <div class="section_item">
                <div class="section_item_wrap">
                    <div class="section_with_button">
                        <h4>{{ $webinar->title }}</h4>
                        <div class="section_with_button--buttons">
                            <a href="{{ route('admin.online.edit', $webinar->id) }}" class="button">
                                <i class="fas fa-external-link-square-alt"></i> Изменить
                            </a>
                            <form action="{{ route('admin.online.destroy', $webinar->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="auth_form_group">
                                    <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Удалить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>



    @push('js')
        @include('admin.layouts.table')
        @if(session('success'))
            <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
        @endif
    @endpush
@endsection
