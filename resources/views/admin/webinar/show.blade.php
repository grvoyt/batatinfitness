@extends('admin.layouts.app')
@section('title', $webinar->title)
@section('description', $webinar->title )
@section('content')
    <h1>{{ $webinar->title }}</h1>
    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <form id="withdraw_action" method="POST" enctype="multipart/form-data" action="{{ route('admin.online.update',$webinar->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="title"
                               value="{{ $webinar->title }}" required placeholder="Название задания">

                        @error('title')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="link"
                               value="https://www.youtube.com/watch?v={{ $webinar->link }}" placeholder="Ссылка на youtube">

                        @error('link')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

@endsection
