@extends('admin.layouts.app')
@section('title', "Изменить задания")
@section('description', "Изменить задания" )
@section('content')
    <h1>Изменить задания</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.tasks.update', $task->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="name"
                               value="{{ $task->name ?? old('name') }}" required placeholder="Название задания">

                        @error('name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="video_url"
                               value="{{ $task->video_url ?? old('video_url') }}" required placeholder="Ссылка на видео">

                        @error('video_url')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <textarea required name="text" id="" cols="30" rows="10" placeholder="Текст задания">{{ $task->text ?? old('text') }}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Изменить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
    @push('js')
        <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace( 'text' );
        </script>
    @endpush
@endsection
