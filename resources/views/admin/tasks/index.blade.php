@extends('admin.layouts.app')
@section('title', "Задания")
@section('description', "Задания" )
@section('content')
    <h1>Задания</h1>
    <a href="{{ route('admin.tasks.create') }}" class="button"><i class="fas fa-user-plus"></i> Добавить</a>
    <section style="margin-top: 30px;">
        @foreach($tasks as $task)
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="section_with_button">
                    <h4>{{ $task->name }}</h4>
                    <div class="section_with_button--buttons">
                        @if($task->id !== 1)
                        <form action="{{ route('admin.tasks.activate', $task->id) }}" method="POST">
                            @csrf
                            <div class="auth_form_group">
                                <button type="submit" class="button">@if( !$task->is_opened )<i class="fas fa-toggle-on"></i> Вкл@else<i class="fas fa-toggle-off"></i> Выкл@endif<button>
                            </div>
                        </form>
                        @endif
                        <a href="{{ route('admin.tasks.edit', $task->id) }}" class="button">
                            <i class="fas fa-external-link-square-alt"></i> Изменить
                        </a>
                        <form action="{{ route('admin.tasks.destroy', $task->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <div class="auth_form_group">
                                <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Удалить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
