@extends('admin.layouts.app')
@section('title', "Добавление задания")
@section('description', "Добавление задания" )
@section('content')
    <h1>Добавление задания</h1>

    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.tasks.store') }}">
                    @csrf
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="name"
                               value="{{ old('name') }}" required placeholder="Название задания">

                        @error('name')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input type="text"
                               class="form-control"
                               name="video_url"
                               value="{{ old('video_url') }}" placeholder="Ссылка на видео">

                        @error('video_url')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <textarea required name="text" id="" cols="30" rows="10" placeholder="Текст задания">{{ old('text') }}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if(session('success'))
        <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
    @endif
@endsection
