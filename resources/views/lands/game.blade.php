<!DOCTYPE html>
<html lang="ru">
<head>
    <base href="{{ url('/') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>Фитнес-школа Александра Бататина</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="{{ asset('game/css/all.min.css') }}?{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/style.css') }}?{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/media.css') }}?{{ d_time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script defer src="{{ asset('game/js/all.min.js') }}?{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/app.js') }}?{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/timer.js') }}?{{ d_time() }}"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
    <!-- End Google Tag Manager -->

</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="container">
        <div class="fl_r sp-b">
            <div class="header_logo">
                <img src="{{ asset('game/img/logo.png') }}" alt="">
            </div>
            <div class="header_menu">
                <ul>
                    <li><a class="scrollto" href="#top">Главная</a></li>
                    <li><a class="scrollto" href="#whats">Ведущие</a></li>
                    <li><a class="scrollto" href="#reviews">Отзывы</a></li>
                    <li><a class="scrollto" href="#have">Преимущества</a></li>
                    <li><a class="scrollto" href="#game">Игра</a></li>
                    <li><a class="scrollto" href="#doit">Участие</a></li>
                    <li><a class="scrollto" href="#form">Регистрация</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<section class="main">
    <div class="container">
        <div class="fl_r sp-b">
            <div class="main_item left">
                <p class="mobile_show prize">
                    <img src="{{ asset('game/img/prize.png') }}" alt="">
                    <span>Приз 5000 руб.</span>
                </p>
                <h2>Старт {{ config('others.dates.game') }} (7 дней)</h2>
                <p class="text_min mb30">Участие из любой точки мира<br>Нужен только интернет, ноутбук или смарфон</p>
                <img src="{{ asset('game/img/title.png') }}" class="mb30 main_item_title" alt="Бесплатная фитнес-игра">
                <p class="text_min mb40"><b>Для девушек, которые хотят красивую фигуру без стрессов и голодовок</b></p>
                <button class="btn jss scrollto" href="#form">Хочу участвовать бесплатно</button>
            </div>
            <div class="main_item right">
                <img src="{{ asset('game/img/baba.png') }}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="time">
    <div class="container">
        <h2 class="title mb30">Получи Бонус: "Готовое Меню для Похудения" при Регистрации в&nbsp;течение:</h2>
        <div class="time_block">
            <img src="{{ asset('game/img/gantelya.png') }}" alt="" class="time_img">
            <div class="time_section">
                <div class="time_section_item" id="time_hour">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
                <div class="time_section_item" id="time_minute">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
                <div class="time_section_item" id="time_second">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="what">
    <div class="container">
        <h2 class="title ">Что это за ФИТНЕС-ИГРА такая? :)</h2>
        <div class="video_border mb40">
            <a href="https://www.youtube.com/watch?v=VrDsaK6Mvu8" class="video_link video_act">
                <img src="http://i.ytimg.com/vi/VrDsaK6Mvu8/maxresdefault.jpg" alt="">
                <div class="play_button"></div>
            </a>
        </div>
        <h2 id="whats" class="title wave mb60">Ведущие фитнес-игры:</h2>
        <div class="treners mb30">
            <div class="treners_item">
                <div class="treners_item_img_w">
                    <img src="{{ asset('game/img/batatin.jpg') }}" alt="" class="treners_item_img">
                </div>
                <div class="treners_item_descr">
                    <p class="title">АЛЕКСАНДР БАТАТИН</p>
                    <p class="text">Основатель Фитнес-школы</p>
                    <p class="text">Главный Тренер Фитнес-Игры</p>
                    <p class="text">Специалист в области Диетологии (опыт 8 лет)</p>
                </div>
            </div>
            <div class="treners_item">
                <div class="treners_item_img_w">
                    <img src="{{ asset('game/img/sorokina.jpg') }}" alt="" class="treners_item_img">
                </div>

                <div class="treners_item_descr">
                    <p class="title">ИРИНА СОРОКИНА</p>
                    <p class="text">Тренер Фитнес-Игры</p>
                    <p class="text">Прошла эту программу (похудела на 12кг)</p>
                    <p class="text">Чемпионка по Фитнесу</p>
                </div>
            </div>
            <div class="treners_item">
                <div class="treners_item_img_w">
                    <img src="{{ asset('game/img/tr3.jpg') }}" alt="" class="treners_item_img">
                </div>

                <div class="treners_item_descr">
                    <p class="title">СЭРМД ХУССАЙН</p>
                    <p class="text">Коуч-психолог (опыт 5 лет)</p>
                    <p class="text">Получишь «Волшебный пендель». Сэрмд расскажет как не опускать руки и не «сливаться». Это важно!</p>
                </div>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section class="otzivi">
    <div class="container">
        <div class="otzivi_header">
            <h2 class="title wave b30 w66">Эти девушки уже получили результат по нашей программе</h2>
        </div>
        <div class="otzivi_main">
            <div class="otzivi_main_item">
                <img src="{{ asset('game/img/ot_1.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Лена (31 год, молодая мама, г.Киев), она похудела в нашей программе на 17кг</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <img src="{{ asset('game/img/ot_2.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Ира (32 года, г.Санкт-Петербург), Она похудела на 12кг в нашей программе.</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <a href="https://www.youtube.com/watch?v=gsjkihcXsbM" class="video_act">
                    <img src="http://i.ytimg.com/vi/gsjkihcXsbM/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео-благодарность Лены после похудения в нашей программе</p>
                </div>
            </div>

            <div class="otzivi_main_item">
                <a href="https://www.youtube.com/watch?v=NOC7YUyyyX0" class="video_act">
                    <img src="http://i.ytimg.com/vi/NOC7YUyyyX0/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео-благодарность Иры после похудения в нашей программе</p>
                </div>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section id="have" class="have">
    <div class="container">
        <h2 class="title wave mb60">За время фитнес-игры ты:</h2>
        <div class="have_wrap">
            <div class="have_item">
                <img src="{{ asset('game/img/have_1.jpg') }}" alt="" class="have_item_img">
                <p class="have_item_secr">Увидишь свои первые изменения во внешнем виде (уйдёт 2-3 кг, коррекция проблемных зон, мышцы станут более упругими, улучшится состояние здоровья)</p>
            </div>
            <div class="have_item">
                <img src="{{ asset('game/img/have_2.jpg') }}" alt="" class="have_item_img">
                <p class="have_item_secr">Получишь практические шаги, как сделать фигуру стройной,
                    подтянутой и закрепить этот результат навсегда
                    (без возврата).</p>
            </div>
            <div class="have_item">
                <img src="{{ asset('game/img/have_3.jpg') }}" alt="" class="have_item_img">
                <p class="have_item_secr">Узнаешь, как круглый год держать себя в подтянутой форме.
                    При этом не ограничивая себя в еде и кушая вкусняшки:)</p>
            </div>
            <div class="have_item">
                <img src="{{ asset('game/img/have_4.jpg') }}" alt="" class="have_item_img">
                <p class="have_item_secr">Перестроишь свой режим Питания на грамотный формат.
                    Чтобы кушать вкусно, не голодать и иметь Красивую фигуру.</p>
            </div>
            <div class="have_item">
                <img src="{{ asset('game/img/have_5.jpg') }}" alt="" class="have_item_img">
                <p class="have_item_secr">Получишь хорошую мотивацию, «Волшебный пендель»:)
                    Так как параллельно с тренерами Фитнес-Школы, будет
                    Коуч-психолог.</p>
            </div>
            <div class="have_item">
                <img src="{{ asset('game/img/have_6.jpg') }}" alt="" class="have_item_img">
                <p class="have_item_secr">Получишь возможность выиграть 5000 рублей по итогам
                    7 дней Игры.  Выполняя обычные задания.
                    Но самое ценное - это твой грамотный старт.

                </p>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section id="game" class="steps">

    <div class="container">
        <h2 class="title wave mb60">Как проходит фитнес-игра:</h2>
        <div class="steps_wrap">
            <div class="step_title w66">У тебя есть закрытый кабинет, в который ты заходишь
                с телефона или компьютера:</div>
            <div class="step_img w80">
                <img src="{{ asset('game/img/dots.jpg') }}" class="steps1">
                <img src="{{ asset('game/img/step_1.jpg') }}" alt="">
                <div class="circle1"></div>
            </div>
            <div class="step_title w80">
                В Игре у тебя есть задания, которые ты выполняешь,
                получаешь Балы!  По итогам заданий и результата
                определиться победительница, которая получит денежный приз! :)</div>
            <div class="step_img w80">
                <img src="{{ asset('game/img/step_2.jpg') }}" />
                <div class="circle2"></div>
            </div>
            <div class="step_title w66">Ты выполняешь упражнения, которые будут в Игре! Чтобы улучшить свою форму и здоровье!</div>
            <div class="step_img w80">
                <div class="cirlce3"></div>
                <img src="{{ asset('game/img/step_3.jpg') }}" />
                <img src="{{ asset('game/img/dots.jpg') }}" class="steps2">
            </div>
            <div class="step_title w80">Во время Игры будут проходить онлайн мастер-классы с высокой
                пользой и ценностью для тебя! Также будет рубрика "Вопрос-ответ".</div>
            <div class="step_video">
                <a class="video_act" href="https://www.youtube.com/watch?v=viqmwXygZS0">
                    <img src="//img.youtube.com/vi/viqmwXygZS0/maxresdefault.jpg" />
                    <div class="play_button"></div>
                </a>
            </div>
        </div>
    </div>
</section>

<section id="reviews" class="reviews">
    <div class="container">
        <h2 class="title wave mb60">Только реальные отзывы наших клиентов</h2>
        <div class="fl_c">
            <div class="reviews_item">
                <a href="https://www.youtube.com/watch?v=sssVkomj91I" class="video_act link_border">
                    <img src="http://i.ytimg.com/vi/sssVkomj91I/maxresdefault.jpg" />
                    <div class="play_button"></div>
                </a>
                <p class="title">Илона (35 лет, Дортмунд, Германия). </p>
                <p class="text">Похудела уже на 5кг в программе Александра</p>
            </div>
            <div class="reviews_item">
                <a href="https://www.youtube.com/watch?v=P0KjWPyHyU4" class="video_act link_border">
                    <img src="http://i.ytimg.com/vi/P0KjWPyHyU4/hqdefault.jpg" />
                    <div class="play_button"></div>
                </a>
                <p class="title">Таня (28 лет, Харьков, Украина).</p>
                <p class="text">Похудела на 7 кг в программе Александра и закрепила результат навсегда</p>
            </div>
            <div class="reviews_item">
                <a href="https://www.youtube.com/watch?v=ZyIEIdmxSE0" class="video_act link_border">
                    <img src="http://i.ytimg.com/vi/ZyIEIdmxSE0/maxresdefault.jpg" />
                    <div class="play_button"></div>
                </a>
                <p class="title">Виктория (37 лет, Чебоксары, Россия). </p>
                <p class="text">Похудела уже на 6,5кг в программе Александра</p>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
            <img src="{{ asset('game/img/block2.jpg') }}" alt="" class="block2_img">
        </div>
    </div>
</section>

<section id="doit" class="block2">

    <div class="container">
        <h2 class="title wave mb60">Участвуй в фитнес-игре, если:</h2>
        <div class="block2_list">

            <ol>
                <li>Тебе не хватает настроя и мотивации (Волшебного пенделя), чтобы просто начать.</li>
                <li>Не хватает знаний, опыта и профессионального контроля специалиста (тренера, диетолога, наставника).</li>
                <li>Ты уже пробовала худеть и это было безрезультатно (ты срывалась, вес возвращался, опускались руки)</li>
                <li>Ты молодая мама и хочешь быть в форме, быть привлекательной и сексуальной.</li>
                <li>Тебе не нравится своё отражение в зеркале и не можешь покупать себе модную трендовую одежду (платье)</li>
                <li>Ты устала садится на разные Диеты, срываться и потом корить себя за это (ругать внутри).</li>
                <li>Ты хочешь стать более уверенной, повысить самооценку. И стать, в конце концов, крутой, привлекательной и самодостаточной женщиной:)</li>
            </ol>
            <img src="{{ asset('game/img/eksp.jpg') }}" class="block2_img2">
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section class="about">
    <div class="container">
        <div class="about_head w80">
            <img src="{{ asset('game/img/about_h.jpg') }}" alt="">
        </div>
        <div class="about_text w80 mb40">
            <p class="title">ОСНОВАТЕЛЬ И ИДЕОЛОГ ПРОЕКТА АЛЕКСАНДР БАТАТИН</p>
            <p class="text">(профессиональный тренер, сертифицированный специалист в области похудения и коррекции фигуры, полуфиналист Чемпионата Мира по фитнесу, помог похудеть 178 девушкам и 75 парням).</p>
        </div>
        <div class="about_img w80">
            <img src="{{ asset('game/img/about_2.jpg') }}" alt="">
        </div>
        <div class="about_text w80 mb40">
            <p class="text"><b>Более 8ми лет Александр посвятил изучению</b> всех деталей
                и тонкостей женского и мужского похудения. И разработал свою
                методику (программу),  которая позволяет похудеть быстро и закрепить
                результат навсегда. Без диет, голодовок и сложных тренировок.</p>
        </div>
        <div class="about_video">
            <a href="https://www.youtube.com/watch?v=ClHT_q09qpM" class="video_act link_border">
                <img src="http://i.ytimg.com/vi/ClHT_q09qpM/maxresdefault.jpg" />
                <div class="play_button"></div>
            </a>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section id="form" class="footer_form">
    <div class="container">
        <div class="form_wrap">
            <div class="form_wrap_img">
                <img src="{{ asset('game/img/footer.jpg') }}" />
            </div>
            <div class="form_wrap_form">
                <form id="main_form" action="{{ route('register') }}" method="POST">
                    @csrf
                    <legend>Заполни форму, чтобы гарантированно попасть в фитнес-игру</legend>
                    @error('name')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input type="text" name="name" placeholder="Введи Имя" class="form_input" value="{{ old('name') }}">
                    @error('email')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input type="email" name="email" placeholder="Введи e-mail" class="form_input" value="{{ old('email') }}">

                    <input type="text" name="promocode" placeholder="Введи промокод если есть" class="form_input last" value="{{ old('promocode') }}">
                    <p class="small_text">* введи промокод (если есть), чтобы получить бонусы!</p>
                    <button class="form_btn" type="submit">Зарегистрироваться в игре</button>
                </form>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="fl_r sp-b">
            <a href="{{ route('politics') }}" class="footer_link">Политика конфиденциальности</a>
            <a href="{{ route('agreement') }}" class="footer_link">Согласие с рассылкой</a>
            <a href="{{ route('otkaz') }}" class="footer_link">Отказ от ответственности</a>
        </div>
    </div>
</footer>

<div id="video_show">
    <div class="video_show_wrapper">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" allowfullscreen></iframe>
        </div>
        <button class="video_show_clode">+</button>
    </div>
</div>
@if(count($errors) > 0)
    <script>location.hash = '#form'</script>
@endif
</body>
</html>