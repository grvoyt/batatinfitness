<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>Платеж успешно принят</title>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="{{ asset('trening/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('trening/css/style.css') }}?1{{ time() }}">
    <link rel="stylesheet" href="{{ asset('trening/css/media.css') }}?1{{ time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="{{ asset('trening/js/jquery.mousewheel.min.js') }}"></script>
    <script defer src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script defer src="{{ asset('trening/js/all.min.js') }}"></script>
    <script defer src="{{ asset('trening/js/app.js') }}?{{ time() }}"></script>
    <script defer src="{{ asset('trening/js/timer.js') }}?{{ time() }}"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="payment">

<section class="main">
    <div class="container">
        <div class="fl_r sp-b fdc w600ma">
            <div class="head_item">
                <span>👍</span>
                <h2 class="main_offer">ПОЗДРАВЛЯЮ!</h2>
                <p>Вы успешно зарегистрировались</p>
            </div>
            <div class="main_item mb30">
                <p class="main_text_bold mb30" style="font-weight: normal;font-style: normal;">Через пару минут на ваш email придет подтверждение регистрации.</p>
                <p class="main_text_bold sec" style="font-style: normal;">Обязательно проверьте почту и папку "Спам", если письма нет.</p>
            </div>
            <div class="action_main">
                <div class="images">
                    <a href="https://t.me/school_by_alex_batatin_bot">
                        <img src="{{ asset('telegram.svg') }}" alt="">
                    </a>
                    <a href="https://m.me/1075174725847396/">
                        <img src="{{ asset('fbmes.svg') }}" alt="">
                    </a>
                </div>
                <p>Куда Вам прислать Бонус (чек-лист)? <br><strong>«Как держать себя в подтянутой форме круглый год»</strong></p>
            </div>
        </div>
    </div>
</section>

<style>
    form {
        width: 100%;
        display: flex;
        flex-direction: column;
    }
    form .form-block {
        width: 100%;
        margin-bottom: 20px;
        padding-bottom: 20px;
        position: relative;
    }

    form .form-block input {
        width: 100%;
        height: 65px;
        padding:5px 10px;
    }

    form .form-block .red_sign {
        position: absolute;
        bottom: 0;
        left: 0;
        font-size: 14px;
        color:red;
    }

    form button {
        height: 65px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        background-color:dodgerblue;
        color:#fff;
        font-weight: bold;
        border-radius: 5px;
    }

    .action_main .images {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 20px;
    }

    .action_main .images a {
        width: 80px;
        flex:0 0 80px;
        margin: 10px;
    }

    .action_main p {
        text-align: center;
        font-size: 20px;
        margin-bottom: 20px;
    }
    .main {
        overflow: auto;
        background-attachment: fixed;
        padding-top: 60px;
    }
    .action_main {
        background-color: rgba(200,200,200,0.5);
        border-radius: 5px;
        padding: 20px;

    }
    .w600ma .main_text_bold {
        font-size: 24px;
        text-align: center;
        font-weight: normal;
    }

    .w600ma .main_text_bold.sec {
        font-size: 24px;
        padding: 20px;
        background-color: rgba(200,200,200,0.5);
        border-radius: 5px;
    }
    .head_item {
        border-radius: 5px;
        background-color: #fff;
        margin-bottom: 30px;
        padding: 20px;
        padding-left: 140px;
        position: relative;
    }

    .head_item span {
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);
        left: 40px;
        font-size: 80px;
    }

    .head_item h2 {
        margin-bottom: 0px;
    }

    .head_item p {
        padding-left: 3px;
        font-size: 20px;
    }

    .w600ma {
        max-width: 600px;
        width: 100%;
        margin: auto;
    }
    .main {
        min-height: 100%;
        height: 100vh;
    }
    .main_item {
        position: relative;
        z-index: 2;
    }
    .main_item .text_min {
        margin-bottom: 20px;
    }
    .main_img_baba {
        bottom: -110px;
        left: 50%;
        -webkit-transform: translateX(-10%);
        -moz-transform: translateX(-10%);
        -ms-transform: translateX(-10%);
        -o-transform: translateX(-10%);
        transform: translateX(-10%);
    }

    .fdc {
        flex-direction: column !important;
    }

    @media only screen and (max-width: 1025px) {
        .main_img_baba {
            -webkit-transform: translateX(0);
            -moz-transform: translateX(0);
            -ms-transform: translateX(0);
            -o-transform: translateX(0);
            transform: translateX(0);
        }
    }

    @media only screen and (max-width: 768px) {
        .main_img_baba {
            width: auto;
            left: 51%;
        }
        .main_item.left {
            max-width: auto;
            width: 100%;
            text-align: center;
        }

        .main_item .text_min, .main_text_bold {
            font-size: 30px;
        }
    }

    @media only screen and (max-width : 480px) {
        .main_item .text_min, .main_text_bold {
            font-size: 18px;
        }
        .main_item.left {
            max-width: 100%;
        }
        #register .main_item.left {
            max-width: 100%;
        }
        .main_img_baba {
            top: auto;
            left: 50%;
            bottom: -40%;
            -webkit-transform: translateX(-25%);
            -moz-transform: translateX(-25%);
            -ms-transform: translateX(-25%);
            -o-transform: translateX(-25%);
            transform: translateX(-25%);
        }
        .head_item span {
            left: 15px;
            font-size: 46px;
        }

        .head_item {
            padding-left: 70px;
        }

        .w600ma .main_text_bold,
        .w600ma .main_text_bold.sec {
            font-size: 18px;
        }
    }
</style>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>
