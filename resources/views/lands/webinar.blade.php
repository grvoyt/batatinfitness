<!DOCTYPE html>
<html lang="ru">
<head>
    <base href="{{ url('/') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>Фитнес-школа Александра Бататина</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="{{ asset('game/css/all.min.css') }}?{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/style.css') }}?1234{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/media.css') }}?1234{{ d_time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script defer src="{{ asset('game/js/all.min.js') }}?{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/app.js') }}?1{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/timer.js') }}?{{ d_time() }}"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
    <!-- End Google Tag Manager -->

</head>
<body id="top" class="mytop">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="container">
        <div class="fl_r sp-b">
            <div class="header_logo my_header_logo">
                <img src="{{ asset('game/img/logo.png') }}" alt="">
            </div>
            <div class="header_menu my_header_menu">
                <ul>
                    <li><a class="scrollto" href="#top">Главная</a></li>
                    <li><a class="scrollto" href="#whats">Ведущие</a></li>
                    <li><a class="scrollto" href="#reviews">Отзывы</a></li>
                    <li><a class="scrollto" href="#have">Преимущества</a></li>
                    <li><a class="scrollto" href="#game">Игра</a></li>
                    <li><a class="scrollto" href="#doit">Участие</a></li>
                    <li><a class="scrollto" href=".myform">Регистрация</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<section class="main land2 my_land2">
    <div class="container">
        <div class="fl_r sp-b fw-w my_flex_dir">
            <div class="main_head">
                <p class="main_head_time my_main_head_time">{{ $date_new }} | 20.00 по Москве (онлайн)</p>
                <h1 class="main_head_h1 my_main_head_h1 my_main_head_h11">БЕСПЛАТНЫЙ ВЕБИНАР  </h1>

                <h1 class="main_head_h1 my_main_head_h1"> «Как получить подтянутую фигуру в 2 раза быстрее»  </h1>
            </div>
            <div class="main_item left my_main_item">
                <p class="mobile_show prize">
                    <img src="{{ asset('game/img/prize.png') }}" alt="">
                    <span hidden>Приз 5000 руб.</span>
                </p>
                <!-- <p class="text_min mb30 my_bold_text" style="text-align: center;font-weight: bold;">Как получить красивую подтянутую фигуру в 2 раза быстрее. Без ПП головного мозга. И повысить уровень энергии!</p> -->
                <!-- <button class="btn jss scrollto mb40" href=".myform">Хочу участвовать бесплатно</button> -->
                <form id="main_form" class="myform" action="{{ route('webinar.register') }}" method="POST">
                    @csrf
                    <legend class="my_bold_text">Заполни форму, чтобы гарантировано попасть на мастер-класс:</legend>
                    <p class="text_min  my_text_min" style="text-align: center;">Участие с любой точки мира. Нужен только интернет, ноутбук или смартфон</p>
                    @error('name')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input type="text" name="name" placeholder="Введи Имя" class="form_input" value="{{ old('name') }}">
                    @error('email')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input type="email" name="email" placeholder="Введи e-mail" class="form_input" value="{{ old('email') }}">
                    <button class="form_btn my_form_btn" type="submit">Участвовать бесплатно</button>
                </form>
                
                <!-- <p class="ppplo">* зависит от индивидуальных особенностей организма</p> -->
            </div>
            <div class="main_item right my_main_item">
                <img src="{{ asset('game/img/baba2.png') }}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="time">
    <div class="container">
        <h2 class="title mb30">Получи Бонус (чек-лист)<br> <strong>«Как держать себя в подтянутой форме круглый год»</strong> <br>при регистрации в течение:</h2>
        <div class="time_block">
            <img src="{{ asset('game/img/PDF.png') }}" alt="" class="time_img my_time_img">
            <div class="time_section">
                <div class="time_section_item  my_time" id="time_hour">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
                <div class="time_section_item  my_time" id="time_minute">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
                <div class="time_section_item  my_time" id="time_second">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="doit" class="block2 my_doit">

    <div class="container">
        <h2 class="title wave mb60">Участвуй в онлайн мастер-классе, если:</h2>
        <div class="block2_list">

            <ol class="my_ol">
                <li>Ты уже пробовала худеть, срывалась и вес возвращался .</li>
                <li>Ты уже начала худеть, но нужны профессиональные рекомендации, которые быстрее приведут к результату.</li>
                <li>Ты только начинаешь худеть и не знаешь как грамотно начать.</li>
                <li>Ты уже начала, но не хватает мотивации ("волшебного пенделя").</li>
                <li>Ты молодая мама и хочешь вернуть себе прежнюю подтянутую, красивую фигуру.</li>
                <li>У тебя нет времени на фитнес-центры, а хочется быть стройной и нравится себе в зеркале.</li>
                <li>Тебе не нравится свой внешний вид, из-за этого есть комплексы, неуверенность и внутренний дискомфорт.</li>
                <li>Ты не можешь носить одежду, которая  тебе по вкусу и в тренде сейчас.</li>
            </ol>
            <img src="{{ asset('game/img/eksp.jpg') }}" class="block2_img2">
        </div>
        <div class="section_bottom second_btn">
            <button class="btn scrollto" href=".myform">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section class="what">
    <div class="container">
        <h2 id="whats" class="title wave mb60">Ведущий мастер–класса:</h2>
        <div class="treners mb30">
            <div class="treners_item2">
                <p class="title">АЛЕКСАНДР БАТАТИН</p>
                <div class="treners_item_img_w">
                    <img src="{{ asset('game/img/batatin.jpg') }}" alt="" class="treners_item_img">
                </div>
                <p class="text">Профессиональный тренер</p>
                <p class="text">Специалист в области диетологии (опыт 8 лет)</p>
                <p class="text">Полуфиналист Кубка Мира по фитнесу</p>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href=".myform">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section class="otzivi">
    <div class="container">
        <div class="otzivi_header">
            <h2 class="title wave b30 w66">Эти девушки участвовали в онлайн мастер-классе и уже добились сильных результатов под контролем Александра</h2>
        </div>
        <div class="otzivi_main">
            <div class="otzivi_main_item">
                <img src="{{ asset('game/img/ot_1.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Лена (31 год, молодая мама), она похудела на 17кг под контролем Александра</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <img src="{{ asset('game/img/ot_2.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Ира (32 года), она похудела на 12 кг под контролем Александра</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <a href="https://www.youtube.com/watch?v=gsjkihcXsbM" class="video_act">
                    <img src="http://i.ytimg.com/vi/gsjkihcXsbM/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео! Как проходил этап Коррекции фигуры у Лены</p>
                </div>
            </div>

            <div class="otzivi_main_item">
                <a href="https://www.youtube.com/watch?v=NOC7YUyyyX0" class="video_act">
                    <img src="http://i.ytimg.com/vi/NOC7YUyyyX0/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео! Как проходил этап Коррекции фигуры у Иры</p>
                </div>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href=".myform">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section id="have" class="have">
    <div class="container">
        <h2 class="title wave mb60">За время мастер-класса ты:</h2>
        <div class="block2_list">
            <ol>
                <li>Узнаешь 3 простых шага, как можно быстро улучшить фигуру без стрессов, диет и голодовок </li>
                <li>Получишь практические шаги, как сделать фигуру подтянутой, стройной и закрепить результат навсегда </li>
                <li>Узнаешь, как быстро вернуть форму после родов, срывов и обжорства</li>
                <li>Получишь практические навыки, чтобы проработать проблемные зоны (живот, бока, ушки, бедра, ягодицы и ручки)</li>
                <li>Узнаешь об ошибках, которые совершают 93% девушек при похудении, чтобы их избежать</li>
                <li>Получишь пошаговую инструкцию, как кушать вкусно, сытно и при этом иметь красивую форму.</li>
            </ol>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href=".myform">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<!-- <section id="game" class="steps">
    <div class="container">
        <div class="step_title w80"><b>Это часть прошлого онлайн мастер-класса.</b><br>На котором каждая участница получила максимум ценности и пользы, как улучшить свою фигуру:</div>
            <div class="step_video">
                <a class="video_act" href="https://www.youtube.com/watch?v=viqmwXygZS0">
                    <img src="//img.youtube.com/vi/viqmwXygZS0/maxresdefault.jpg" />
                    <div class="play_button"></div>
                </a>
            </div>
        </div>
    </div>
</section> -->

<section id="reviews" class="reviews">
    <div class="container">
        <h2 class="title wave mb60">Девушки с разных стран мира уже получают результат по методике Александра:</h2>
        <div class="fl_c">
            <div class="reviews_item">
                <a href="https://www.youtube.com/watch?v=sssVkomj91I" class="video_act link_border">
                    <img src="http://i.ytimg.com/vi/sssVkomj91I/maxresdefault.jpg" />
                    <div class="play_button"></div>
                </a>
                <p class="title webinar">Илона (35 лет, Дортмунд, Германия). </p>
                <p class="text">Похудела уже на 5кг в программе Александра</p>
            </div>
            <div class="reviews_item">
                <a href="https://www.youtube.com/watch?v=P0KjWPyHyU4" class="video_act link_border">
                    <img src="http://i.ytimg.com/vi/P0KjWPyHyU4/hqdefault.jpg" />
                    <div class="play_button"></div>
                </a>
                <p class="title webinar">Таня (28 лет, Харьков, Украина).</p>
                <p class="text">Похудела на 7 кг в программе Александра и закрепила результат навсегда</p>
            </div>
            <div class="reviews_item">
                <a href="https://www.youtube.com/watch?v=ZyIEIdmxSE0" class="video_act link_border">
                    <img src="http://i.ytimg.com/vi/ZyIEIdmxSE0/maxresdefault.jpg" />
                    <div class="play_button"></div>
                </a>
                <p class="title webinar">Виктория (37 лет, Чебоксары, Россия). </p>
                <p class="text">Похудела уже на 6,5кг в программе Александра</p>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href=".myform">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
            <img src="{{ asset('game/img/block2.jpg') }}" alt="" class="block2_img">
        </div>
    </div>
</section>

<!-- <section id="doit" class="block2">

    <div class="container">
        <h2 class="title wave mb60">Участвуй в онлайн мастер-классе, если:</h2>
        <div class="block2_list">

            <ol>
                <li>Ты уже пробовала худеть, срывалась и вес возвращался .</li>
                <li>Ты уже начала худеть, но нужны профессиональные рекомендации, которые быстрее приведут к результату.</li>
                <li>Ты только начинаешь худеть и не знаешь как грамотно начать.</li>
                <li>Ты уже начала, но не хватает мотивации ("волшебного пенделя").</li>
                <li>Ты молодая мама и хочешь вернуть себе прежнюю подтянутую, красивую фигуру.</li>
                <li>У тебя нет времени на фитнес-центры, а хочется быть стройной и нравится себе в зеркале.</li>
                <li>Тебе не нравится свой внешний вид, из-за этого есть комплексы, неуверенность и внутренний дискомфорт.</li>
                <li>Ты не можешь носить одежду, которая  тебе по вкусу и в тренде сейчас.</li>
            </ol>
            <img src="{{ asset('game/img/eksp.jpg') }}" class="block2_img2">
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href=".myform">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section> -->

<section class="about">
    <div class="container">
        <div class="about_head w80">
            <img src="{{ asset('game/img/about_h.jpg') }}" alt="">
        </div>
        <div class="about_text w80 mb40">
            <p class="title">ВЕДУЩИЙ ОНЛАЙН МАСТЕР-КЛАССА АЛЕКСАНДР БАТАТИН</p>
            <p class="text">(профессиональный тренер, сертифицированный специалист в области похудения и коррекции фигуры, полуфиналист Чемпионата Мира по фитнесу, помог похудеть 178 девушкам и 75 парням).</p>
        </div>
        <div class="about_img w80">
            <img src="{{ asset('game/img/about_2.jpg') }}" alt="">
        </div>
        <div class="about_text w80 mb40">
            <p class="text"><b>Более 8ми лет Александр посвятил изучению</b> всех деталей
                и тонкостей женского и мужского похудения. И разработал свою
                методику (программу),  которая позволяет похудеть быстро и закрепить
                результат навсегда. Без диет, голодовок и сложных тренировок.</p>
        </div>
        <!-- <div class="about_video">
            <a href="https://www.youtube.com/watch?v=ClHT_q09qpM" class="video_act link_border">
                <img src="http://i.ytimg.com/vi/ClHT_q09qpM/maxresdefault.jpg" />
                <div class="play_button"></div>
            </a>
        </div> -->
        <div class="section_bottom">
            <button class="btn scrollto" href=".myform">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<section id="form" class="footer_form">
    <div class="container">
        <div class="form_wrap">
            <div class="form_wrap_img">
                <img src="{{ asset('game/img/footer.jpg') }}" />
            </div>
            <div class="form_wrap_form">
                <form id="main_form" action="{{ route('webinar.register') }}" method="POST">
                    @csrf
                    <legend>Заполни форму, чтобы гарантировано попасть на мастер-класс:</legend>
                    @error('name')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input type="text" name="name" placeholder="Введи Имя" class="form_input" value="{{ old('name') }}">
                    @error('email')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input type="email" name="email" placeholder="Введи e-mail" class="form_input" value="{{ old('email') }}">
                    <button class="form_btn" type="submit">Участвовать бесплатно</button>
                </form>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="fl_r sp-b">
            <a href="{{ route('politics') }}" class="footer_link">Политика конфиденциальности</a>
            <a href="{{ route('agreement') }}" class="footer_link">Согласие с рассылкой</a>
            <a href="{{ route('otkaz') }}" class="footer_link">Отказ от ответственности</a>
        </div>
    </div>
</footer>

<div id="video_show">
    <div class="video_show_wrapper">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" allowfullscreen></iframe>
        </div>
        <button class="video_show_clode">+</button>
    </div>
</div>


<style>


    .ppplo {
        font-size: 14px;font-style: italic;text-align: center;
    }

    #endmodal {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(255,255,255,0.8);

    }
    
    .model_content {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%,-50%);
        -moz-transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        -o-transform: translate(-50%,-50%);
        transform: translate(-50%,-50%);
        max-width: 450px;
        width: 100%;
        padding: 20px;
        background-color: #fff;
        -webkit-box-shadow: 0px 0px 26.39px 2.61px rgba(34, 34, 34, 0.2);
        box-shadow: 0px 0px 26.39px 2.61px rgba(34, 34, 34, 0.2);
    }

    .model_clode {
        position: absolute;
        right: 5px;
        top: -2px;
        font-weight: bold;
        transform: rotate(45deg);
        font-size: 28px;
        background-color: transparent;
    }

    .model_content h2 {
        color: #ff47df;
        font-size: 36px;
        font-weight: bold;
        margin-bottom: 25px;
        text-transform: uppercase;
    }

    .model_content p {
        margin-bottom: 20px;
    }

    form {
        width: 100%;
        display: flex;
        flex-direction: column;
    }
    form .form-block {
        width: 100%;
        margin-bottom: 20px;
        position: relative;
    }

    form .form-block input {
        width: 100%;
        height: 65px;
        padding:5px 10px;
    }

    form .form-block .red_sign {
        font-size: 12px;
        color:red;
    }

    form button {
        height: 65px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        background-color:dodgerblue;
        color:#fff;
        font-weight: bold;
        border-radius: 5px;
    }

    .action_main .images {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 20px;
    }

    .action_main .images a {
        width: 50px;
        flex:0 0 50px;
        margin: 10px;
    }

    .action_main p {
        text-align: center;
        font-size: 20px;
        margin-bottom: 20px;
    }

    p.micro {
        font-size: 12px;
    }

    p.micro a {
        font-size: inherit;
    }

    @media only screen and (max-width: 480px) {
        .main.land2 .main_item .text_min {
            max-width: 100%;
            margin-bottom: 20px !important;
            line-height: 1.3;
        }

        .ppplo {
            font-size: 6px;
            position: absolute;
            bottom: 5px;
            left: 10px;
        }
    }

</style>
@if(count($errors) > 0)
    <script>location.hash = '#form'</script>
@endif
</body>
</html>