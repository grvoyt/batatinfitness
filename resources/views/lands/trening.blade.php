<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>Онлайн-тренинг по корекции фигуры в домашних условиях (30 дней)</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="{{ asset('trening/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('trening/css/style.css') }}?123{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('trening/css/media.css') }}?123{{ d_time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="{{ asset('trening/js/jquery.mousewheel.min.js') }}"></script>
    <script defer src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script defer src="{{ asset('trening/js/all.min.js') }}"></script>
    <script defer src="{{ asset('trening/js/app.js') }}?{{ d_time() }}"></script>
    <script defer src="{{ asset('trening/js/timer.js') }}?{{ d_time() }}"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
    <!-- End Google Tag Manager -->

</head>
<body id="top">s
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="container">
        <div class="fl_r sp-b">
            <div class="header_logo">
                <img src="{{ asset('trening/img/logo.png') }}" alt="">
            </div>
            <div class="header_menu">
                <ul>
                    <li><a class="scrollto" href="#top">Главная</a></li>
                    <li><a class="scrollto" href="#whats">Ведущие</a></li>
                    <li><a class="scrollto" href="#reviews">Результаты</a></li>
                    <li><a class="scrollto" href="#have">Преимущества</a></li>
                    <li><a class="scrollto" href="#buy">Участие</a></li>
                    <li><a class="scrollto" href="#doit">Тренинг</a></li>
                    <li><a class="scrollto" href="#otz">Отзывы</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>

<section class="main">
    <div class="container">
        <div class="fl_r sp-b">
            <div class="main_item left">
                <h2 class="main_offer">Старт {{ config('others.dates.online') }}</h2>
                <p class="text_min">Участие из любой точки мира</p>
                <p class="text_min mb30">Нужен только интернет, ноутбук или смарфон</p>
                <p class="main_text_bold mb30"><span>Онлайн-тренинг</span> по корекции фигуры в домашних условиях (30 дней)</p>
                <button class="btn jss scrollto" href="#buy">Хочу участвовать</button>
            </div>
            <img class="main_img_baba" src="{{ asset('trening/img/main_girl.png') }}" alt="">
        </div>
    </div>
</section>

<section class="time">
    <div class="container">
        <h2 class="title mb30">Получи Бонус: "15 правил красивой фигуры" при оформлении заявки в течение:</h2>
        <div class="time_block">
            <img src="{{ asset('trening/img/gantelya.png') }}" alt="" class="time_img">
            <div class="time_section">
                <div class="time_section_item" id="time_hour">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
                <div class="time_section_item" id="time_minute">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
                <div class="time_section_item" id="time_second">
                    <div class="time_section_item--head">
                        <span class="target">00</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('trening/img/gant.png') }}" alt="" class="time_img">
</section>

<section class="what">
    <div class="container">
        <h2 id="whats" class="title wave mb60">Твои наставники во время онлайн-тренинга:</h2>
        <div class="treners mb30 w80">
            <div class="treners_item">
                <div class="treners_item_img_w">
                    <img src="{{ asset('trening/img/batatin.png') }}" alt="" class="treners_item_img">
                </div>
                <div class="treners_item_descr">
                    <p class="title">АЛЕКСАНДР БАТАТИН</p>
                    <p class="text">Основатель Фитнес-школы</p>
                    <p class="text">Главный Тренер Фитнес-Игры</p>
                    <p class="text">Специалист в области Диетологии (опыт 8 лет)</p>
                </div>
            </div>
            <div class="treners_item">
                <div class="treners_item_img_w">
                    <img src="{{ asset('trening/img/tr2.png') }}" alt="" class="treners_item_img">
                </div>

                <div class="treners_item_descr">
                    <p class="title">ИРИНА СОРОКИНА</p>
                    <p class="text">Тренер Фитнес-Игры</p>
                    <p class="text">Прошла эту программу (похудела на 12кг)</p>
                    <p class="text">Чемпионка по Фитнесу</p>
                </div>
            </div>
            <div class="treners_item">
                <div class="treners_item_img_w">
                    <img src="{{ asset('trening/img/tr3.png') }}" alt="" class="treners_item_img">
                </div>

                <div class="treners_item_descr">
                    <p class="title">СЭРМД ХУССАЙН</p>
                    <p class="text">Коуч-психолог (опыт 5 лет)</p>
                    <p class="text">Получишь “Волшебный пендель” от нашего коуча-психолога. Он расскажет как постоянно иметь крутой настрой и высокую мотивацию.</p>
                </div>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
    </div>
</section>

<section id="reviews" class="otzivi">
    <div class="otzivi_circle1"></div>
    <div class="otzivi_circle2"></div>
    <div class="container">
        <div class="otzivi_header">
            <h2 class="title wave mb30 w66">Эти девушки уже получили результат по нашей программе</h2>
        </div>
        <div class="otzivi_main">
            <div class="otzivi_main_item">
                <img src="{{ asset('trening/img/ot_1.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Лена (31 год, молодая мама, г.Киев), она похудела в нашей программе на 17кг</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <img src="{{ asset('trening/img/ot_2.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Ира (32 года, г.Санкт-Петербург), Она похудела на 12кг в нашей программе.</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <a href="https//www.youtube.com/watch?v=gsjkihcXsbM" class="video_act">
                    <img src="//i.ytimg.com/vi/gsjkihcXsbM/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео-благодарность Лены после похудения в нашей программе</p>
                </div>
            </div>

            <div class="otzivi_main_item">
                <a href="https://www.youtube.com/watch?v=NOC7YUyyyX0" class="video_act">
                    <img src="//i.ytimg.com/vi/NOC7YUyyyX0/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео-благодарность Иры после похудения в нашей программе</p>
                </div>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
    </div>
</section>

<section id="have" class="have">
    <div class="have_circle_1"></div>
    <div class="container">
        <h2 class="title wave mb60">За время онлайн-тренинга ты:</h2>
        <div class="have_wrap">
            <div class="have_item">
                <div class="have_item_wimg"><img src="{{ asset('trening/img/q1.jpg') }}" alt="" class="have_item_img"></div>
                <p class="have_item_secr">Похудеешь на 4-6 кг, если выполнишь все задания, применишь все советы</p>
            </div>
            <div class="have_item">
                <div class="have_item_wimg"><img src="{{ asset('trening/img/q2.jpg') }}" alt="" class="have_item_img"></div>
                <p class="have_item_secr">Научишься грамотно выполнять упражнения, чтобы быть в форме круглый год</p>
            </div>
            <div class="have_item">
                <div class="have_item_wimg"><img src="{{ asset('trening/img/q3.jpg') }}" alt="" class="have_item_img"></div>
                <p class="have_item_secr">Сделаешь свою фигуру более подтянутой и сексуальной</p>
            </div>
            <div class="have_item">
                <div class="have_item_wimg"><img src="{{ asset('trening/img/q4.jpg') }}" alt="" class="have_item_img" style="flex:0 0 74px"></div>
                <p class="have_item_secr">Изменишь свои пищевые привычки, а это значит: прощай срыв</p>
            </div>
            <div class="have_item">
                <div class="have_item_wimg"><img src="{{ asset('trening/img/q5.jpg') }}" alt="" class="have_item_img"></div>
                <p class="have_item_secr">Разберешься в питании и упражнениях, чтобы самостоятельно
                    подбирать для себя программу</p>
            </div>
            <div class="have_item">
                <div class="have_item_wimg"><img src="{{ asset('trening/img/q6.jpg') }}" alt="" class="have_item_img"></div>
                <p class="have_item_secr">Познакомишься со своими единомышленницами в нашем
                    онлайн-сообществе. Окружение творит чудеса :)</p>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
    </div>
</section>

<section class="slider">
    <div class="container">
        <div class="carousel">
            <div id="carousel" class="slides slider_otziv">
                <div class="slider_otziv_item">
                    <div class="slider_otziv_item_in">
                        <img src="{{ asset('trening/img/otziv.png') }}" alt="" class="otziv_item_img">
                        <p class="title">Елена Петрова</p>
                        <p class="descr">“Не думала, что за такую маленькую цену можно получить Программу, которая даёт Результат! За 4 недели я похудела  на 7 кг и наладила режим питания.  Энергии хоть отбавляй! Благодарна Вам, Александр:)”</p>
                    </div>
                </div>

                <div class="slider_otziv_item">
                    <div class="slider_otziv_item_in">
                        <img src="{{ asset('trening/img/otziv2.png') }}" alt="" class="otziv_item_img">
                        <p class="title">Таня Бережнюк</p>
                        <p class="descr">«Очень рада, что мой вес сдвинулся с «мёртвой» точки после родов.  За месяц минус 6кг. Это при том, что я постоянно ем. Уже записалась на 2й месяц тренинга»</p>
                    </div>
                </div>
                <div class="slider_otziv_item">
                    <div class="slider_otziv_item_in">
                        <img src="{{ asset('trening/img/otziv3.png') }}" alt="" class="otziv_item_img">
                        <p class="title">Таня Бережнюк</p>
                        <p class="descr">“Хочу поблагодарить Александра за поддержку и такую основательную Программу:) мой результат - это минус 5,6 кг за 30 дней. Учитывая, что есть маленькие дети и семейные заботы! Спасибо, Саша, большое!”</p>
                    </div>
                </div>
                <div class="slider_otziv_item">
                    <div class="slider_otziv_item_in">
                        <img src="{{ asset('trening/img/otziv.png') }}" alt="" class="otziv_item_img">
                        <p class="title">Елена Петрова</p>
                        <p class="descr">“Не думала, что за такую маленькую цену можно получить Программу, которая даёт Результат! За 4 недели я похудела  на 7 кг и наладила режим питания.  Энергии хоть отбавляй! Благодарна Вам, Александр:)”</p>
                    </div>
                </div>
                <div class="slider_otziv_item">
                    <div class="slider_otziv_item_in">
                        <img src="{{ asset('trening/img/otziv2.png') }}" alt="" class="otziv_item_img">
                        <p class="title">Таня Бережнюк</p>
                        <p class="descr">«Очень рада, что мой вес сдвинулся с «мёртвой» точки после родов.  За месяц минус 6кг. Это при том, что я постоянно ем. Уже записалась на 2й месяц тренинга»</p>
                    </div>
                </div>
                <div class="slider_otziv_item">
                    <div class="slider_otziv_item_in">
                        <img src="{{ asset('trening/img/otziv3.png') }}" alt="" class="otziv_item_img">
                        <p class="title">Таня Бережнюк</p>
                        <p class="descr">“Хочу поблагодарить Александра за поддержку и такую основательную Программу:) мой результат - это минус 5,6 кг за 30 дней. Учитывая, что есть маленькие дети и семейные заботы! Спасибо, Саша, большое!”</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="tarif">
    <div class="tarif_circle"></div>
    <div class="container">
        <h2 class="title wave mb60">Выбери для себя комфортный формат участия:</h2>
        <div class="tarifs">
            <div class="tarifs_item">
                <div class="tarifs_item_head">
                    <p class="tarif_name">STANDART</p>
                    <p class="tarif_price"><span>1490</span> рублей</p>
                </div>
                <div class="tarif_item_body">
                    <p class="list">Доступ к мастер-классам</p>
                    <p class="list">Доступ к сессии Вопрос-Ответ</p>
                    <p class="list">Доступ ко всему обучающему материалу</p>
                    <p class="list wrong">БЕЗ закрытого чата телеграмм для участниц</p>
                </div>
                <a class="btn" href="{{ route('checkout', 'standart') }}">Я участвую</a>
            </div>
            <div  id="buy" class="tarifs_item hit">
                <div class="tarifs_item_head">
                    <p class="tarif_name">GOLD</p>
                    <p class="tarif_price"><span>1950</span> рублей</p>
                </div>
                <div class="tarif_item_body">
                    <p class="list">Доступ к мастер-классам</p>
                    <p class="list">Доступ к сессии Вопрос-Ответ</p>
                    <p class="list">Доступ ко всему обучающему материалу</p>
                    <p class="green">+ бонусный материал</p>
                    <p class="green">+ доступ к закрытому чату Телеграм</p>
                    <p class="green">+ доступ к мастер-классам с коучем-психологом (Сэрмд Хуссайн)</p>
                </div>
                <a class="btn" href="{{ route('checkout', 'gold') }}">Я участвую</a>
            </div>
            <div class="tarifs_item">
                <div class="tarifs_item_head">
                    <p class="tarif_name">PLATINA</p>
                    <p class="tarif_price"><span>3950</span> рублей</p>
                </div>
                <div class="tarif_item_body">
                    <p class="list">Доступ к мастер-классам</p>
                    <p class="list">Доступ к сессии Вопрос-Ответ</p>
                    <p class="list">Доступ ко всему обучающему материалу</p>
                    <p class="green">+ бонусный материал</p>
                    <p class="green">+ закрытая мастер-группа и личный менеджер (личный доступ к чату с Александром)</p>
                    <p class="green">+ личный разбор от коуча-психолога вашей ситуации (мотивация, настрой, эмоции, нет энергии и т д)</p>
                </div>
                <a class="btn" href="{{ route('checkout', 'platina') }}">Я участвую</a>
            </div>
        </div>
    </div>
</section>

<section class="personal">
    <div class="container">
        <div class="personal_text">
            <p class="first">ЛИЧНАЯ РАБОТА С АЛЕКСАНДРОМ</p>
            <p class="offer">30 000 рублей в месяц</p>
            <p class="offer2">Полное личное сопровождение 30 дней+ доступ к общему материалу!</p>
        </div>
    </div>
    <img src="{{ asset('trening/img/batatin_max.png') }}" alt="" class="personal_man">
    <img src="{{ asset('trening/img/dots.png') }}" alt="" class="personal_dots">
</section>

<section id="doit" class="whathap">
    <div class="whathap_circle"></div>
    <div class="container">
        <div class="section_bottom">
            <a class="btn " href="{{ route('checkout', 'personal') }}">ХОЧУ УЧАСТВОВАТЬ</a>
        </div>
        <h2 class="title wave mb60">Что происходит внутри тренинга</h2>
        <p class="whathap_text">Это наша обучающая платформа, где проходит онлайн-тренинг</p>
        <div class="whathaps w80">
            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>У тебя все в одном месте</p>
                </div>
            </div>
            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>Заходишь с телефона или компьютера ( это очень удобно)</p>
                </div>
            </div>

            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>Смотришь задания (упражнения и питание)</p>
                </div>
            </div>

            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>Выполняешь задания</p>
                </div>
            </div>

            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>Никакой большой нагрузки и сложных упражнений</p>
                </div>
            </div>

            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>Видишь прогресс на каждой неделе</p>
                </div>
            </div>

            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>Доступ к программе в любом месте</p>
                </div>
            </div>

            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>С нашей стороны контроль заданий</p>
                </div>
            </div>

            <div class="whathaps_item">
                <div class="whathaps_item_img">
                    <img src="{{ asset('trening/img/apple.png') }}" alt="">
                </div>
                <div class="whathaps_item_descr">
                    <p>Участвуешь в закрытых прямых эфирах каждую неделю</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="trenings">
    <div class="trenings_top"></div>
    <div class="container">
        <h2 class="title">Полное видео сопровождение тренинга</h2>
        <div class="trenings_img w80">
            <img src="{{ asset('trening/img/h1.jpg') }}">
        </div>

        <h2 class="title">Объясняем с чего и как грамотно начинать</h2>
        <div class="trenings_img w80">
            <div class="trenings_circle1" ></div>
            <div class="trenings_circle2" ></div>
            <img src="{{ asset('trening/img/h2.jpg') }}">
        </div>

        <h2 class="title">Показываем наглядно правильное выполнение</h2>
        <div class="trenings_img w80">
            <img src="{{ asset('trening/img/h3.jpg') }}">
        </div>

        <p class="trenings_text w66">Так проходят прямые эфиры внутри тренинга,
            на которых мы раскрываем важные темы по питанию,
            тренировкам, режиму, а также рубрика “Вопрос-ответ”
        </p>
        <div class="trenings_img w80">
            <a class="video_act" href="https://www.youtube.com/watch?v=viqmwXygZS0&feature=youtu.be">
                <img src="//img.youtube.com/vi/viqmwXygZS0/maxresdefault.jpg">
                <span class="play_button"></span>
            </a>
            <div class="trenings_img_circle"></div>
        </div>
    </div>
</section>

<section class="likes">
    <div class="container">
        <h2 class="title wave mb60">Вот так делают упражнения наши ученицы внутри программы</h2>
        <div class="likes_wrap">
            <div class="likes_item">
                <a href="https://www.youtube.com/watch?v=ZHcIgr5lC9s&feature=youtu.be" class="video_act">
                    <img src="{{ asset('trening/img/ho_1.jpg') }}" alt="">
                    <span class="play_button"></span>
                </a>
            </div>

            <div class="likes_item">
                <a href="https://www.youtube.com/watch?v=dNdujg0TDV8" class="video_act">
                    <img src="{{ asset('trening/img/ho_2.jpg') }}" alt="">
                    <span class="play_button"></span>
                </a>
            </div>

            <div class="likes_item">
                <a href="https://www.youtube.com/watch?v=5aYk2yT0upo&feature=youtu.be" class="video_act">
                    <img src="{{ asset('trening/img/ho_3.jpg') }}" alt="">
                    <span class="play_button"></span>
                </a>
            </div>

            <div class="likes_item">
                <a href="https://www.youtube.com/watch?v=QN0mLOACMNQ" class="video_act">
                    <img src="{{ asset('trening/img/ho_4.jpg') }}" alt="">
                    <span class="play_button"></span>
                </a>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
    </div>
    <img src="{{ asset('trening/img/gant2.png') }}" class="block2_img3" >
</section>

<section id="doit" class="block2">

    <div class="container">
        <h2 class="title wave mb60">Участвуй в онлайн-тренинге, если:</h2>
        <div class="block2_list">

            <ol>
                <li>Тебе не хватает настроя и мотивации (Волшебного пенделя), чтобы просто начать.</li>
                <li>Не хватает знаний, опыта и профессионального контроля специалиста (тренера, диетолога, наставника).</li>
                <li>Ты уже пробовала худеть и это было безрезультатно (ты срывалась, вес возвращался, опускались руки)</li>
                <li>Ты молодая мама и хочешь быть в форме, быть привлекательной и сексуальной.</li>
                <li>Тебе не нравится своё отражение в зеркале и не можешь покупать себе модную трендовую одежду (платье)</li>
                <li>Ты устала садится на разные Диеты, срываться и потом корить себя за это (ругать внутри).</li>
                <li>Ты хочешь стать более уверенной, повысить самооценку. И стать, в конце концов, крутой, привлекательной и самодостаточной женщиной:)</li>
            </ol>
            <img src="{{ asset('trening/img/eksp.jpg') }}" class="block2_img2">
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
    </div>
</section>

<section class="trener">
    <div class="container">
        <h2 class="title">александр бататин</h2>
        <p class="title2 mb30">основатель фитнес-школы</p>
        <div class="w80 mb60">
            <img src="{{ asset('trening/img/about_h.jpg') }}">
        </div>

        <p class="trener_text w80 mb40"><b>Профессиональный тренер, сертифицированный специалист</b>
            в области похудения и коррекции фигуры. Полуфиналист чемпионата
            мира по фитнесу. Помог похудеть 178 девушкам и 75 парням</p>

        <div class="w80 mb60">
            <img src="{{ asset('trening/img/about_2.jpg') }}">
        </div>

        <p class="trener_text2 mb40"><b>Более 8 лет Александр посвятил изучению всех деталей</b>
            и тонкостей женского и мужского похудения. Разработал свою  методику, которая позволяет худеть быстро и навсегда. Без диет, голодовок и сложных тренировок.</p>

        <div class="trener_video">
            <a class="video_act" href="https://www.youtube.com/watch?v=ClHT_q09qpM">
                <img src="{{ asset('trening/img/about_v.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
        </div>

        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
        <div class="trener_circle1"></div>
    </div>
</section>

<section id="otz" class="otzivi">
    <div class="container">
        <h2 class="title wave mb60">Результат и слова благодарности наших учениц</h2>

        <div class="otzivi_item mb60">
            <a href="https://www.youtube.com/watch?v=sssVkomj91I" class="video_act mb40">
                <img src="{{ asset('trening/img/o11.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
            <p class="title">Илона (35 лет, Дортмунд, Германия).</p>
            <p class="text">Похудела уже на 5кг в программе Александра</p>
            <div class="otzivi_item_circle1"></div>
        </div>

        <div class="otzivi_item mb60">
            <a href="https://www.youtube.com/watch?v=P0KjWPyHyU4" class="video_act mb40">
                <img src="{{ asset('trening/img/o12.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
            <p class="title">Таня (28 лет, Харьков, Украина).</p>
            <p class="text">Похудела на 7 кг в программе Александра и закрепила результат навсегда</p>
        </div>

        <div class="otzivi_item mb60">
            <a href="https://www.youtube.com/watch?v=ZyIEIdmxSE0" class="video_act mb40">
                <img src="{{ asset('trening/img/o13.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
            <p class="title">Виктория (37 лет, Чебоксары, Россия).</p>
            <p class="text">Похудела уже на 6,5 кг в программе Александра</p>
            <div class="otzivi_item_circle2"></div>
        </div>

        <div class="otzivi_item mb60">
            <a href="https://www.youtube.com/watch?v=vF0QKfyIHuY" class="video_act mb40">
                <img src="{{ asset('trening/img/o1.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
            <p class="title">Анна (Москва, Россия).</p>
            <p class="text">Похудела на 5,5 кг за 30 дней</p>
            <div class="otzivi_item_circle3"></div>
        </div>

        <div class="otzivi_item mb60">
            <a href="https://www.youtube.com/watch?v=gsA8IuGFSCI" class="video_act mb40">
                <img src="{{ asset('trening/img/o2.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
            <p class="title">Наташа (Киев, Украина).</p>
            <p class="text">похудела на 4,5 кг за 30 дней и продолжает идти к своей цели!</p>
        </div>

        <div class="otzivi_item mb60">
            <a href="https://www.youtube.com/watch?v=T5ncm4H9MdY" class="video_act mb40">
                <img src="{{ asset('trening/img/o3.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
            <p class="title">Юля (Уфа, Россия, молодая мама).</p>
            <p class="text">уже участвует в нашей программе и похудела<br>на 2,5 кг за 2 недели. Молодчинка :)</p>
        </div>

        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
    </div>
</section>

<section class="biger">
    <div class="container">
        <h2 class="title wave mb60">Также эту программу рекомендуют парни,  которые проходили мужской тренинг :</h2>
        <div class="biger_imgs">
            <div class="biger_imgs_item">
                <img src="{{ asset('trening/img/o4.jpg') }}" alt="">
            </div>
            <div class="biger_imgs_item">
                <img src="{{ asset('trening/img/o5.jpg') }}" alt="">
            </div>
        </div>
        <p class="biger_text"><b>Максим Чирков,</b> бизнесмен<br>Похудел по методике Александра на 23 кг</p>
        <p class="quote">"Я весил 106 кг. Не знал, что делать и никто не мог мне помочь.
            Случайно в интернете наткнулся на программу Александра.
            Саша помог мне похудеть  на 23 кг и закрепить  результат навсегда.
            Это несмотря на то, что мы в разных странах.  Очень благодарен
            Александру."</p>

        <div class="biger_video mb40">
            <a href="https://www.youtube.com/watch?v=ZyIEIdmxSE0" class="video_act">
                <img src="{{ asset('trening/img/o6.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
        </div>

        <div class="biger_imgs">
            <div class="biger_imgs_item">
                <img src="{{ asset('trening/img/o7.jpg') }}" alt="">
            </div>
            <div class="biger_imgs_item">
                <img src="{{ asset('trening/img/o8.jpg') }}" alt="">
            </div>
        </div>

        <p class="biger_text"><b>Олесь Тимофеев,</b> бизнесмен, основатель Genius Marketing<br>Тренируется под контролем Александра уже 2 года</p>
        <p class="quote">"Саша Бататин действительно профи своего дела. Его подход и методика
            по коррекции  веса действительно работают. Он настолько правильно
            подбирает нагрузку и питание, что я круглый год держу себя
            в подтянутой форме."</p>

        <div class="biger_video mb40">
            <a href="https://www.youtube.com/watch?v=ZyIEIdmxSE0" class="video_act">
                <img src="{{ asset('trening/img/o9.jpg') }}" alt="">
                <span class="play_button"></span>
            </a>
        </div>

        <div class="biger_imgs">
            <div class="biger_imgs_item">
                <img src="{{ asset('trening/img/o10.jpg') }}" alt="">
            </div>
            <div class="biger_imgs_item">
                <img src="{{ asset('trening/img/o101.jpg') }}" alt="">
            </div>
        </div>

        <p class="biger_text"><b>Антон Бовт,</b> серийный предприниматель<br>Тренируется под контролем Александра уже 2 года</p>
        <p class="quote last">"Хочу поблагодарить Сашу за мою форму! Я очень хорошо похудел
            и улучшил свои физические показатели. Это в условиях очень большой
            нагрузки на работе. Спасибо Саня, тебе!"</p>
        <div class="section_bottom">
            <button class="btn scrollto" href="#buy">ХОЧУ УЧАСТВОВАТЬ</button>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="fl_r sp-b">
            <a href="{{ route('politics') }}" class="footer_link">Политика конфиденциальности</a>
            <a href="{{ route('agreement') }}" class="footer_link">Согласие с рассылкой</a>
            <a href="{{ route('otkaz') }}" class="footer_link">Отказ от ответственности</a>
        </div>
    </div>
</footer>

<div id="video_show">
    <div class="video_show_wrapper">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" allowfullscreen></iframe>
        </div>
        <button class="video_show_clode">+</button>
    </div>
</div>

</body>
</html>
