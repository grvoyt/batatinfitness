<!DOCTYPE html>
<html lang="ru">
<head>
    <base href="{{ url('/') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>Фитнес-школа Александра Бататина</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="{{ asset('game/css/all.min.css') }}?{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/style.css') }}?56{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/media.css') }}?56{{ d_time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script defer src="{{ asset('game/js/all.min.js') }}?{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/app.js') }}?126{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/timer2.js') }}?26x{{ d_time() }}"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
    <!-- End Google Tag Manager -->

</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="container">
        <div class="fl_r justify-content-center">

            <div class="header_logo land3">
                <img src="{{ asset('game/img/logo.png') }}" alt="">
            </div>
        </div>
    </div>
</header>

<section id="form" class="main land2 websale">
    <div class="container">
        <div class="fl_r sp-b fw-w">
            <div class="main_head mb10">
                <h1 class="main_head_h1 mb15" style="font-weight: bold;">Комплексная личная программа Александра Бататина</h1>
                <p class="main_head_time mb0" style="font-weight: normal;">Оставь заявку, чтобы узнать детали программы и забронировать место</p>

            </div>
            <div class="main_block-bottom">

                <div class="main_block_form form_wrap_form">
                    <form class="form_this" action="{{ route('webinar.sale_reg') }}" method="POST">
                        @csrf
                        <div class="form-line">
                            @error('name')
                            <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form_input" name="name" placeholder="Введи имя">

                        </div>
                        <div class="form-line">
                            @error('phone')
                            <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form_input" name="phone" placeholder="Введи телефон">
                        </div>
                        <div class="form-line last">
                            @error('email')
                            <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form_input" name="email" placeholder="Введи email">
                        </div>
                        <input type="hidden" id="form_this_tarif" name="tarif">
                    </form>
                </div>
                <div class="main_video_land3">
                    <h2>При оплате любого пакета в течение 60 минут - ты получаешь Бонус. Онлайн-тренинг <strong>«Красивая талия и животик за 21 день»</strong> БЕСПЛАТНО!</h2>
                    <p class="mb30"><strong>Предложение заканчивается через:</strong></p>
                    <div class="time_section" data-time="60">
                        <div class="arrow arrow_left mobile_show">
                            <img src="{{ asset('game/img/arrow-down.png') }}" alt="">
                        </div>
                        <div class="time_section_item" id="time_minute">
                            <div class="time_section_item--head">
                                <span class="target">00</span>
                            </div>
                        </div>
                        <div class="time_section_item" id="time_second">
                            <div class="time_section_item--head">
                                <span class="target">00</span>
                            </div>
                        </div>
                        <div class="arrow arrow_right mobile_show">
                            <img src="{{ asset('game/img/arrow-down.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="tarif websale">
    <div class="tarif_circle"></div>
    <div class="container">
        <h2 class="title wave mb60">Выбери интересующий тебя пакет:</h2>
        <div class="tarifs land2">
            <div class="tarifs_item">
                <div class="tarifs_item_head">
                    <p class="tarif_name">STANDART</p>
                    <p class="tarif_price"><span>$300</span><br>(1 месяц) </p>
                </div>
                <a class="btn try_reg" href="#" data-tarif="standart2">Подать заявку</a>
            </div>
            <div class="tarifs_item hit">
                <div class="tarifs_item_head">
                    <p class="tarif_name">GOLD</p>
                    <p class="tarif_price"><span>$300 вместо <strike>$500</strike></span><br>(2 месяца, скидка только сегодня) </p>
                </div>
                <a class="btn try_reg" href="#" data-tarif="gold300">Подать заявку</a>
            </div>
            <div  id="buy" class="tarifs_item ">
                <div class="tarifs_item_head">
                    <p class="tarif_name">SILVER</p>
                    <p class="tarif_price"><span>$500 вместо <strike>$700</strike></span><br>(3 месяца, скидка только сегодня)</p>
                </div>
                <a class="btn try_reg" href="#" data-tarif="silver500">Подать заявку</a>
            </div>

            <div class="tarifs_item">
                <div class="tarifs_item_head">
                    <p class="tarif_name">PLATINA</p>
                    <p class="tarif_price"><span>2500$</span><br>(12 месяцев, без скидок, 2 места)</p>
                </div>
                <a class="btn try_reg" href="#" data-tarif="platina2">Подать заявку</a>
            </div>
        </div>
    </div>
</section>

<section id="have" class="have">
    <div class="container">
        <h2 class="title wave mb60">ЧТО ПОЛУЧИТ КАЖДАЯ УЧАСТНИЦА:</h2>
        <div class="block2_list">
            <ol>
                <li>Доступ к обучающей платформе, где храниться весь материал и уроки (формат пакета)</li>
                <li>Гарантированная скорость похудения на 4-8кг в месяц или формирование подтянутой красивой фигуры</li>
                <li>Личная программа питания и тренировок исходя из особенностей организма, исходного состояния и цели</li>
                <li>Материал и уроки по здоровью, улучшению физ. состояния и повышению уровня энергии</li>
                <li>Моя личная поддержка, контроль и коррекция по ходу (в зависимости от пакета)</li>
                <li>Возможность подбора упражнений и нагрузки для зала</li>
                <li>Возможность корректировать питания, нагрузку и режим в целом исходя из ритма жизни и режима (отпуск, командировка, болезнь и т.д.)</li>
                <li>Возможность коррекции формы без физической нагрузки  (по согласованию)</li>
            </ol>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</button>
        </div>
    </div>
</section>

<div id="video_show">
    <div class="video_show_wrapper">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" allowfullscreen></iframe>
        </div>
        <button class="video_show_clode">+</button>
    </div>
</div>
<style>
    .mb0 {
        margin-bottom: 0 !important;
    }
    .tarifs_item {
        order-color: #feff42;
        -webkit-box-shadow: 0px 0px 9.1px 0.9px rgba(136, 88, 5, 0.2);
        box-shadow: 0px 0px 9.1px 0.9px rgba(136, 88, 5, 0.2);
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        -ms-transition: all 0.3s;
        -o-transition: all 0.3s;
        transition: all 0.3s;
    }
    .tarifs_item:hover {
        -webkit-box-shadow: 0px 0px 9.1px 0.9px rgba(136, 88, 5, 0.42);
        box-shadow: 0px 0px 9.1px 0.9px rgba(136, 88, 5, 0.42);
    }
    .tarifs_item.hit {
        position: relative;
    }
    .tarifs_item.hit:before {
        content: '';
        position: absolute;
        top: -2px;
        left: -2px;
        width: 148px;
        height: 96px;
        background: url(/trening/img/hit.png) no-repeat center center;
        z-index: 2;
    }

    .main_block-bottom h2 strong {
        font-size: inherit;
    }

    .mobile_show {
        display: none;
    }
    .time_section {
        position: relative;
    }
    @media only screen and (max-width: 480px) {
        .arrow {
            position: absolute;
            top: 30%;
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            -o-transform: translateY(-50%);
            transform: translateY(-50%);
            width: 50px;
        }
        .arrow_left {
            left: 10px;
        }
        .arrow_right {
            right: 10px;
        }
        .header_logo.land3 {
            width: 90%;
            max-width: 100%;
        }
        .main.websale {
            padding-bottom: 0;
        }
        .tarifs_item.hit:before {
            width: 80px;
            height: 52px;
            background-size:contain;
        }
        .mobile_show {
            display: block;
        }
    }
</style>
@if(count($errors) > 0)
    <script>location.hash = '#form'</script>
@endif
</body>
</html>
