<!DOCTYPE html>
<html lang="ru">
<head>
    <base href="{{ url('/') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>Фитнес-школа Александра Бататина</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="{{ asset('game/css/all.min.css') }}?{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/style.css') }}?1234{{ d_time() }}">
    <link rel="stylesheet" href="{{ asset('game/css/media.css') }}?1234{{ d_time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script defer src="{{ asset('game/js/all.min.js') }}?{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/app.js') }}?1{{ d_time() }}"></script>
    <script defer src="{{ asset('game/js/timer.js') }}?{{ d_time() }}"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
    <!-- End Google Tag Manager -->

</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <div class="container">
        <div class="fl_r sp-b">
            <div class="header_logo land3">
                <p>99 руб./мес</p>
            </div>
            <div class="header_logo land3">
                <img src="{{ asset('img/tel.png') }}" alt="" style="width: 100px;">
            </div>
            <div class="header_logo land3">
                <img src="{{ asset('game/img/logo.png') }}" alt="">
            </div>
        </div>
    </div>
</header>

<section id="form" class="main land2">
    <div class="container">
        <div class="fl_r sp-b fw-w">
            <div class="main_head">
                <h1 class="main_head_h1 mb15" style="font-weight: bold;">ЗАКРЫТЫЙ ОНЛАЙН-КЛУБ ДЛЯ ДЕВУШЕК</h1>
                <p class="main_head_time " style="font-weight: normal;margin-bottom: 50px;">Это твой самый лучший помощник на пути к Фигуре своей мечты :)</p>
                <p class="text_min mb30" style="text-align: center;font-weight: bold;">Обязательно посмотри видео Александра Бататина!</p>
            </div>
            <div class="main_block-bottom">

                <div class="main_block_form form_wrap_form">
                    <form class="land3_wrap" action="{{ route('club_register') }}" method="POST">
                        @csrf
                        <legend>Ответь на вопросы, чтобы подать Заявку на участие</legend>
                        <div class="form-line">
                            <label for="">Твой возраст?</label>
                            @error('age')
                            <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form_input" name="age" placeholder="">

                        </div>
                        <div class="form-line">
                            <label for="">Какая цель во внешнем виде? (похудение, коррекция проблемных зон, набор веса, здоровье и т.д.)</label>
                            @error('goal')
                            <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form_input" name="goal">
                        </div>
                        <div class="form-line">
                            <label for="">Почему для тебя это важно?</label>
                            @error('hap')
                            <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form_input" name="hap">
                        </div>
                        <button class="form_btn" type="submit">ОТПРАВИТЬ ОТВЕТ И ПОДАТЬ ЗАЯВКУ НА УЧАСТИЕ</button>
                    </form>
                </div>
                <div class="main_video_land3">
                    <a class="video_act" href="https://www.youtube.com/watch?v=viqmwXygZS0">
                        <img src="//img.youtube.com/vi/viqmwXygZS0/maxresdefault.jpg">
                        <div class="play_button"></div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="have" class="have land3">
    <div class="container">
        <h2 class="title wave mb60">Что Ты Получишь внутри клуба?</h2>
        <div class="block2_list">
            <ol>
                <li>Грамотно выстроенное питание именно под твой организм!</li>
                <li>Упражнения именно под тебя (видео уроки)</li>
                <li>Рецепты правильного вкусного питания</li>
                <li>Бонусные видео уроки</li>
                <li>Советы и рекомендации</li>
                <li>Приоритетное участие во всех наших комплексных онлайн-программах</li>
            </ol>
        </div>
    </div>
</section>

<section class="otzivi">
    <div class="container">
        <div class="otzivi_header">
            <h2 class="title wave b30 w66">Результаты и отзывы участниц клуба:</h2>
        </div>
        <div class="otzivi_main">
            <div class="otzivi_main_item">
                <img src="{{ asset('game/img/ot_1.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Лена (31 год, молодая мама), она похудела на 17кг под контролем Александра</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <img src="{{ asset('game/img/ot_2.jpg') }}" alt="">
                <div class="otzivi_main_item_footer">
                    <p>Это Ира (32 года), она похудела на 12 кг под контролем Александра</p>
                </div>
            </div>
            <div class="otzivi_main_item">
                <a href="https://www.youtube.com/watch?v=gsjkihcXsbM" class="video_act">
                    <img src="http://i.ytimg.com/vi/gsjkihcXsbM/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео! Как проходил этап Коррекции фигуры у Лены</p>
                </div>
            </div>

            <div class="otzivi_main_item">
                <a href="https://www.youtube.com/watch?v=NOC7YUyyyX0" class="video_act">
                    <img src="http://i.ytimg.com/vi/NOC7YUyyyX0/maxresdefault.jpg" alt="">
                    <div class="play_button"></div>
                </a>
                <div class="otzivi_main_item_footer">
                    <p>Видео! Как проходил этап Коррекции фигуры у Иры</p>
                </div>
            </div>
        </div>
        <div class="section_bottom">
            <button class="btn scrollto" href="#form">ОТПРАВИТЬ ОТВЕТ И ПОДАТЬ ЗАЯВКУ НА УЧАСТИЕ</button>
        </div>
    </div>
</section>



<footer>
    <div class="container">
        <div class="fl_r sp-b">
            <a href="{{ route('politics') }}" class="footer_link">Политика конфиденциальности</a>
            <a href="{{ route('agreement') }}" class="footer_link">Согласие с рассылкой</a>
            <a href="{{ route('otkaz') }}" class="footer_link">Отказ от ответственности</a>
        </div>
    </div>
</footer>

<div id="video_show">
    <div class="video_show_wrapper">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" allowfullscreen></iframe>
        </div>
        <button class="video_show_clode">+</button>
    </div>
</div>
@if(count($errors) > 0)
    <script>location.hash = '#form'</script>
@endif
</body>
</html>
