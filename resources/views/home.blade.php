<!DOCTYPE html>
<html>
<head>
<title>Как похудеть девушке без диет и тренировок</title>

<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="title" content="Как похудеть девушке без диет и тренировок">
<meta name="description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
<link rel="image_src" href="assets/images/header-pic.jpg">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta property="og:locale" content="ru_RU">
<meta property="og:type" content="website">
<meta property="og:title" content="Женский онлайн Мастер-класс «Как похудеть без диет и голодовок»">
<meta property="og:description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
<meta property="og:image" content="http://batatinfitness.club/assets/images/header-pic.png">
<meta property="og:url" content="{{ url('/') }}">
<meta property="og:site_name" content="Как похудеть девушке без диет и тренировок">

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Как похудеть девушке без диет и тренировок">
<meta name="twitter:description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
<meta name="twitter:image" content="http://batatinfitness.club/assets/images/header-pic.png">

<meta itemprop="name" content="Как похудеть девушке без диет и тренировок">
<meta itemprop="description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
<meta itemprop="image" content="http://batatinfitness.club/assets/images/header-pic.png">

<!-- Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#d6c4a0">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#d6c4a0">
<!-- iOS Safari -->
<meta name="apple-mobile-web-app-status-bar-style" content="#d6c4a0">

<link rel="stylesheet" href="{{ asset('land/cache/style.css') }}?{{ d_time() }}" type="text/css">
<link rel="stylesheet" href="{{ asset('land/cache/intlinput/css/intlTelInput.css') }}">
    <link rel="stylesheet" href="{{ asset('land/yved/style.css') }}">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
<!-- End Google Tag Manager -->

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@auth
    <div class="left">
        <a class="button" href="{{ route('cabinet.index') }}">В кабинет</a>
    </div>
    <style>
        .left {
            position: fixed;
            right: 15px;
            top: 15px;
            z-index: 9990;
        }

        .button {
            display: inline-flex;
            align-items: center;
            color:#fff;
            height: 35px;
            line-height: 35px;
            padding:0 15px;
            background: #833ab4;
            background: -webkit-linear-gradient(to left ,#fd1d1d, #833ab4, #833ab4);
            background: linear-gradient(to left ,#fd1d1d, #833ab4, #833ab4);
            background-size:200%;
            background-position-x: 100%;
            border-radius: 5px;
            -webkit-transition: all .9s;
            -moz-transition: all .9s;
            -ms-transition: all .9s;
            -o-transition: all .9s;
            transition: all .9s;
        }
    </style>
@endauth



<div class="wrapper">
    <header class="header">
        <div class="container">
            <div class="dates logo">
                <img src="{{ asset('land/images/logo.jpg') }}" alt="Фитнес школа Александра Бататина">
            </div>
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="h-left">
                        <div class="title">
                            Старт 3 июня (7 дней)
                        </div>
                        <div class="dates">
                            <p>Участие из любой точки мира. Нужен только интернет, ноутбук или смартфон</p>
                        </div>
                        <div class="gold-title toptitle">
                            <img src="{{ asset('land/images/title.jpg') }}" alt="Бесплатная Фитнес игра">
                        </div>
                        <div class="dates" id="big">
                            <p><strong class="bolderH">Для тех девушек, кто хочет сделать красивую подтянутую фигуру без стрессов и голодовок</strong></p>
                        </div>
                        <div class="mobs d-md-none">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VrDsaK6Mvu8?rel=0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="order">
                            <a class="blue popup" data-target="#registration-popup" data-toggle="modal" href="#">Хочу участвовать бесплатно</a>
                        </div>
                        <div class="mt-2 pb-3 top-timer">
                            <p class="top-timer_paragraph">Получите Бонус: "Готовое Меню для Похудения"<br>при Регистрации в течении:</p>
                            <span class="clock is-countdown" id="topcount" style="font-size:22px;display:inline;"><span id="countdown" class="countdown-row countdown-amount">00:00</span></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 d-none d-md-block">
                    <div class="h-img" style="">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VrDsaK6Mvu8?rel=0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="teachers">
        <div class="container">
            <div class="title">ВЕДУЩИЕ ФИТНЕС ИГРЫ:</div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <div class="team_wrapper">
                        <div class="team_item">
                            <div class="team_item_img" style="background-image: url({{ asset('team/592c67fa76.png') }})"></div>
                            <div class="team_item_descr">
                                <p class="title">Александр Бататин</p>
                                <p class="subtext">Основатель Фитнес-школы</p>
                                <p class="subtext">Главный Тренер Фитнес Игры</p>
                                <p class="subtext">Специалист в области Диетологии (опыт 8 лет)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="team_wrapper">
                        <div class="team_item">
                            <div class="team_item_img" style="background-image: url({{ asset('team/702dbdc407d4d6ca35e522cb6bd7209f.jpg') }})"></div>
                            <div class="team_item_descr">
                                <p class="title">Ирина Сорокина</p>
                                <p class="subtext">Тренер Фитнес Игры</p>
                                <p class="subtext">Прошла эту программу (похудела на 12кг)</p>
                                <p class="subtext">Чемпионка по Фитнесу</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="team_wrapper">
                        <div class="team_item">
                            <div class="team_item_img" style="background-image: url({{ asset('team/cd7b8e2c32.png') }})"></div>
                            <div class="team_item_descr">
                                <p class="title">Сэрмд Хуссайнс</p>
                                <p class="subtext">Коуч-психолог (опыт 5 лет)</p>
                                <p class="subtext">Поможет тебе получить крутой настрой и сильную мотивацию. Которая позволит тебе получить максимальный результат.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <a class="blue popup click" data-target="#registration-popup" data-toggle="modal" href="#">Хочу участвовать бесплатно</a>
                </div>
            </div>
        </div>
    </section>

    <section class="girls">
        <div class="container">
            <div class="girls-inner">
                <div class="title">Эти девушки уже получили результат по нашей программе</div>
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6 col-xl-6 text-center">
                        <p style="margin-bottom:10px;font-size:18px;"><strong>Это Лена (31 год, молодая мама, г.Киев), она похудела в нашей программе на 17кг</strong></p>
                        <img class="result-img" src="{{ asset('land/images/elena1.jpg') }}" alt="">
                        <p style="margin-bottom:10px;font-size:18px;"><strong>Видео-благодарность Лены после Похудения в нашей программе</strong></p>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/gsjkihcXsbM" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-xl-6 text-center">
                        <p style="margin-bottom:10px;font-size:18px;"><strong>Это Ира (32 года, г.Санкт-Питербург), она похудела на 12кг в нашей программе.</strong></p>
                        <img class="result-img" src="{{ asset('land/images/ira1.jpg') }}" alt="">
                        <p style="margin-bottom:10px;font-size:18px;"><strong>Видео-благодарность Иры после Похудения в нашей программе</strong></p>
                        <div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/NOC7YUyyyX0" frameborder="0" class="embed-responsive-item" allowfullscreen></iframe></div>

                    </div>
                    <div class="col-12 text-center">
                        <div class="order mt-5">
                            <a class="blue popup" data-target="#registration-popup" data-toggle="modal" href="#">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="you">
        <div class="container">
            <div class="you-inner">
                <div class="title">ЗА ВРЕМЯ ФИТНЕС ИГРЫ ТЫ:</div>
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6 col-xl-auto">
                        <div class="y-list">
                            <p>Увидишь свои первые изменения во внешнем виде (уйдёт 2-3 кг, коррекция проблемных зон, мышцы станут более упругими, улучшиться  состояния, здоровье и т.д.)</p>
                            <p>Получишь практические шаги, как сделать фигуру стройной, подтянутой и закрепить этот результат навсегда (без возврата). </p>
                            <p>Узнаешь, как круглый год держать себя в подтянутой форме. При этом не ограничивая себя в еде и кушая вкусняшки:)</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-xl-auto">
                        <div class="y-list">
                            <p>Перестроишь свой режим Питания на грамотный формат. Чтобы кушать вкусно, не голодать и иметь Красивую фигуру. </p>
                            <p>Получишь хорошую мотивацию, «Волшебный пендель»:) Так как параллельно с тренерами Фитнес Школы, будет Коуч-психолог. Который расскажет, как постоянно иметь крутой настрой и высокую мотивацию. Как не опускать руки и не «сливаться». Это Важно! </p>
                            <p>Получишь возможность выиграть 5000 рублей по итогам 7 дней Игры. Выполняя обычные задания. Но самое ценное - это твой грамотный старт.
                            </p>
                        </div>
                    </div>

                    <div class="col-12 text-center">
                        <div class="order mt-5 mb-5">
                            <a class="blue popup" href="#" data-toggle="modal" data-target="#registration-popup">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</a>
                        </div>
                    </div>
                </div>
                <!-- <div class="under-title">
                    Удаленный заработок и неограниченные путешествия<br class="d-none d-md-inline">  станут вашими реальными, а главное, <span>достижимыми</span> целями!
                </div> -->
            </div>
        </div>
    </section>

    <section class="come-on">
        <div class="container">
            <div class="gold-title text-center">
                Только
            </div>
            <div class="title">
                реальные отзывы<br class="d-sm-none"> наших клиентов
            </div>
            <div class="row m-0">
                <!-- <div class="col-12 col-md-6 col-xl-7 p-0">
                    <div class="c-left">
                        <div class="c-img d-none d-md-block">
                            <img src="assets\images\ipad.png" alt="">
                        </div>
                    </div>
                </div> -->
                <div class="col-12 col-md-12 col-xl-6 p-2 offset-xl-3 text-center">
                    <div class="mb-3">
                        <strong>Илона (35 лет, Дортмунд, Германия).</strong> Похудела уже на 5кг в программе Александра
                    </div>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/sssVkomj91I" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-12 col-md-12 col-xl-6 p-2 offset-xl-3 text-center">
                    <div class="mb-3">
                        <strong>Таня (28 лет, Харьков, Украина).</strong> Похудела на 7 кг в программе Александра и закрепила результат навсегда
                    </div>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/P0KjWPyHyU4" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-12 col-md-12 col-xl-6 p-2 offset-xl-3 text-center">
                    <div class="mb-3">
                        <strong>Виктория (37 лет, Чебоксары, Россия).</strong> Похудела уже на 6,5кг в программе Александра
                    </div>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZyIEIdmxSE0" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-12 text-center">
                    <div class="order mb-5 mt-5">
                        <a class="blue popup" data-target="#registration-popup" data-toggle="modal" href="#">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="come-on">
        <div class="container">
            <div class="gold-title text-center">
                УЧАСТВУЙ
            </div>
            <div class="title">
                 В ФИТНЕС ИГРЕ, ЕСЛИ:
            </div>
            <div class="row m-0">
                <div class="col-12 col-md-12 col-xl-6 p-0 offset-xl-3">
                    <div class="c-right">
                        <div class="c-list">
                            <p data-numb="1">Тебе не хватает настроя и мотивации (Волшебного пенделя), чтобы просто начать. </p>
                            <p data-numb="2">Не хватает знаний, опыта и профессионального контроля специалиста (тренера, диетолога, наставника). </p>
                            <p data-numb="3">Ты уже пробовала худеть и это было безрезультатно (ты срывалась, вес возвращался, опускались руки)</p>
                            <p data-numb="4">Ты молодая мама и хочешь быть в форме, быть привлекательной и сексуальной.</p>
                            <p data-numb="5">Тебе не нравится своё отражение в зеркале и не можешь покупать себе модную трендовую одежду (платье) </p>
                            <p data-numb="6">Ты устала садится на разные Диеты, срываться и потом корить себя за это (ругать внутри).</p>
                            <p data-numb="7">Ты хочешь стать более уверенной, повысить самооценку. И стать, в конце концов, крутой,  привлекательной и самодостаточной женщиной:)</p>

                        </div>
                        <div class="order">
                            <a class="blue" href="#" data-toggle="modal" data-target="#registration">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <div class="container mb-5">
        <div class="row">
            <div class="col-12 text-center">
                <div class="order">
                    <a class="blue go-to-block" data-target="#registration" data-toggle="modal" href="#">Зарегистрироваться на мастер-класс</a>
                </div>
            </div>
        </div>
    </div> -->

    <section class="speakers" style="margin-top:-50px;">
        <div class="container">
            <!-- <div class="gold-title">
                ВЕДУЩИЙ МАСТЕР-КЛАССА Александр Бататин
            </div> -->
            <div class="speaker sp-2 row align-items-center">
                <div class="col-12 col-sm-6 order-2 order-sm-2">
                    <div class="sp-img">
                        <img src="{{ asset('land/images/m2-mob.png') }}" alt="">
                    </div>
                    <div class="sp-img">
                        <img src="{{ asset('land/images/m3-mob.png') }}" alt="">
                    </div>
                    <p class="mt-2 mb-2"><strong>Пресс-конференция Александра Бататина по Фитнесу (прямой эфир на 1500 человек)</strong></p>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ClHT_q09qpM" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-12 col-sm-6 order-1 order-sm-1 mb-3">
                    <!-- <div class="sp-text">
                        <div class="title">
                            Александр Бататин
                        </div>
                        <div class="sp-list">
                            <p>Профессиональный тренер</p>
                            <p>Сертифицированный специалист в области похудения и коррекции фигуры</p>
                            <p>Фитнес-эксперт на ТВ-программах</p>
                            <p>Полуфиналист Чемпиоата Мира по фитнесу</p>
                            <p>Помог похудеть 268 девушкам (192 девушки пытались похудеть до этого более 5ти лет)</p>
                        </div>

                    </div> -->
                    <div class="text-justify">
                        <p class="mb-3"><strong>ОСНОВАТЕЛЬ И ИДЕОЛОГ АЛЕКСАНДР БАТАТИН</strong> (профессиональный тренер, сертифицированный специалист в области похудения и коррекции фигуры, полуфиналист Чемпионата Мира по фитнесу, помог похудеть 178 девушкам и 75 парням).</p>
                        <p><strong>Более 8ми лет Александр посвятил</strong> изучению всех деталей и тонкостей женского и мужского похудения. И разработал свою методику (программу), которая позволяет похудеть быстро и закрепить результат навсегда. Без диет, голодовок и сложных тренировок.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <div class="order mt-5">
                        <a class="blue popup" href="#" data-toggle="modal" data-target="#registration-popup">ХОЧУ УЧАСТВОВАТЬ БЕСПЛАТНО</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="possibility">
        <div class="container">
            <div class="poss-inner">
                <div class="poss-top" style="padding:33px 10px 15px 10px !important;">
                    <div class="gold-title">
                        Заполни форму ниже
                    </div>
                    <div class="title">
                        чтобы гарантированно попасть<br class="d-none d-md-inline"> на Фитнес Игру
                    </div>
                </div>

                <div class="row justify-content-center justify-content-sm-between">
                    <div class="col-12">
                        <div class="rawform">
                            <form action="{{ route('register') }}" method="post" id="modalform_raw">
                                @csrf
                                <input placeholder="Введите имя" type="text" name="name" required="">
                                <span class="error-msg"></span>
                                <input placeholder="Введите e-mail" name="email" type="email" required="">
                                <input id="promocode" type="text" name="promocode" value="" class=" " placeholder="Введите промокод если у тебя есть">
                                <!-- <input type="hidden" name="campaign_token" value="UW"> -->
                                <!-- <input type="hidden" name="thankyou_url" value="/intensive/thanks" data-url="/intensive/thanks"> -->
                                <!-- <input name="custom_http_referer2" type="hidden" value="https://domain.com/intensive/"> -->
                                <!-- <select name="webinartime">
                                        <option value="" disabled selected>Выберите время</option>
                                        <option value="12">12:00 МСК</option>
                                        <option value="20">20:00 МСК</option>
                                </select> -->
                                <input type="hidden" name="start_day" value="0">
                                <input type="hidden" name="updatedphone" value="" id="updatedphone">
                                <a href="#" data-toggle="modal" data-target="#registration"></a>
                                <div class="order">
                                    <button class="blue send" type="button">Зарегистрироваться в игре</button>
                                </div>
                            </form>

                            <!-- <div class="p-tit">На почту</div>
                            <div class="social">
                                <a href="#" data-toggle="modal" data-target="#registration" class="s-1">
                                   <i class="far fa-envelope"></i>
                                </a>
                            </div>
                            <p>C регистрацией</p> -->
                        </div>
                    </div>
                    <!---<div class="col-12 col-sm-auto">
                        <div class="p-block">
                            <div class="p-tit">В мессенджер</div>
                            <div class="social">
                                <div class="s-2" onclick="yaCounter47976731.reachGoal('INT_25_27_06_18_FB');">
                                    <script src="//unpkg.com/@textback/notification-widget@latest/build/index.js"></script>
                                    <tb-notification-widget widget-id="59a8bde4-ff43-2eca-0c96-01624e8924c3">
                                    </tb-notification-widget>
                                </div>
                                <div class="vks s-5" onclick="yaCounter47976731.reachGoal('INT_25_27_06_18_VK');">
                                    <script>!window.jQuery && document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>');</script><script src="//senler.ru/web/js/senler.js?2"></script><script>try {Senler.ButtonSubscribe("senlerBtn-1528892197");} catch (e) {console.log(e);}</script><div id="senlerBtn-1528892197" data-vk_group_id="167617204" data-subscription_id="51445" data-text="" data-alt_text=""></div>
                                </div>

                                <a target="_blank" href="" class="s-5">
                                    <i class="fab fa-vk"></i>
                                </a>

                            </div>
                            <p>Без регистрации</p>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">
            <div class="texts row justify-content-center">
                <div class="col-12 col-sm-auto">
                    <p><a target="_blank" href="{{ route('politics') }}">Политика конфиденциальности</a></p>
                </div>
                <div class="col-12 col-sm-auto">
                    <p><a target="_blank" href="{{ route('agreement') }}">Согласие с рассылкой</a></p>
                </div>
                <div class="col-12 col-sm-auto">
                    <p><a target="_blank" href="{{ route('otkaz') }}">Отказ от ответственности</a></p>
                </div>
            </div>
        </div>
    </footer>


    <script src="{{ asset('land/cache/scripts.js') }}"></script>        </div>

<!-- Modal -->
<div class="modal fade" id="registration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-header">
                <div class="title">
                    Заполните форму ниже,<br> чтобы гарантировано попасть в Фитнес Игру
                </div>
            </div>
            <div class="modal-body">
                <form action="{{ route('register') }}" method="post" id="modalform">
                    @csrf
                    <input placeholder="Введите имя" type="text" name="name" required="">
                    <span class="error-msg"></span>
                    <input placeholder="Введите e-mail" name="email" type="email" required="">
                    <input id="promocode" type="text" name="promocode" value="" class=" " placeholder="Введите промокод если у тебя есть">
                    <!-- <input type="hidden" name="campaign_token" value="UW"> -->
                    <!-- <input type="hidden" name="thankyou_url" value="/intensive/thanks" data-url="/intensive/thanks"> -->
                    <!-- <input name="custom_http_referer2" type="hidden" value="https://domain.com/intensive/"> -->
                    <!-- <select name="webinartime">
                            <option value="" disabled selected>Выберите время</option>
                            <option value="12">12:00 МСК</option>
                            <option value="20">20:00 МСК</option>
                    </select> -->
                    <input type="hidden" name="start_day" value="0">
                    <input type="hidden" name="updatedphone" value="" id="updatedphone">
                    <a href="#" data-toggle="modal" data-target="#registration"></a>
                    <div class="order">
                        <button class="blue send" type="button">Зарегистрироваться в игре</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Leave modal -->

<!-- Modal -->
<div class="modal fade" id="registration-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-header">
                <div class="title">
                    Заполни форму ниже,<br> чтобы гарантировано попасть в Фитнес Игру
                </div>
            </div>
            <div class="modal-body">
                <form action="{{ route('register') }}" method="post" id="modalform">
                    @csrf
                    <input placeholder="Введите имя" type="text" name="name" required="">
                    <span class="error-msg"></span>
                    <input placeholder="Введите e-mail" name="email" type="email" required="">
                    <input id="promocode" type="text" name="promocode" value="" class=" " placeholder="Введите промокод если у тебя есть">
                    <!-- <input type="hidden" name="campaign_token" value="UW"> -->
                    <!-- <input type="hidden" name="thankyou_url" value="/intensive/thanks" data-url="/intensive/thanks"> -->
                    <!-- <input name="custom_http_referer2" type="hidden" value="https://domain.com/intensive/"> -->
                    <!-- <select name="webinartime">
                            <option value="" disabled selected>Выберите время</option>
                            <option value="12">12:00 МСК</option>
                            <option value="20">20:00 МСК</option>
                    </select> -->
                    <input type="hidden" name="start_day" value="0">
                    <input type="hidden" name="updatedphone" value="" id="updatedphone">
                    <a href="#" data-toggle="modal" data-target="#registration"></a>
                    <div class="order">
                        <button class="blue send" type="button">Зарегистрироваться в игре</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Leave modal -->

<!--
<script>
    $("html").bind("mouseleave", function () {
        $('#myModal').modal();
        $("html").unbind("mouseleave");

        $('#modal-yes').click(function () {
            $('#registration2').modal();
        });

    });
</script>
-->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content" style="border:10px solid orange;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ФИТНЕС ШКОЛА <br> АЛЕКСАНДРА БАТАТИНА</h4>
            </div>
            <div class="modal-body" style="padding:15px;">
                <p>Пошаговая инструкция (Видео + ПДФ)</p>
                <p style="font-size:1.5em;font-weight:bold;margin-top:10px;">Как быстро Похудеть без Диет и Голодовок</p>
                <div class="row mt-5">
                    <div class="col-6">
                        <div id="modal-yes" data-dismiss="modal" class="text-center modal-btn">
                            <div>
                                <span class="modal-btn-title">Да</span> <br>
                                <span class="modal-btn-desc">Отправить Мне Пошаговый План</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div data-dismiss="modal" id="modal-no" class="text-center modal-btn">
                            <div>
                                <span class="modal-btn-title">Нет</span> <br>
                                <span class="modal-btn-desc">У меня уже Красивая фигура и я все знаю</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12">
                        <span style="font-size:12px;">*Ваши данные никогда не будут переданы третьим лицам</span>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>

    </div>
</div>

<!-- Yes Modal -->
<div class="modal fade" id="registration2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-header">
                <div class="title">
                    Зарегистрируйтесь<br> на мастер-класс
                </div>
            </div>
            <div class="modal-body">
                <form action="{{ route('register') }}" method="post">
                    @csrf
                    <input placeholder="Введите имя" type="text" name="name" required="">
                    <input placeholder="Введите e-mail" name="email" type="email" required="">
                    <input placeholder="Введите телефон" name="phone" type="text" id="telphone2" required="" class="phoneinput">
                    <span class="error-msg"></span>
                    <input type="hidden" name="start_day" value="0">
                    <div class="order">
                        <button class="blue send" type="button">Зарегистрироваться на мастер-класс</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('land/yved/script.js') }}"></script>

<div class="yvedw">
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Ирина Швецова (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Алла Прокудина (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Юлия Батина (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Евгения Сорокина (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Александра Веселова (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Катя Иванчук (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Анжелика Красина (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Зоя Вишневская (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Ольга Самина (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Аня Шестова (успешная регистрация на фитнес игру)</div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // Counter -----------------------
        var timeSpend = 1000*300;

        if (jQuery.cookie('firstOpen') != undefined)
        {
            var interval = ((new Date()).getTime() - jQuery.cookie('firstOpen'));
            if (interval > timeSpend){
                timeSpend = 0;
            } else {
                timeSpend -= interval;
            }
        }
        else
        {
            jQuery.cookie('firstOpen', (new Date()).getTime(), { expires: 30 });
        }

        console.log('asd',timeSpend);
        jQuery('#topcount').countdown({until: timeSpend/1000, format: 'M:S', compact: true, onExpiry: butthide});

        if (timeSpend < 1){
            butthide();
        }

        function butthide(){
            //$('.with').remove();
        }

        // Дата сегодня



        $('.date p').text(setTime);

    });
</script>


<script src="{{ asset('land/cache/moment-with-locales.min.js') }}"></script>
<script>
    let d = new Date();
    // let currentTime = parseInt(moment().format('H:mm'));
    let setTime;
    let moscowTimeUTC = d.getUTCHours() + 3;

    if (moscowTimeUTC >= 18) {
        setTime = moment().add("1", "days").locale("ru").format("DD MMMM") + " 21:00 МСК / 20:00 по Киеву";
        // setTime = moment().add("1", "days").locale("ru").format("DD MMMM") + " 12:00 / 20:00 МСК"
    } else {
        setTime = moment().locale("ru").format("DD MMMM") + " 21:00 МСК / 20:00 по Киеву"
        // setTime = moment().locale("ru").format("DD MMMM") + " 12:00 / 20:00 МСК"
    }
</script>
<!-- <script src="assets\cache\jquery.maskedinput.js"></script>

<script>
jQuery(function($){
    $("input[name='phone']").mask("+99999999999?9");
});
</script> -->

<style>
    .hide {display:none;}
    .error-msg {display:block;margin-bottom:10px;color:red;}
</style>

<script src="{{ asset('land/cache/intlinput/js/intlTelInput.js') }}"></script>
<script src="{{ asset('land/cache/intlinput/js/utils.js') }}"></script>
<script src="{{ asset('land/cache/timer.js') }}?{{ d_time() }}"></script>
<script>
    var errorMsg = $('.error-msg');
    var errorMap = [ "Неаравильный формат номера", "Неправильный код страны", "Слишком короткий номер", "Слишком длинный номер", "Номер введён неверно"];

    var input = document.querySelector("#telphone");
    var iti = window.intlTelInput(input, {
        autoPlaceholder: "aggressive",
        // onlyCountries: ["ru", "ua"],
        preferredCountries: ["ru", "ua"],
        nationalMode: false,
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io?token=ca0ce32dc4dc2f', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
    });

    var tel2 = document.querySelector("#telphone2");
    var iti2 = window.intlTelInput(tel2, {
        autoPlaceholder: "aggressive",
        // onlyCountries: ["ru", "ua"],
        preferredCountries: ["ru", "ua"],
        nationalMode: false,
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io?token=ca0ce32dc4dc2f', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
    });

    var tel3 = document.querySelector("#telphone3");
    var iti3 = window.intlTelInput(tel3, {
        autoPlaceholder: "aggressive",
        // onlyCountries: ["ru", "ua"],
        preferredCountries: ["ru", "ua"],
        nationalMode: false,
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io?token=ca0ce32dc4dc2f', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
    });

    // $('.phoneinput').on('blur', function(e) {
    //     if (this.value.trim()) {
    //         if (iti.isValidNumber()) {
    //             console.log('Valid');
    //         } else {
    //             var errorCode = iti.getValidationError();
    //             $('.error-msg').html(errorMap[errorCode]);
    //             $(e.target).closest('form').find('.error-msg').removeClass("hide");
    //         }
    //     }
    //     reset();
    // });

    // $('.phoneinput').on('change', reset);
    // $('.phoneinput').on('keyup', reset);

    // function reset() {
    //     $('.error-msg').html("");
    //     $('.error-msg').addClass("hide");
    // }
</script>

</body>
</html>
