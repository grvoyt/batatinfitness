@extends('cabinet.layouts.app')
@section('title', __('team.title'))
@section('description', __('team.description') )
@section('content')
    <h1>ВЕДУЩИЕ ФИТНЕС&nbsp;-&nbsp;ИГРЫ</h1>

    <section>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="team_wrapper">
                    <div class="team_item">
                        <div class="team_item_img" style="background-image: url({{ asset('team/592c67fa76.png') }})"></div>
                        <div class="team_item_descr">
                            <p class="title">Александр Бататин</p>
                            <p class="subtext">Основатель Фитнес-школы</p>
                            <p class="subtext">Главный Тренер Фитнес Игры  Специалист в области Диетологии (опыт 8 лет)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="team_wrapper">
                    <div class="team_item">
                        <div class="team_item_img" style="background-image: url({{ asset('team/702dbdc407d4d6ca35e522cb6bd7209f.jpg') }})"></div>
                        <div class="team_item_descr">
                            <p class="title">Ирина Сорокина</p>
                            <p class="subtext">Тренер Фитнес Игры</p>
                            <p class="subtext">Прошла эту программу (похудела на 12кг)  Чемпионка по Фитнессу</p>
                        </div>
                    </div>
                    <div class="team_video">
                        <h3 class="team_video_title">Приглашение Ирины на Фитнес Игру🏆</h3>
                        <div class="team_item_video">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/5q7sH_frGos?rel=0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="team_wrapper">
                    <div class="team_item">
                        <div class="team_item_img" style="background-image: url({{ asset('team/cd7b8e2c32.png') }})"></div>
                        <div class="team_item_descr">
                            <p class="title">Сэрмд Хуссайнс</p>
                            <p class="subtext">Коуч-психолог Фитнес-Игры</p>
                            <p class="subtext">Помогает разобраться со своими эмоциями, сделать крутой настрой и высокую мотивацию. Чтобы жить на драйве и иметь 100% результатов всем!</p>
                        </div>
                    </div>
                    <div class="team_video">
                        <h3 class="team_video_title">Приглашение Сэрмда на Фитнес Игру🏆</h3>
                        <div class="team_item_video">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZNE27apmUj8?rel=0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
