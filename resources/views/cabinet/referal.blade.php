@extends('cabinet.layouts.app')
@section('title', "Зарабатывай с Нами")
@section('description', "Зарабатывай с Нами" )
@section('content')
    <h1>Зарабатывай с Нами</h1>

    @if(Auth::user()->is_partner)
        <section>
            <div class="section_item w70">
                <div class="section_item_wrap">
                    <h3>Посмотри сейчас это видео!</h3>
                    <div class="section_video mb40  ">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UcXGo30L2JU?rel=0" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="profile_main_affiliate mb30">
                        <div class="profile_main_affiliate--title">
                            <p>Реф.ссылка на Бесплатную Фитнес Игру</p>
                        </div>
                        <div class="profile_main_affiliate_group">
                            <input id="ref1" readonly type="text" value="{{ route('fitness_ref',$promocod) }}">
                            <button type="button" class="copy-button" data-clipboard-action="copy" data-clipboard-target="#ref1">
                                <i class="far fa-copy copy__icon"></i>
                                <span class="copy_success">Скопировано!</span>
                            </button>
                        </div>
                    </div>
                    <div class="profile_main_affiliate mb30">
                        <div class="profile_main_affiliate--title">
                            <p>Реф.ссылка на Комплексный Тренинг (30 дней)</p>
                        </div>
                        <div class="profile_main_affiliate_group">
                            <input id="ref2" readonly type="text" value="{{ route('trening_ref', $promocod) }}">
                            <button type="button" class="copy-button" data-clipboard-action="copy" data-clipboard-target="#ref2">
                                <i class="far fa-copy copy__icon"></i>
                                <span class="copy_success">Скопировано!</span>
                            </button>
                        </div>
                    </div>
                    <div class="profile_main_affiliate mb40">
                        <div class="profile_main_affiliate--title">
                            <p>Промокод</p>
                        </div>
                        <div class="profile_main_affiliate_group">
                            <input id="ref3" type="text" value="{{ $promocod }}">
                            <button type="button" class="copy-button" data-clipboard-action="copy" data-clipboard-target="#ref3">
                                <i class="far fa-copy copy__icon"></i>
                                <span class="copy_success">Скопировано!</span>
                            </button>
                            <button type="button" data-input="#ref3" class="button save js-savePromocode" >
                                <i class="fas fa-save"></i>
                                <span class="copy_success">Сохранить!</span>
                            </button>
                        </div>
                    </div>
                    <div class="section_lined mb30">
                        <p class="section_text"><i class="fas fa-user-friends"></i> Приглашенные на Бесплатную Фитнес Игру</p>
                        <div class="section_lined_right human">{{ $not_payed_partners }}</div>
                    </div>
                    <div class="section_lined mb30">
                        <p class="section_text"><i class="fas fa-user-friends"></i> Оплатили Участие в Тренинге (30 дней)</p>
                        <div class="section_lined_right human">{{ $payed_partners }}</div>
                    </div>
                    <div class="section_lined mb40">
                        <p class="section_text"><i class="fas fa-user-friends"></i> Мои Бонусы (фитнес-баллы)</p>
                        <div class="section_lined_right balance">{{ $balance }}</div>
                        <span>* 1 рубль российский = 10 фитнес-баллов</span>
                    </div>
                    <p class="section_text">Чтобы подат Заявку на вывод фитнес-балов - напишите нашему менеджеру:</p>
                    <div class="request_wrap">
                        <form action="{{ route('cabinet.referal.amount') }}" method="POST">
                            @csrf
                            <input class="input" type="number" name="amount" placeholder="Введите кол-во баллов" value="0">
                            <button class="button" type="submit"><i class="fas fa-comments-dollar"></i> Заявка на вывод</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    @else
    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <h3 class="mb15">Партнёрство с нами:</h3>
                <div class="section_video mb30">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UcXGo30L2JU?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
                <h3 class="mb15">Наша миссия и ценности:</h3>
                <div class="section_video mb30">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VFRr33Kgbh8?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
                <p class="section_text">Если ты хочешь быть в нашей команде, быть партнёром “batatinfitness” и зарабатывать от 10000руб./мес. (дистанционно) - заполни эту Анкету!</p>
                <a href="https://forms.gle/de5SzqeKnp6pDA9r5" target="_blank" class="button">Заполнить Анкету</a>
            </div>
        </div>
    </section>
    @endif

    @include('cabinet.layouts.swal')
@include('cabinet.layouts.copy')
    @push('js')
    <script>
        $('.js-savePromocode').on('click', function(e) {
            var promo = $( $(this).data('input') ).val();
            var button = $(this);

            $.ajax({
                url: '{{ route('cabinet.referal.change') }}',
                type:'post',
                data: {
                    _token: $('[name="csrf-token"]').attr('content'),
                    promo: promo,
                    action: 'promo'
                },
                beforeSend: function () {
                    button.prop('disabled',true)
                },
                success: function(res) {
                    if(res.response) {
                        swal({
                            icon:'success',
                            title: res.response
                        });
                    }
                },
                complete: function() {
                    button.prop('disabled',false)
                }
            }).fail(function(error) {
                var res = error.responseJSON.errors.promo[0];
                swal({
                    icon:'error',
                    title: res
                })
            })
        })
    </script>
    @error('amount')
    <script>swal({icon:'error',title:'{{ $message }}'})</script>
    @enderror
    @endpush
@endsection
