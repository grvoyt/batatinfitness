@extends('cabinet.layouts.app')
@section('title', "Настройки")
@section('description', "Настройки" )
@section('content')
    <h1>Настройки</h1>

    <section>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <h3>Сменить пароль</h3>

                <form action="{{ route('cabinet.settings.reset') }}" method="POST" class="section_form">
                    @csrf
                    <div class="auth_form_group">
                        <input type="password"
                               class="form-control @error('password') is-invalid @enderror"
                               name="password" required autocomplete="current-password" placeholder="Ваш пароль">

                        @error('password')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="auth_form_group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               placeholder="Повторно пароль" required autocomplete="new-password">
                    </div>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Сменить пароль</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <h3>Аватарка</h3>
                <div class="section_img js-avatar" style="background-image: url({{ asset($user->info->img ?? 'img/no-avatar.jpg') }})"></div>
                <form id="setAvatar" enctype="multipart/form-data" action="{{ route('cabinet.settings.avatar') }}" method="POST" class="section_form">
                    @csrf
                    <input type="file" accept="image/*" name="avatar" id="avatar">
                    <label for="avatar">
                        <i class="fas fa-cloud-download-alt"></i> Выбрать аватар
                        <p class="small">фото видно только на ПК</p>
                    </label>
                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Сменить аватар</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if(session('success'))
    <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
@endif
    @error('avatar')
    <script>swal({icon:'error',title:'{{ $message }}'});</script>
    @enderror

@push('js')
    <script>
        $('#setAvatar').on('submit', function(e) {
            e.preventDefault();
            var form = $(this);
            var file = form.find('[type="file"]').prop('files');
            if( file.length == 0 ) return false;

            var file_size = file[0].size/1024/1024;

            if( file_size >= 3 ) {
                swal({
                    icon:'error',
                    title:'Файл больше 3 Мб',
                });
                return false;
            }

            this.submit();
        });
        $('#avatar').on('change', function(e) {
            var file = $(this).prop('files')[0];
            var file_size = file.size/1024/1024;
            if( file_size >= 3 ) {
                swal({
                    icon:'error',
                    title:'Файл больше 3Мб',
                });
                return false;
            }
            getBase64(file).then(function(imgBase64) {
                $('.js-avatar').attr('style','background-image: url('+imgBase64+')');
            });
        });

        function getBase64(file) {
            return new Promise((res,rej) => {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    res(reader.result)
                };
                reader.onerror = function (error) {
                    rej(error);
                };
            });
        }
    </script>

@endpush
@endsection
