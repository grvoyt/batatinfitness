@extends('cabinet.layouts.app')
@section('title', "Бонусы")
@section('description', "Бонусы" )
@section('content')
    <h1>Бонусы</h1>

    <section>

        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="section_video mb0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/biwlsSawTFE?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="section_with_button">
                    <h4>Больше материала здесь</h4>
                    <a href="https://drive.google.com/drive/folders/1aSoAP0NOsMqZ4pR5cmMok5NRxg2lkbHU" target="_blank" class="button"><i class="fas fa-external-link-square-alt"></i> Больше материала здесь</a>
                </div>
            </div>
        </div>
    </section>
@endsection
