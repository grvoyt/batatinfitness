@extends('cabinet.layouts.app')
@section('title', "Отзывы")
@section('description', "Отзывы" )
@section('content')
    <h1>Онлайн фитнес школа Александра Бататина</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="profile_video_title">
                    <h4>Листай ниже и смотри отзывы учениц😉</h4>
                </div>
                <div class="profile_video_wrap">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/cZ-J8aVIEKA?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="sectction_img_text">
                    <div class="sectction_img_text--img w30">
                        <img src="{{ asset('admin/img/batatin.jpg') }}" alt="">
                    </div>
                    <div class="sectction_img_text--text w70">
                        <p class="section_text">Я рад видеть тебя в нашем проекте! :) На протяжении последних 8-ми лет я помогаю девушкам улучшить свою фигуру, внешний вид, физическое состояние и здоровья в целом!</p>
                        <p class="section_text">И мне очень приятно, что мой подход (методика), как тренера и специалиста, действительно помогает огромному количеству девушек добиться крутого результата!</p>
                        <p class="section_text">Это за слуга не только моя, но и всей моей команды специалистов! Благодарен вам, друзья!</p>
                        <p class="section_text">Также! Благодарен тебе, что доверилась наше команде и участвуешь в нашем проекте:)</p>
                        <p class="section_text">Ниже малая часть успешных отзывов участниц нашей школы! Уверен, что в ближайшее время, твой Отзыв будет здесь!:)</p>
                        <p class="section_text">Удачи во всем! (Александр Бататин)</p>
                    </div>
                </div>

            </div>
        </div>
        @foreach($reviews as $review)
            <div class="section_item w50">
                <div class="section_item_wrap">
                    <div class="profile_video_title">
                        <h4>{{ $review->name }}</h4>
                    </div>
                    <div class="profile_video_wrap">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $review->video_id }}?rel=0" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
@endsection
