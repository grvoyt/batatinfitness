@extends('cabinet.layouts.app')
@section('title', __('profile.title'))
@section('description', __('profile.description') )
@section('content')
    <h1>Расписание марафона</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="profile_video_wrap">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ewOMGbH5T7Q?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        @foreach($schedules as $schedule)
        <div class="section_item">
            <div class="section_item_wrap check">
                <h2>{{ Carbon::parse($schedule->date)->format('H:i d.m.Y') }}</h2>
                <h4 class="mb20 ">{{ $schedule->title }}</h4>
                @if( $schedule->host )
                    <div class="section_text bold">
                        <p>{{ $schedule->host }}</p>
                    </div>
                @endif
                <div class="section_text">{!! $schedule->text !!} </div>
            </div>
        </div>
        @endforeach
    </section>
@endsection
