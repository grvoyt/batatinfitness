@extends('cabinet.layouts.app')
@section('title', "Задания")
@section('description', "Задания" )
@section('content')
    <h1>Задания</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="profile_video_wrap mb0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/NRF9X4yI1ac?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="section_lined mb30">
                    <p class="section_text"><i class="fas fa-user-friends"></i> Всего заработала баллов:</p>
                    <div class="section_lined_right balls">{{ $ball }}</div>
                    <span>И стала на 1 шаг ближе к Победе :)</span>
                </div>
            </div>
        </div>
        @foreach($tasks as $task)
            @continue( !$task->is_opened )
        <div class="section_item">
            <div class="section_item_wrap task">
                <i class="fas fa-{{ $task->is_done ? 'check' : 'hourglass' }}"></i>
                <div class="section_with_button">
                    <h4>{{ $task->name }} </h4>
                    @if(!$task->is_done)
                    <a href="{{ route('cabinet.game.task_show', $task->id) }}" class="button">
                        <i class="fas fa-external-link-square-alt"></i> Посмотреть
                    </a>
                    @else
                        <span class="ball_for">+{{ $points[$task->id] }}</span>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </section>

    @include('cabinet.layouts.swal')
    @if(session('error'))
    <script>
        swal({icon:'error',title:'{{ session('error') }}'})
    </script>
    @endif
@endsection
