@extends('cabinet.layouts.app')
@section('title', __('profile.title'))
@section('description', __('profile.description') )
@section('content')
    <h1>Марафон</h1>

    <section>
        <div class="section_item w30">
            <a href="{{ route('cabinet.marafon.rules') }}" class="button_img" style="background-image: url({{ asset('img/rasp.jpg') }})">
                <p>Правила</p>
            </a>
        </div>
        <div class="section_item w30">
            <a href="{{ route('cabinet.marafon.schedule') }}" class="button_img" style="background-image: url({{ asset('img/rasp2.jpg') }})">
                <p>Расписание</p>
            </a>
        </div>
        <div class="section_item w30">
            <a href="{{ route('cabinet.marafon.tasks') }}" class="button_img" style="background-image: url({{ asset('img/rasp3.jpg') }})">
                <p>Твои задания</p>
            </a>
        </div>
    </section>

@endsection
