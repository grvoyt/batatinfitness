@extends('cabinet.layouts.app')
@section('title', $task->name )
@section('description', $task->name )
@section('content')

    <div class="loading" style="display:none">
        <img src="{{ asset('admin/img/loading.svg') }}" alt="">
    </div>
    <h1>{{ $task->name }}</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <h2>Задание</h2>
                @if($video)
                <div class="section_video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $video }}?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
                @endif
                <div class="section_text">{!! $task->text  !!}</div>
                @if($show_form)
                <form id="task" action="{{ route('cabinet.game.task_store', $task->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" accept="image/*" name="img" id="img">
                    <label for="img">
                        <i class="fas fa-cloud-download-alt"></i> Выбрать Фото
                    </label>
                    <div class="auth_form_group">
                        <textarea name="answer" id="" cols="30" rows="10" placeholder="Ваш коментарий">{{ old('answer') ?? $task_info->answer ?? '' }}</textarea>
                    </div>

                    <div class="auth_form_group">
                        <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Сохранить ответ и получить балы</button>
                    </div>
                </form>
                @else

                @endif
            </div>
        </div>
    </section>

    @push('js')
    <script>
        $('#task').on('submit', function(e) {
            e.preventDefault();
            $('.loading').show();
            this.submit();
        })
    </script>
    @endpush

    @include('cabinet.layouts.swal')
    @error('answer')
    <script>swal({icon:'error',title:'{{ $message }}'})</script>
    @enderror
@endsection
