@extends('cabinet.layouts.app')
@section('title', "Баланс")
@section('description', "Баланс" )
@section('content')
    <h1>Только для партнёров</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <h3 class="mb15">Партнёрство с нами:</h3>
                <div class="section_video mb30">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UcXGo30L2JU?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
                <h3 class="mb15">Наша миссия и ценности:</h3>
                <div class="section_video mb30">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VFRr33Kgbh8?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
                <p class="section_text">Если ты хочешь быть в нашей команде, быть партнёром “batatinfitness” и зарабатывать от 10000руб./мес. (дистанционно) - заполни эту Анкету!</p>
                <a href="https://forms.gle/de5SzqeKnp6pDA9r5" target="_blank" class="button">Заполнить Анкету</a>
            </div>
        </div>
    </section>
@endsection
