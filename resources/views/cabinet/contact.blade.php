@extends('cabinet.layouts.app')
@section('title', __('contact.title'))
@section('description', __('contact.description') )
@section('content')
    <h1>Контакты</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="section_video mb0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PyArxMlfNPo?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="section_with_button">
                    <h5>Подпишись на наш Инстаграм. Чтобы быть в Курсе самой ценной информации</h5>
                    <a href="https://www.instagram.com/batatinfitness/?igshid=15f6z8x2umlv4" target="_blank" class="button"><i class="fab fa-instagram"></i> Подписаться</a>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="section_with_button">
                    <h5>Подпишись на Закрытый Канал Телеграм. Чтобы не пропустить информацию и важные Новости Фитнес Игры. </h5>
                    <a href="https://tele.click/joinchat/AAAAAE3dfOmlQarvrPej9g" target="_blank" class="button"><i class="fas fa-bullhorn"></i> Подписаться</a>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="section_with_button">
                    <h5>По вопросам партнёрства (заработка вместе в нашей команде) пиши сюда!</h5>
                    <a href="https://tele.click/Vikkifitness" target="_blank" class="button"><i class="fas fa-comments"></i> Телеграм чат</a>
                </div>
            </div>
        </div>
        <div class="section_item">
            <div class="section_item_wrap">
                <div class="section_with_button">
                    <h5>По всем техническим вопросам пиши сюда. Чтобы участие в Игре было максимально комфортным. </h5>
                    <a href="https://tele.click/Vikkifitness" target="_blank" class="button"><i class="fas fa-comments"></i> Телеграм чат</a>
                </div>
            </div>
        </div>
    </section>
@endsection
