@extends('cabinet.layouts.app')
@section('title', __('profile.title'))
@section('description', __('profile.description') )
@section('content')
    <h1>Ура:) Ты в Фитнес-Игре<br>(старт 10 июня)</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="profile_video">
                    <div class="profile_video_wrap mb0">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/atVM5fid0Hg?rel=0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section_item w50">
            <div class="section_item_wrap">
                <div class="profile_video_title">
                    <h4 class="">Скопируй и скинь эту ссылку 3м подругам, чтобы они тоже были получили крутой результат!</h4>
                </div>
                <p class="section_text">И получи доступ в Комплексный Тренинг (30 дней) Бесплатно. Старт онлайн-тренинга по Коррекции Фигуры будет  24 июня (30 дней).</p>
                <div class="profile_main_affiliate_group">
                    <input id="ref2" readonly type="text" value="{{  route('fitness_ref',$user->info->referal_link) }}">
                    <button type="button" class="copy-button" data-clipboard-action="copy" data-clipboard-target="#ref2">
                        <i class="far fa-copy copy__icon"></i>
                        <span class="copy_success">Скопировано!</span>
                    </button>
                </div>
                <div class="section_lined">
                    <p class="section_text"><i class="fas fa-user-friends"></i> Пригласила подруг</p>
                    <div class="section_lined_right human">{{ $not_paid }}</div>
                </div>
            </div>
        </div>

        <div class="section_item w50">
            <div class="section_item_wrap">
                <div class="social">
                    <h4>Подпишись обязательно на нас в социальных сетях прямо сейчас:</h4>
                    <div class="social_tabl">
                        <a href="https://www.instagram.com/batatinfitness/?igshid=15f6z8x2umlv4" class="social_item">
                            <i class="fab fa-instagram"></i>
                            <p>Инстаграм</p>
                        </a>
                        <a href="https://t.me/joinchat/AAAAAE3dfOmlQarvrPej9g" class="social_item">
                            <i class="fab fa-telegram"></i>
                            <p>Телеграм</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@include('cabinet.layouts.copy')
@endsection
