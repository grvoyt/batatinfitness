@extends('cabinet.layouts.app')
@section('title', "Прямой эфир")
@section('description', "Прямой эфир" )
@section('content')
    <h1>Прямой эфир</h1>

    <section>
        <div class="section_item w70">
            <div class="section_item_wrap">
                <div class="section_video mb0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CjHeIaIBN-o?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        @forelse($webinars as $webinar)
        <div class="section_item w70">
            <div class="section_item_wrap">
                <h3>{{ $webinar->title }}</h3>
                <div class="section_video mb0">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $webinar->link }}?rel=0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        @empty
            <div class="section_item w70">
                <p>Пока нет записей</p>
            </div>
        @endforelse
    </section>
@endsection
