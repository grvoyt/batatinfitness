<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@if(session('success'))
    <script>swal({icon:'success',title:'{{ session('success') }}'});</script>
@endif
