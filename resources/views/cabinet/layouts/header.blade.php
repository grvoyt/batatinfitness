<header>
    <div class="header_left">
        @auth<button class="menu_button"><span><div></div></span><p>Меню</p></button>@endif
        <div class="header_logo" style="margin-top: 3px;">batatinfitness</div>
    </div>
    @auth
    <div class="header_right">
        <div class="profile">
            @if(Auth::user()->is_partner)
            <div class="profile_wallet">
                <i class="fas fa-wallet"></i>
                <span class="balance">{{ Cache::get('balance_'.Auth::user()->id,0) }}</span>
            </div>
            @endif
            <div class="profile_name">
                <img src="{{ asset(Cache::get('user_img_'.Auth::id(),'img/no-avatar.jpg')) }}" alt="" class="profile_name_img">
                {{ Auth::user()->name }}
            </div>
            <a href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i> {{ __('main.logout') }}</a>
        </div>
    </div>
    @endif
</header>
