@include('cabinet.layouts.head')
<body>
@include('cabinet.layouts.header')
@include('cabinet.layouts.menu')
<div class="content_body">
    @yield('content')
    @include('cabinet.layouts.footer')
</div>
<!-- scripts -->
<script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="{{ asset('admin/js/all.js') }}?{{ d_time() }}"></script>
<script src="{{ asset('admin/js/app.js') }}?{{ d_time() }}"></script>
@stack('js')
</body>
</html>
