@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>
    <script>
        var clipboard = new ClipboardJS('.copy-button');

        clipboard.on('success', function(e) {
            var alert = $(e.trigger).find('.copy_success');
            alert.addClass('show');
            setTimeout(function() {
                alert.removeClass('show');
            },1000);
        });

        clipboard.on('error', function(e) {
            console.error('Action:', e.action);
            console.error('Trigger:', e.trigger);
        });
    </script>
@endpush
