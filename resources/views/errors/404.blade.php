@extends('errors.minimal')

@section('title', "Страница не найдена | ".env('APP_NAME'))
@section('code', '404')
@section('message', "Страница не найдена")
