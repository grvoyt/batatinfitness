<!DOCTYPE html>
<html>
<head>
    <title>Как похудеть девушке без диет и тренировок</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Как похудеть девушке без диет и тренировок">
    <meta name="description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
    <link rel="image_src" href="assets/images/header-pic.jpg">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Женский онлайн Мастер-класс «Как похудеть без диет и голодовок»">
    <meta property="og:description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
    <meta property="og:image" content="http://batatinfitness.club/assets/images/header-pic.png">
    <meta property="og:url" content="http://batatinfitness.club">
    <meta property="og:site_name" content="Как похудеть девушке без диет и тренировок">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Как похудеть девушке без диет и тренировок">
    <meta name="twitter:description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
    <meta name="twitter:image" content="http://batatinfitness.club/assets/images/header-pic.png">

    <meta itemprop="name" content="Как похудеть девушке без диет и тренировок">
    <meta itemprop="description" content="Женский онлайн Мастер-класс, на котром мы разберем основные шаги, которые нужно сделать, чтобы похудеть и сделать красивую форму. Также советы и рекомендации, которые позволят всегда быть в красивой подтянутой форме.">
    <meta itemprop="image" content="http://batatinfitness.club/assets/images/header-pic.png">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#d6c4a0">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#d6c4a0">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#d6c4a0">

    <link rel="stylesheet" href="{{ asset('land/cache/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('land/cache/intlinput/css/intlTelInput.css') }}">
    <link rel="stylesheet" href="{{ asset('land/yved/style.css') }}">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHGZMWP');</script>
    <!-- End Google Tag Manager -->

</head>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHGZMWP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<body class="thanks">
<div class="wrapper">
    <section class="thanks-header">
        <div class="container">
            <div class="th-top">
                <div class="date d-inline-flex flex-wrap flex-sm-nowrap justify-content-center">
                    <p>Бесплатный онлайн мастер-класс</p>
                </div>
                <!-- <div class="title">
                    Как похудеть девушке
                </div>
                <div class="gold-title">
                    без диет и тренировок
                </div> -->
            </div>
            <div class="mobs d-none">
                <img src="{{ asset('land/images/h-mob.png') }}" alt="">
            </div>
            <div class="th-bottom">
                <div class="th-inner">
                    <div class="row justify-content-xl-between">
                        <div class="col-12 col-sm-6">
                            <div class="title">
                                Поздравляем, вы успешно<br class="d-none d-xl-inline"> подтвердили участие<br class="d-none d-xl-inline"> в мастер-классе
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="text">
                                <p>В течение 5-ти минут на указанный email<br class="d-none d-xl-inline"> придёт письмо с подтверждением регистрации (проверьте почту).<br> <strong>А сейчас посмотрите обязательно это видео ниже!</strong></p>
                                <div class="gold-title">
                                    До встречи!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="with">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-8 offset-md-2">
                    <div class="embed-responsive embed-responsive-16by9 mb-3">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/jdjDN7eIbl4" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <p class="w-text">
                <strong>ВОЗМОЖНО, ВЫ ХОТИТЕ НАЧАТЬ МЕНЯТЬ СВОЮ ФИГУРУ ПРЯМО СЕЙЧАС?</strong><br>
                Тогда предлагаем Вам мини-курс: «Как скинуть несколько килограмм уже за 3 дня Без изменения пищевых привычек?»
            </p>

            <div class="title d-none d-sm-block">Благодаря этому курсу вы:</div>
            <div class="row justify-content-center">
                <div class="col-12 col-sm-5 col-md-6">
                    <div class="wt-img">
                        <img src="{{ asset('land/images/book.png') }}" alt="">
                    </div>
                    <div class="proposal d-xl-none">
                        <div class="time d-flex justify-content-center align-items-center">
                            <p>Предложение доступно ещё:</p>
                            <div class="clock"></div>
                        </div>
                        <div class="order">
                            <a class="blue" href="#" data-toggle="modal" data-target="#pay">
                                Начать менять фигуру прямо сейчас всего за $3
                            </a>
                        </div>
                    </div>
                    <div class="mt-5 mb-5">
                        <img src="{{ asset('land/images/otziv1.png') }}" alt="" class="rounded-circle w-50">
                        <p class="mt-3 mb-3"><strong>Елена Петрова</strong></p>
                        <p>"Не думала, что за такую маленькую цену можно получить Программу, которая даёт Результат. Я похудела на 2,5 за неделю и наладила Питание. Энергии хоть отбавляй. Благодарна Вам, Александр!"</p>
                    </div>
                    <div class="mt-5 mb-5">
                        <img src="{{ asset('land/images/otziv2.png') }}" alt="" class="rounded-circle w-50">
                        <p class="mt-3 mb-3"><strong>Таня Бережнюк</strong></p>
                        <p>"Очень рада, что мой вес сдвинулся с мертвой точки после родов. За неделю минус 2,4кг. Это при том, что я постоянно ем. Уже записалась на месячную Программу. Удачи;)"</p>
                    </div>
                </div>
                <div class="col-12 col-sm-7 col-md-6">
                    <div class="title d-sm-none">Благодаря этому<br class="d-sm-none"> курсу вы:</div>
                    <div class="w-block">
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Похудеете на  несколько килограмм уже за 3 дня по нашей методике</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap align-items-center">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Получите пошаговый план Питания на основе обычных недорогих продуктов</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap align-items-center">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Почувствуете прилив энергии и нормализуете свой сон</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Подтяните свои проблемные зоны (часть тела)</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Получите 15 проверенных рекомендаций для похудения</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Получите пошаговую программу питания на 7 дней + практические советы</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Получите фитнес-дневник для записей ваших целей, задач, замеров и прогресса</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Получите Чек-лист (программу), что нужно вам делать если вы сорвались / загуляли. Чтобы это не повлияло на фигуру</p>
                        </div>
                        <div class="w-line d-flex flex-wrap flex-sm-nowrap">
                            <div class="wl-img">
                                <img src="{{ asset('land/images/th3.png') }}" alt="">
                            </div>
                            <p>Получите комплекс из 7ми легких, но эффективных упражнений для дома, без тренажёров и доп.оборудования</p>
                        </div>
                        <div class="proposal">
                            <div class="time d-flex justify-content-center align-items-center">
                                <p>Предложение доступно ещё:</p>
                                <div class="clock"></div>
                            </div>
                            <div class="order">
                                <a class="blue" href="#" data-toggle="modal" data-target="#pay">
                                    Начать менять фигуру прямо сейчас всего за $3
                                </a>
                            </div>
                        </div>
                        <p class="mt-4 mb-4 text-center"><strong>ЭТИ ДЕВУШКИ УЖЕ ПРОХОДЯТ<br> МИНИ-КУРС И ХУДЕЮТ</strong></p>
                        <div class="embed-responsive embed-responsive-16by9 mb-2">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dNdujg0TDV8" allowfullscreen></iframe>
                        </div>
                        <div class="embed-responsive embed-responsive-16by9 mb-2">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZHcIgr5lC9s" allowfullscreen></iframe>
                        </div>
                        <div class="embed-responsive embed-responsive-16by9 mb-2">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/5aYk2yT0upo" allowfullscreen></iframe>
                        </div>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/QN0mLOACMNQ" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="proposal">
                <div class="time d-flex justify-content-center align-items-center">
                    <p>Предложение доступно ещё:</p>
                    <div class="clock"></div>
                </div>
                <div class="order">
                    <a class="blue" href="#" data-toggle="modal" data-target="#pay">
                        Начать менять фигуру прямо сейчас всего за $3
                    </a>
                </div>
                <div class="mt-5 mb-5">
                    <img src="{{ asset('land/images/otziv3.png') }}" alt="" class="rounded-circle w-50">
                    <p class="mt-3 mb-3"><strong>Ирина Гармата</strong></p>
                    <p>"Саша! Спасибо за Мини-Курс по похудению. Результат - это минус 3 кг за неделю. Спасибочки тебе:)"</p>
                </div>
                <div class="mt-5 mb-5">
                    <img src="{{ asset('land/images/otziv4.png') }}" alt="" class="rounded-circle w-50">
                    <p class="mt-3 mb-3"><strong>Юлия Бондарчук</strong></p>
                    <p>"За неделю в Талии минус 3см) Спасибо Вам за такую удобную онлайн-программу по похудению)) Все четко и конкретно, без лишней воды. И очень удобно, что есть видео-уроки с примерами!"</p>
                </div>
                <div class="proposal">
                    <div class="time d-flex justify-content-center align-items-center">
                        <p>Предложение доступно ещё:</p>
                        <div class="clock"></div>
                    </div>
                    <div class="order">
                        <a class="blue" href="#" data-toggle="modal" data-target="#pay">
                            Начать менять фигуру прямо сейчас всего за $3
                        </a>
                    </div>
                </div>
                <p>Это разовое предложение.<br> Оно действует только сейчас и только на этой странице.</p>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="pay-form-block">
                <p class="tit">Мини-курс "Как похудеть за 3 дня"</p>
                <p class="price">Цена: <span>$3</span></p>
                <p style="color:red;">Посмотрите обязательно видео-инструкцию перед оплатой</p>
                <div class="embed-responsive embed-responsive-16by9 mb-3">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/jhZzSpzNzdI" allowfullscreen></iframe>
                </div>

                <form method="post" id="ltForm2623846" action="https://batatinfitness.getcourse.ru/pl/lite/block-public/process-html?id=105697572">
                    <input type="hidden" name="formParams[setted_offer_id]" >
                    <div class="inp-bl">
                        <input type="text" name="formParams[full_name]" placeholder="Имя *" required />
                    </div>
                    <div class="inp-bl">
                        <input type="email" name="formParams[email]" placeholder="Email *" required />
                    </div>
                    <div class="inp-bl">
                        <input type="text" name="formParams[phone]" placeholder="Телефон" id="telphone" />
                    </div>
                    <div class="order">
                        <button id="button9125677" type="button" class="send blue" onclick="if(window['btnprs5bffb554946bf']){return false;}window['btnprs5bffb554946bf']=true;setTimeout(function(){window['btnprs5bffb554946bf']=false},6000);return true;">Отправить</button>
                    </div>

                    <input type="hidden" name="__gc__internal__form__helper" class="__gc__internal__form__helper" value="">
                    <input type="hidden" name="requestTime" value="1543484756">
                    <input type="hidden" name="requestSimpleSign" value="a29b2cf907e5337e6afad607c1ec269a">
                    <input name="order_page" value="" type="hidden">

                    <p class="note">
                        Нажимая на кнопку, я даю свое согласие на обработку моих персональных данных, а также соглашаюсь с <a href="{{ route('agreement') }}" target="_blank">офертой</a> и <a href="{{ route('politics') }}" target="_blank">политикой конфиденциальности</a>
                    </p>
                </form>

                <script>window.onload=function(){document.getElementsByClassName("__gc__internal__form__helper")[0].value = window.location.href;}</script>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('land/cache/scripts.js') }}"></script>

<script src="{{ asset('land/yved/script.js') }}"></script>

<div class="yvedw">
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Елена Малькова (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Катя Швецова (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Малик Кудрина (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Мая Остапчук (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Катерина Белявская (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Марина Холодина (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Ольга Крылова (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Татьяна Филина (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Зоя Архипова (доступ к курсу открыт)</div>
        </div>
    </div>
    <div class="yved yvedf1"> <img src="{{ asset('land/yved/yico.png') }}" alt="" class="yvedi">
        <div class="yvedvt">
            <div class="yvedt">Евгения Гальчук (доступ к курсу открыт)</div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function(){
        // Counter -----------------------
        var timeSpend = 1000*120;

        if (jQuery.cookie('firstOpen') != undefined)
        {
            var interval = ((new Date()).getTime() - jQuery.cookie('firstOpen'));
            if (interval > timeSpend){
                timeSpend = 0;
            } else {
                timeSpend -= interval;
            }
        }
        else
        {
            jQuery.cookie('firstOpen', (new Date()).getTime(), { expires: 30 });
        }

        jQuery('.clock').countdown({until: timeSpend/1000, format: 'S', compact: true, onExpiry: butthide});

        if (timeSpend < 1){
            butthide();
        }

        function butthide(){
            //$('.with').remove();
        }
    });

    // Loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '100%',
            width: '100%',
            playerVars: {
                'controls': 0,
                'showinfo': 0,
                'rel': 0,
                'autoplay': 1
            },
            videoId: 'xVRo75byM5M',
            events: {
                'onStateChange': onPlayerStateChange
            }
        });
    }

    var timer1 = false;
    var timer2 = false;

    // Время в секундах. После этого выполняется первый показ контента
    var step1Sec = 5;
    // Время в секундах. После этого выполняется второй показ контента
    var step2Sec = 15;

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING) {

            var interval = setInterval(function(){
                if ( player.getCurrentTime() > step1Sec && !timer1 ) {
                    timer1 = true;

                    $('.controls').show();
                }
                if ( player.getCurrentTime() > step2Sec && !timer2 ) {
                    timer2 = true;
                    clearInterval(interval);

                    $('.controls-2').show();
                }
            },1000);
        }
    }
    function stopVideo() {
        player.stopVideo();
    }
</script>

<script src="{{ asset('land/cache/intlinput/js/intlTelInput.js') }}"></script>
<script src="{{ asset('land/cache/intlinput/js/utils.js') }}"></script>
<script>
    var input = document.querySelector("#telphone");
    var iti = window.intlTelInput(input, {
        autoPlaceholder: "aggressive",
        // onlyCountries: ["ru", "ua"],
        preferredCountries: ["ru", "ua"],
        nationalMode: false,
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
    });
</script>

</body>
</html>
