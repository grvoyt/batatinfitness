<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>Платеж в обработке</title>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="{{ asset('trening/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('trening/css/style.css') }}?1{{ time() }}">
    <link rel="stylesheet" href="{{ asset('trening/css/media.css') }}?1{{ time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="{{ asset('trening/js/jquery.mousewheel.min.js') }}"></script>
    <script defer src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script defer src="{{ asset('trening/js/all.min.js') }}"></script>
    <script defer src="{{ asset('trening/js/app.js') }}?{{ time() }}"></script>
    <script defer src="{{ asset('trening/js/timer.js') }}?{{ time() }}"></script>

</head>
<body id="payment">

<section class="main">
    <div class="container">
        <div class="fl_r sp-b fdc">
            <div class="main_item left mb30">
                <h2 class="main_offer">Платеж в обработке</h2>
                <p class="main_text_bold mb30">Ожидайте зачисления платежа, мы вам сообщим. <a href="{{ route('trening') }}"> На главную</a></p>
            </div>

        </div>
    </div>
    <img class="main_img_baba" src="{{ asset('trening/img/main_girl.png') }}" alt="">
</section>

<style>
    .main {
        min-height: 100%;
        height: 100vh;
    }
    .main_item {
        position: relative;
        z-index: 2;
    }
    .main_item .text_min {
        margin-bottom: 20px;
    }
    .main_img_baba {
        bottom: -110px;
        left: 50%;
        -webkit-transform: translateX(-10%);
        -moz-transform: translateX(-10%);
        -ms-transform: translateX(-10%);
        -o-transform: translateX(-10%);
        transform: translateX(-10%);
    }

    .fdc {
        flex-direction: column !important;
    }

    @media only screen and (max-width: 1025px) {
        .main_img_baba {
            -webkit-transform: translateX(0);
            -moz-transform: translateX(0);
            -ms-transform: translateX(0);
            -o-transform: translateX(0);
            transform: translateX(0);
        }
    }

    @media only screen and (max-width: 768px) {
        .main_img_baba {
            width: auto;
            left: 51%;
        }
        .main_item.left {
            max-width: auto;
            width: 100%;
            text-align: center;
        }

        .main_item .text_min, .main_text_bold {
            font-size: 30px;
        }
    }

    @media only screen and (max-width : 480px) {
        .main_item .text_min, .main_text_bold {
            font-size: 18px;
        }
        .main_item.left {
            max-width: 100%;
        }
        #register .main_item.left {
            max-width: 100%;
        }
        .main_img_baba {
            top: auto;
            left: 50%;
            bottom: -40%;
            -webkit-transform: translateX(-25%);
            -moz-transform: translateX(-25%);
            -ms-transform: translateX(-25%);
            -o-transform: translateX(-25%);
            transform: translateX(-25%);
        }
    }
</style>
</body>
</html>