<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <title>fitness</title>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="{{ asset('trening/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('trening/css/style.css') }}?1{{ time() }}">
    <link rel="stylesheet" href="{{ asset('trening/css/media.css') }}?1{{ time() }}">


    <script defer src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script defer src="{{ asset('trening/js/jquery.mousewheel.min.js') }}"></script>
    <script defer src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script defer src="{{ asset('trening/js/all.min.js') }}"></script>
    <script defer src="{{ asset('trening/js/app.js') }}?{{ time() }}"></script>
    <script defer src="{{ asset('trening/js/timer.js') }}?{{ time() }}"></script>

</head>
<body id="register">

<section class="main">
    <div class="container">
        <div class="fl_r sp-b fdc">
            <div class="main_item left mb30">
                <p class="text_min">Заполните форму, чтобы получить доступ к программе</p>
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <input type="hidden" name="action" value="1">

                    @error('name')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input placeholder="Введите Имя" id="name" type="text" class="form_input @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required>

                    @error('email')
                    <span class="invalid-feedback" style="color:red;font-style: italic;font-size: 14px;" role="alert">{{ $message }}</span>
                    @enderror
                    <input placeholder="Введите Email" id="email" type="email" class="form_input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required >

                    <input type="text" name="telegram" placeholder="Твой телеграм" class="form_input last">
                    <p class="small_text">Если есть, чтобы менеджер скинул всю информацию</p>

                    <button type="submit" class="form_btn">Зарегистрироваться</button>
                </form>
            </div>
            <div class="main_item">
                <p class="text_min">Где взять CVV2 на карте:</p>
                <img class="cvv_img" src="{{ asset('trening/img/csv.jpg') }}" alt="">
            </div>
            <img class="main_img_baba" src="{{ asset('trening/img/main_girl.png') }}" alt="">
        </div>
    </div>
</section>

<style>
    .cvv_img {
        max-width: 300px;
        width: 100%;
    }
    .main_item {
        position: relative;
        z-index: 2;
    }
    .main_item .text_min {
        margin-bottom: 20px;
    }
    .form_input {
        width: 100%;
        padding: 25px 10px 25px 25px;
        border: 2px solid #feff42;
        margin-bottom: 30px;
        -webkit-border-radius: 7px;
        border-radius: 7px;
    }

    .form_input.last {
        margin-bottom: 0;
    }

    .form_btn {
        width: 100%;
        height: 86px;
        line-height: 86px;
        text-transform: uppercase;
        color: #fff;
        font-size: 24px;
        font-weight: bold;
        -webkit-border-radius: 43px;
        border-radius: 43px;
        background-color: rgb(255, 154, 244);
        -webkit-box-shadow: 0px 6px 9.1px 0.9px rgba(74, 174, 159, 0.42);
        box-shadow: 0px 6px 9.1px 0.9px rgba(74, 174, 159, 0.42);
    }

    .small_text {
        width: 100%;
        padding-top: 10px;
        padding-left: 25px;
        margin-bottom: 40px;
    }

    .fdc {
        flex-direction: column !important;
    }

    @media only screen and (max-width : 480px) {
        #register .main_item.left {
            max-width: 100%;
        }

        .main_img_baba {
            display: none;
        }

        .text_min {
            font-size: 18px;
            text-align: center;
        }

        .cvv_img {
            max-width: 100% ;
        }
    }
</style>
</body>
</html>