@extends('auth.layouts')
@section('title', 'Вход' )
@section('description', 'Вход' )

@section('content')
<div class="login">
    <div class="login_wrap">
        <h2>Вход</h2>
        <form method="POST" action="{{ route('login') }}" class="auth_form">
            @csrf
            <div class="auth_form_group">
                <input type="email"
                       class="form-control @error('email') is-invalid @enderror"
                       name="email"
                       value="{{ old('email') }}" required autocomplete="email" placeholder="Ваш email">

                @error('email')
                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                @enderror
            </div>
            <div class="auth_form_group">
                <input type="password"
                       class="form-control @error('password') is-invalid @enderror"
                       name="password" required autocomplete="current-password" placeholder="Ваш пароль">

                @error('password')
                <span class="invalid-feedback" role="alert">{{ $message }}</span>
                @enderror
            </div>
            <div class="auth_form_group">
                <button type="submit" class="button"><i class="fas fa-sign-in-alt"></i> Войти</button>
            </div>
        </form>
    </div>
</div>

<style>

</style>
@endsection
