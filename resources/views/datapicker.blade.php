@push('css')
    <link rel="stylesheet" href="{{ asset('admin/css/datepicker.min.css') }}">
    <style> .datepicker { z-index: 1000; } </style>
    @endpush
@push('js')
    <script src="{{ asset('admin/js/datepicker.min.js') }}"></script>
    <script>
        var datepicker = $('.datepicker-here').datepicker({
            minDate: new Date(),
            timepicker: 1
        });
        if( !!$('.datepicker-here').data('time') ) {
            datepicker
                .data('datepicker')
                .selectDate(new Date( $('.datepicker-here').data('time') ));
        }
    </script>
@endpush
