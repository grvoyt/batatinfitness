<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\Contacts;
use App\Model\UserMarks;
use App\Model\UserInfo;
use App\Model\BalanceType;

class CreatePopulateOther extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Contacts::insert([
            [
                'key'  => 'instagram',
                'name' => "Инстаграм",
                'link' => 'https://instagram.com',
                'icon' => 'fa-instagram'
            ],
            [
                'key'  => 'telegram',
                'name' => "Телеграм",
                'link' => 'https://telegram.com',
                'icon' => 'fa-telegram'
            ]
        ]);

        UserMarks::create([
            'user_id' => 1
        ]);

        $promocode = \Illuminate\Support\Str::upper(\Illuminate\Support\Str::random(9));
        UserInfo::create([
            'user_id' => 1,
            'referal_link' => $promocode,
            'referal2'     => $promocode,
            'promocode'    => $promocode
        ]);

        BalanceType::insert([
            [
                'id' => 100,
                'ru' => "Оплата курса"
            ],
            [
                'id' => 101,
                'ru' => "Начисление по партнерской программе"
            ],
            [
                'id' => 103,
                'ru' => "Начисление системой"
            ],
            [
                'id' => 200,
                'ru' => 'Снятие с баланса'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('populate_other');
    }
}
