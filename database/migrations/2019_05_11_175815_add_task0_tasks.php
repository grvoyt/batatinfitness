<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\Tasks;
use Illuminate\Support\Facades\DB;

class AddTask0Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tasks::create([
            'is_opened' => 1,
            'name' => "Задание 0",
            'text' => "Подписаться на кучу соц сетей"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tasks')->truncate();
    }
}
