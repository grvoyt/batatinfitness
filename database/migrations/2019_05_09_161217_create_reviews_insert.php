<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReviewsInsert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::insert('INSERT INTO `reviews` (`id`, `name`, `video_id`, `created_at`, `updated_at`)
VALUES
	(1,\'Моя ученица Ира похудела на 12 кг!\',\'NOC7YUyyyX0\',\'2019-05-09 16:05:52\',\'2019-05-09 16:05:52\'),
	(2,\'Лена похудела на 17 кг\',\'gsjkihcXsbM\',\'2019-05-09 16:06:18\',\'2019-05-09 16:06:18\'),
	(3,\'Илона\',\'sssVkomj91I\',\'2019-05-09 16:08:27\',\'2019-05-09 16:08:27\'),
	(4,\'Таня, (Харьков, Украина)\',\'P0KjWPyHyU4\',\'2019-05-09 16:08:50\',\'2019-05-09 16:08:50\'),
	(5,\'Виктория Чбоксары\',\'ZyIEIdmxSE0\',\'2019-05-09 16:09:16\',\'2019-05-09 16:09:16\'),
	(6,\'Юля (Россия, Уфа)\',\'T5ncm4H9MdY\',\'2019-05-09 16:09:36\',\'2019-05-09 16:09:36\'),
	(7,\'Оксана (Никополь, Украина)\',\'QX2nXuyzSK4\',\'2019-05-09 16:09:54\',\'2019-05-09 16:09:54\'),
	(8,\'Анна (Москва, Россия)\',\'vF0QKfyIHuY\',\'2019-05-09 16:10:15\',\'2019-05-09 16:10:15\')');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('reviews')->truncate();
    }
}
