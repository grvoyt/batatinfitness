<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\Model\Points;

class UpdatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('points')->truncate();
        Points::insert([
            ['task_id' => 1, 'value' => 1],
            ['task_id' => 2, 'value' => 4],
            ['task_id' => 3, 'value' => 3],
            ['task_id' => 4, 'value' => 4],
            ['task_id' => 5, 'value' => 4],
            ['task_id' => 6, 'value' => 3],
            ['task_id' => 7, 'value' => 4],
            ['task_id' => 8, 'value' => 3]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('points')->truncate();
    }
}
