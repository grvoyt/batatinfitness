<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\Points;

class CreatePointsValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Points::create([
            'task_id' => 1,
            'value' => 2
        ]);
        Points::create([
            'task_id' => 2,
            'value' => 22
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Points::truncate();
    }
}
