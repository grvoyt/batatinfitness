<?php

namespace App\Events;

use App\Model\TasksAnswers;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TasksConfirm
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user_id;
    public $task_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id, $user_id)
    {
        $this->task_id = $task_id;
        $this->user_id = $user_id;
    }
}
