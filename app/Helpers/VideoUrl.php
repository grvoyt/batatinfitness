<?php


namespace App\Helpers;


use Illuminate\Support\Str;

class VideoUrl
{
    const HOSTERS = [
        'youtube',
        'vimeo'
    ];
    const YOUTUBE_IFRAME = 'https://www.youtube.com/embed/';

    public static function getId($url) {
        $hoster = self::checkUrl($url);
        switch($hoster) {
            case 'youtube':
                return self::getYoutubeId($url);
                break;

            default:
                return self::default();
                break;
        }
    }

    public static function getIframe($url) {
        $hoster = self::checkUrl($url);
        switch($hoster) {
            case 'youtube':
                return self::youtubeIframe($url);
                break;

            default:
                return self::default();
                break;
        }
    }

    protected static function checkUrl($url) {
        $scheme = parse_url( $url );
        foreach(self::HOSTERS as $hoster) {
            if(Str::contains($scheme['host'], $hoster)) {
                return $hoster;
            }
        }
    }

    public static function getYoutubeId($url) {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $video );
        return $video['v'];
    }

    public static function youtubeIframe($url) {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $video );
        $data = [
            'rel' => 0
        ];
        $url = self::YOUTUBE_IFRAME.$video['v'].'?'.http_build_query($data);
        return $url;
    }

    protected static function default() {
        return false;
    }
}
