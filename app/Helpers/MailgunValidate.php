<?php
/**
 * Created by PhpStorm.
 * User: Georgiy Voytkevich
 * Date: 11.06.2019
 * Time: 12:54
 */

namespace App\Helpers;


use GuzzleHttp\Client as HttpClient;

class MailgunValidate
{
    public $status;
    public $message;
    public static function validate($email)
    {
        $requestData = [
            'auth' => ['api', config('services.mailgun.api_key')],
            'form_params' => [
                'address' => $email
            ]
        ];

        $client = new HttpClient();
        $response = $client->request(
            'GET',
            'https://api.mailgun.net/v4/address/validate',
            $requestData
        );

        return self::getBody($response);
    }

    protected static function getBody($response)
    {
        $self = new self();
        $answer = json_decode( (string) $response->getBody(), true );

        switch($answer['result']) {
            case 'deliverable':
                $self->status =  1;
                break;

            case 'undeliverable':
            case 'unknown':
                $self->status = 0;
                $self->message = self::getReason($answer['reason']);
                break;
        }

        return $self;
    }

    protected static function getReason($message)
    {
        switch($message) {
            case 'mailbox_does_not_exist':
                return 'Такого email не существует, попробуйте другой';
                break;
        }
    }
}