<?php


namespace App\Helpers;


use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;

class Getresponse
{
    const URL = 'https://api.getresponse.com/v3';

    /**
     * @param $data
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function addContact($data)
    {
        $contact = self::makeContact($data);
        return self::makeRequest('contacts', $contact,'POST',true);
    }

    /**
     * @param $data
     * @return array
     */
    public static function makeContact($data)
    {
        return [
            'name' => $data['name'],
            'email' => $data['email'],
            "dayOfCycle" => '0',
            "campaign" => [
                "campaignId" => $data['campaign'] ?? config('services.getresponse.campaignId')
            ]
        ];
    }



    /**
     * @param array $data
     * @param string $method
     * @param bool $json
     * @return bool|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function makeRequest($url = '', $data=[], $method = 'GET', $json=false)
    {
        $requestData = [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Auth-Token' => 'api-key '.config('services.getresponse.api_key'),
            ]
        ];
        if($json) {
            $data_string = json_encode($data);
            $requestData['headers']['Content-Length'] = strlen($data_string);
            $requestData['body'] = $data_string;
        } else {
            $property = ($method == "GET") ? 'query' : 'form_params';
            $requestData[$property] = $data;
        }

        $client = new HttpClient();
        $response = $client->request($method, self::URL.'/'.$url,$requestData);

        return $response;
    }
}
