<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClubRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'age' => 'required',
            'goal' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'age.required' => 'Обязательное поле',
            'goal.required' => 'Обязательное поле'
        ];
    }
}
