<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RequestAmount extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->is_partner;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric|min:1'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'amount.required' => 'Сумма обязательна',
            'amount.numeric'  => 'Должно быть цифрой',
            'amount.min'      => 'Вывод должен быть больше 0'
        ];
    }
}
