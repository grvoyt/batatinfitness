<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TeamAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->is_admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'img'  => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'name' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя обязательно',
            'img.required'  => "Необходимо добавить фотографию",
            'img.image'     => "Необходимо добавить фотографию",
            'img.mimes'     => "Фотография должа быть расщирения: jpeg,png,jpg,gif",
            'img.max'       => 'Слишком большой размер файла, максимум 2 Мб'
        ];
    }
}
