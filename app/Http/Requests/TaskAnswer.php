<?php

namespace App\Http\Requests;

use App\Model\TasksAnswers;
use Illuminate\Foundation\Http\FormRequest;

class TaskAnswer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'img' => 'image|mimes:jpeg,png,jpg,gif'
        ];
    }

    public function messages()
    {
        return [
            'answer.required' => 'Комментарий обязателен',
            'answer.min' => 'Комментарий должен быть больше :min',
            'img.image'    => "Необходимо добавить фотографию",
            'img.mimes'    => "Фотография должа быть расщирения: jpeg,png,jpg,gif",
            'img.max'      => 'Слишком большой размер файла, максимум 5 Мб'
        ];
    }
}
