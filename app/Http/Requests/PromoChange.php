<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PromoChange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'promo' => 'required|string|min:8|max:10|unique:user_info,promocode'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'promo.required' => 'Промокод обязательно к заполнениею',
            'promo.min' => "Промокод должен быть больше :min",
            'promo.max' => "Промокод должен быть меньше :max",
            'promo.unique' => "Такой промокод уже существует"
        ];
    }
}
