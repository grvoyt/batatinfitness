<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AvatarSetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar'  => 'image|mimes:jpeg,png,jpg,gif|max:3048',
        ];
    }

    public function messages()
    {
        return [
            'avatar.image'    => "Необходимо добавить фотографию",
            'avatar.mimes'    => "Фотография должа быть расщирения: jpeg,png,jpg,gif",
            'avatar.max'      => 'Слишком большой размер файла, максимум 3 Мб'
        ];
    }
}
