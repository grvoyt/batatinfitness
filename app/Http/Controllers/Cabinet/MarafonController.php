<?php

namespace App\Http\Controllers\Cabinet;

use App\Helpers\VideoUrl;
use App\Http\Controllers\Controller;
use App\Http\Traits\StoreImg;
use App\Model\GameSchedule;
use App\Model\Points;
use App\Model\PointsUsers;
use App\Model\Tasks;
use App\Model\TasksAnswers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TaskAnswer;


class MarafonController extends Controller
{
    use StoreImg;

    public function index() {
        return view('cabinet.marafon.index');
    }

    public function rules() {
        return view('cabinet.marafon.rules');
    }

    public function schedule() {
        $schedules = GameSchedule::getLast();
        return view('cabinet.marafon.schedule',compact('schedules'));
    }

    public function tasks()
    {
        //получаем список открытых заданий
        $tasks_opened = Tasks::isOpened();
        $tasks_opened = $tasks_opened->mapWithKeys(function($item) {
            return [$item['id'] => $item];
        })->toArray();

        //получаем список отвеченых заданий
        $user_task_answers = TasksAnswers::where('user_id', Auth::id())
                                            ->where('status',1)->get('task_id');
        $user_task_answers = $user_task_answers->mapWithKeys(function($item) {
            return [$item['task_id'] => $item];
        })->toArray();

        //добавляем в задания закрыто/открыто
        $tasks = [];
        foreach($tasks_opened as $task) {
            $task['is_done'] = isset($user_task_answers[$task['id']]) ? 1 : 0;
            $tasks[] = (object) $task;
        }

        //получаем балы и форматируем
        $points = Points::all();
        $points = $points->mapWithKeys(function($item) {
            return [$item['task_id'] => $item['value']];
        });

        $ball = 0;
        foreach($user_task_answers as $k=>$v) {
            $ball += $points[$v['task_id']];
        }
        
        return view('cabinet.marafon.tasks',compact('tasks','ball','points'));
    }

    public function task_show($id) {
        $task = Tasks::find($id);
        $task_info = TasksAnswers::where('task_id', $task->id)
                        ->where('user_id', Auth::id())->first();
        $show_form = 1;
        if( isset($task_info->status) ) {
            $show_form = !$task_info->status ? 1 : 0;
        }

        $video = !empty($task->video_url) ? $task->video_url : 0;

        return view('cabinet.marafon.task_show', compact('task','video','task_info','show_form'));
    }

    public function task_store(TaskAnswer $request,$id)
    {
        $task = TasksAnswers::where([
            'task_id' => $id,
            'user_id' => Auth::user()->id
        ])->first();
        $answer = [
            'success' => 'Ваше задание принято'
        ];

        if($task && $task->status) {
            $answer = [
                'error' => 'Ваш комментарий уже принят'
            ];
            return redirect()->route('cabinet.game.tasks')
                ->with($answer);
        }

        $update = [
            'answer'  => $request->answer ?? '',
            'status' => 1
        ];

        if(!!$request->img) {
            $update['img'] = $this->storeImg($request,'task_'.$id,'img');
        }

        TasksAnswers::updateOrCreate(
            ['task_id' => $id,'user_id' => Auth::user()->id,],
            $update
        );
        return redirect()->route('cabinet.game.tasks')
            ->with($answer);
    }
}
