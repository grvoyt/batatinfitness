<?php

namespace App\Http\Controllers\Cabinet;

use App\User;
use App\Model\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ProfileController extends Controller
{
    public function index() {
        $user = Auth::user();
        $video = Settings::where('key','profile_main')->value('value');
        $img = $user->info->img ?? 'img/no-avatar.jpg';
        $balance = Cache::get('balance_'.Auth::id(),0);
        $not_paid = User::where('parent_id', Auth::id())->count();
        return view('cabinet.profile.index', compact(
            'user',
            'img',
            'video',
            'balance',
            'not_paid'
        ));
    }
}
