<?php

namespace App\Http\Controllers\Cabinet;

use App\Model\Reviews;
use App\Http\Controllers\Controller;

class OtziviController extends Controller
{
    public function index() {
        $reviews = Reviews::all();
        return view('cabinet.otzivi', compact('reviews'));
    }
}
