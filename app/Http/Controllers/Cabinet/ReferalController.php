<?php

namespace App\Http\Controllers\Cabinet;

use App\Events\BalanceChange;
use App\Http\Requests\PromoChange;
use App\Http\Requests\RequestAmount;
use App\Model\Balance;
use App\Model\UserInfo;
use App\Model\Withdraw;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ReferalController extends Controller
{
    public function index()
    {
        $promocod = Auth::user()->info->promocode;
        $not_payed_partners = User::where('parent_id', Auth::id())->count();
        $payed_partners = Balance::notPaid(Auth::id());
        $balance = Cache::get('balance_'.Auth::id(),0);

        return view('cabinet.referal', compact(
            'promocod',
            'not_payed_partners',
            'payed_partners',
            'balance'
        ));
    }

    public function promoChange(PromoChange $request)
    {
        $user_info = UserInfo::where('user_id', Auth::id())->first();
        $user_info->promocode = $request->promo;
        $user_info->save();
        return response()->json([
            'response' => "Промокод изменен"
        ]);
    }

    public function requestPayment(RequestAmount $request)
    {
        $balance = Balance::byUser(Auth::id());

        if( ($balance['balance'] - $request->amount) < 0 ) {
            return back()->withErrors([
                'amount' => "У Вас нет необходимого кол-ва баллов"
            ]);
        }

        $withdraw = Withdraw::create([
            'user_id' => Auth::id(),
            'amount'  => $request->amount
        ]);

        Balance::create([
            'user_id' => Auth::id(),
            'type'    => 200,
            'amount'  => $request->amount,
            'withdraw_id' => $withdraw->id
        ]);

        event(new BalanceChange(Auth::id()));

        return back()->with('success', 'Вывод зарегистрирован');
    }
}
