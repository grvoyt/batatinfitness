<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Traits\StoreImg;
use App\Http\Requests\AvatarSetRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Model\UserInfo;
use Illuminate\Auth\Events\PasswordReset;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SettingsController extends Controller
{
    use StoreImg;

    protected $filepath = 'avatars';

    protected $filename = 'avatar';

    public function index()
    {
        $user = Auth::user();
        return view('cabinet.settings', compact('user'));
    }


    public function reset(ResetPasswordRequest $request)
    {
        $user = Auth::user();

        $user->password = Hash::make($request->password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));

        return back()->with([
            'success' => 'Пароль успешно изменен'
        ]);
    }

    public function avatar(AvatarSetRequest $request)
    {
        $user = Auth::user();
        $user_info = UserInfo::where('user_id',$user->id)->first();
        $user_img = $this->storeImg($request);
        $user_info->img = $user_img;
        $user_info->save();

        Cache::put('user_img_'.Auth::id(),$user_img);

        return back()->with([
            'success' => 'Аватар сохранен'
        ]);
    }
}
