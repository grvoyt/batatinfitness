<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Traits\SettingsKeys;
use App\Http\Controllers\Controller;
use App\Model\Settings;

class ContactController extends Controller
{
    use SettingsKeys;

    protected $only = ['instagram_main','fitnes_chat','close_chat','bonus_link','telegram_help','telegram_tech'];

    public function index() {
        $keys = $this->clearKeys();
        $settings = Settings::whereIn('key',$this->only)->get();
        return view('cabinet.contact', compact('keys','settings'));
    }

    public function clearKeys() {
        return collect($this->keys)->only($this->only)->all();
    }
}
