<?php

namespace App\Http\Controllers\Cabinet;

use App\Model\Balance;
use App\Model\BalanceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BalanceController extends Controller
{
    private $status = ['Ожидает','Одобрено','Отклонен'];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $status = $this->status;
        $balances = Balance::where('user_id',Auth::id())->get();
        $types = BalanceType::all()->mapWithKeys(function($item){
            return [$item['id'] => $item['ru']];
        });
        return view('cabinet.balance',compact('balances','types','status'));
    }
}
