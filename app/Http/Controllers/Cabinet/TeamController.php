<?php

namespace App\Http\Controllers\Cabinet;

use App\Model\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    public function index() {
        $teachers = Team::all();
        return view('cabinet.team', compact('teachers'));
    }
}
