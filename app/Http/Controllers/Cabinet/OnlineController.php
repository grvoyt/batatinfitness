<?php

namespace App\Http\Controllers\Cabinet;

use App\Model\Webinar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OnlineController extends Controller
{
    public function index() {
        $webinars = Webinar::all();
        return view('cabinet.online', compact('webinars'));
    }
}
