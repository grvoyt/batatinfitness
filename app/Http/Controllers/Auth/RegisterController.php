<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\MailgunValidate;
use App\Mail\SendInfoAdmin;
use App\Model\UserInfo;
use App\Model\UserMarks;
use App\Payment\Fondy;
use App\User;
use App\Http\Controllers\Controller;
use App\UserSocial;
use App\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/cabinet';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $tarif_key = Session::get('tarif_key',0);
        return !$tarif_key
            ? redirect()->back()
            : view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        $user = User::where('email', $request->email)->first();

        if(!$user) {
            $user = $this->create($request->all());
        }

        event(new Registered($user));

        $this->guard()->login($user);

        //отправить на email админу инфу
        if(!!$request->action) $this->sendToAdmin($request->all());

        if(!!$request->telegram) $this->makeSocial($user,$request->telegram);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * @param User $user
     * @param $telegram
     * @return mixed
     */
    protected function makeSocial(User $user, $telegram)
    {
        return UserSocial::create([
            'user_id' => $user->id,
            'telegram' => $telegram
        ]);
    }

    /**
     * send email to admin
     * @param array $data
     */
    protected function sendToAdmin(array $data)
    {
        $tarif_key = Session::get('tarif_key',0);
        $tarif = $this->getTarif($tarif_key);
        $tarif_data = is_array($tarif) ? $tarif : [];

        try {
            Mail::to( config('others.contacts.email') )
                ->send(new SendInfoAdmin($data,$tarif_data));
        } catch (\Exception $e) {
            Log::error('ADMIN EMAIL', [$e->getMessage()]);
        }
    }

    /**
     * REgister by console
     * @param $data
     * @return User
     */
    public function registerConsole($data)
    {
        $user = User::where('email', $data['email'])->first();

        if(!$user) {
            $user = $this->create($data);
        }

        $type_compain = (Carbon::now()->hour < 16) ? 'before' : 'after';

        $getresponse_compain = config('services.getresponse.av1_'.$type_compain.'_16');

        event(new Registered($user,$getresponse_compain));
        return $user;
    }

    public function registerWebinarSale(array $data)
    {
        $user = User::where('email', $data['email'])->first();
        if(!$user) {
            $user = $this->create($data);
        }
        $getresponse_compain = config('services.getresponse.webinar_sale');
        event(new Registered($user,$getresponse_compain));

        try {
            $tarif = $this->getTarif($data['tarif']);
            Mail::to( config('others.contacts.email') )
                ->send(new SendInfoAdmin($data,$tarif,'webinar_sale'));
        } catch (\Exception $e) {
            Log::error('ADMIN EMAIL', [$e->getMessage()]);
        }

        return $this->makeCheckout($tarif,$user);
    }

    public function registerCoaching(Request $request)
    {
        $post = $request->except('_token');
        $user = User::where('email', $post['email'])->first();
        if(!$user) {
            $user = $this->create($post);
        }
        $getresponse_compain = config('services.getresponse.coaching_paid');
        event(new Registered($user,$getresponse_compain));
        $tarif = Session::get('tarif_key',0);
        return $this->makeCheckout(
            $this->getTarif($tarif),
            $user
        );
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $password = random_int(100000,999999);
        Session::put('password',$password);

        $user_data = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($password),
        ];

        $user_data['parent_id'] = !!$this->getPartnerID($data) ? $this->getPartnerID($data) : 0;

        return User::create($user_data);
    }

    protected function getPartnerID($data)
    {
        return ( isset($data['promocode']) && !!$data['promocode'] )
            ? UserInfo::where('promocode', $data['promocode'])->value('user_id')
            : Cookie::get('parent_id',0);
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     * @throws \Cloudipsp\Exception\ApiException
     */
    protected function registered(Request $request, $user)
    {
        //delete cookie
        Cookie::queue(
            Cookie::forget('marks')
        );
        Cookie::queue(
            Cookie::forget('parent_id')
        );

        $tarif_key = Session::get('tarif_key',0);

        return $this->makeCheckout(
            $this->getTarif($tarif_key),
            $user
        );
    }

    /**
     * get tarif array
     * @param $tarif_key
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function getTarif($tarif_key)
    {
        switch($tarif_key) {
            case 'standart':
                $amount = 1490;
                $title  = 'STANDART';
                $currency = 'RUB';
                break;

            case 'gold':
                $amount = 1950;
                $title  = 'GOLD';
                $currency = 'RUB';
                break;

            case 'platina':
                $amount = 3950;
                $title  = 'PLATINA';
                $currency = 'RUB';
                break;

            case 'personal':
                $amount = 30000;
                $title  = 'PERSONAL';
                $currency = 'RUB';
                break;

            case 'standart2':
                $amount = 300;
                $title  = 'STANDART';
                $currency = 'USD';
                break;

            case 'silver':
                $amount = 500;
                $title  = 'SILVER';
                $currency = 'USD';
                break;

            case 'gold2':
                $amount = 750;
                $title  = 'GOLD';
                $currency = 'USD';
                break;

            case 'platina2':
                $amount = 2500;
                $title  = 'PLATINA';
                $currency = 'USD';
                break;

            case 'gold300':
                $amount = 300;
                $title  = 'GOLD акционный';
                $currency = 'USD';
                break;

            case 'silver500':
                $amount = 500;
                $title  = 'SILVER акционный';
                $currency = 'USD';
                break;

            default:
                return redirect('/cabinet');
                break;
        }

        return compact('amount','title','currency');
    }

    /**
     * @param $tarif
     * @throws \Cloudipsp\Exception\ApiException
     */
    protected function makeCheckout($tarif,$user)
    {
        return is_array($tarif) ? Fondy::makeCheckout($tarif['amount'], $tarif['title'], $user->id, $tarif['currency']) : $tarif;
    }
}
