<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TeamAdd;
use App\Http\Traits\StoreImg;
use App\Model\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    use StoreImg;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Team::all();
        return view('admin.team.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamAdd $request)
    {
        $teacher = new Team();
        $teacher->name = $request->name;
        $teacher->position = $request->position;
        $teacher->description = $request->description;
        $teacher->img = $request->hasFile('img')
            ? $this->storeImg($request,'team','img')
            : 'img/no-avatar.jpg';
        $teacher->save();
        return redirect()->route('admin.team.index')
            ->with([
                'success' => 'Член команды был добавлен'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Team::where('id',$id)->first();
        return view('admin.team.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamAdd $request, $id)
    {
        $teacher = Team::where('id',$id)->first();
        $teacher->name = $request->name;
        $teacher->name = $request->name;
        $teacher->position = $request->position;
        $teacher->description = $request->description;
        if($request->hasFile('img')) {
            $teacher->img = $this->storeImg($request,'team','img');
        }
        $teacher->save();
        return redirect()->route('admin.team.index')->with([
            'success' => 'Член команды был обновлен'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::destroy($id);
        return redirect()->route('admin.team.index')->with([
            'success' => 'Член комманды удален'
        ]);
    }
}
