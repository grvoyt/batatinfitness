<?php

namespace App\Http\Controllers\Admin;

use App\Events\BalanceChange;
use App\Model\Balance;
use App\Model\Withdraw;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WithdrawController extends Controller
{
    private $status = ['Ожидает','Одобрено','Отклонен'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $this->status;
        switch($request->show) {
            case 'all':
                $withdraws = Withdraw::all();
                break;

            case 'accept':
                $withdraws = Withdraw::where('status',1)->get();
                break;

            case 'reject':
                $withdraws = Withdraw::where('status',2)->get();
                break;

            default:
                $withdraws = Withdraw::where('status',0)->get();
                break;
        }

        return view('admin.withdraw.index', compact('withdraws','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = $this->status;
        $withdraw = Withdraw::find($id);
        $client = User::find($withdraw->user_id);
        return view('admin.withdraw.show', compact('withdraw','client','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $request->action
            ? $this->acceptAction($request, $id)
            : $this->rejectAction($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function acceptAction($request, $id)
    {
        $withdraw = Withdraw::find($id);
        $withdraw->status = 1;
        $withdraw->message = $request->text;
        $withdraw->save();
        return $this->mainRoute()->with('success', 'Успешно одобрено');
    }

    public function rejectAction($request, $id)
    {
        $withdraw = Withdraw::find($id);
        $withdraw->status = 2;
        $withdraw->message = $request->text;
        $withdraw->save();
        Balance::create([
            'user_id' => Auth::id(),
            'type'    => 104,
            'amount'  => $withdraw->amount,
            'withdraw_id' => $withdraw->id
        ]);
        event(new BalanceChange(Auth::id()));
        return $this->mainRoute()->with('success', 'Отказ сохранен');
    }

    public function mainRoute()
    {
        return redirect()->route('admin.withdraws.index');
    }
}
