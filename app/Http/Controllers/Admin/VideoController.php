<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\VideoUrl;
use App\Http\Traits\SettingsKeys;
use App\Model\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    use SettingsKeys;

    public function index() {
        $settings = Settings::all();
        $keys = $this->keys;
        return view('admin.video', compact(
            'settings',
            'keys'
        ));
    }

    public function store(Request $request) {
        switch($request->key) {
            case 'profile_main':
                return $this->storeProfileVideo($request);
                break;

            default:
                return $this->storeDefault($request);
                break;
        }
    }

    public function storeProfileVideo(Request $request) {
        $profile_main = Settings::where('key', $request->key)->first();
        $profile_main->value = VideoUrl::getId($request->value);
        $profile_main->save();
        return redirect()->route('admin.video.index')->with([
            'success' => 'Видео обновлено'
        ]);
    }

    public function storeDefault(Request $request) {
        Settings::updateOrCreate(
            ['key' => $request->key],
            ['value' => $request->value]
        );
        return redirect()->route('admin.video.index')->with([
            'success' => 'Обновлено'
        ]);
    }
}
