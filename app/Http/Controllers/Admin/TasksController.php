<?php

namespace App\Http\Controllers\Admin;

use App\Http\Traits\YoutubeId;
use App\Model\Tasks;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class TasksController extends Controller
{
    use YoutubeId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Tasks::all();
        return view('admin.tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();
        $post['video_url'] = $this->getYoutubeId($post['video_url']);
        Tasks::create($post);
        return $this->success('Задание добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Tasks::find($id);
        return view('admin.tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->except('_token', '_method');
        if(strlen($post['video_url']) >= 11 ) $post['video_url'] = $this->getYoutubeId($post['video_url']);
        Tasks::where('id',$id)
            ->update($post);
        return $this->success("$request->name обновлен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tasks::where('id',$id)->delete();
        } catch( \Exception $e ) {
            Log::error('ADMIN DELETE TASK',[$e->getMessage(), $e->getCode()]);
            return back()->withErrors([
                'delete' => 'Произошла ошибка'
            ]);
        }

        return $this->success('Задание удалено');
    }

    public function activate(Request $request, $id)
    {
        $task = Tasks::find($id);
        $task->is_opened = !$task->is_opened;
        $task->save();
        return $this->success('Успешно изменено');
    }

    public function success($text)
    {
        return redirect()->route('admin.tasks.index')
            ->with([
                'success' => $text
            ]);
    }
}
