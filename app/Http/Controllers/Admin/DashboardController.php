<?php

namespace App\Http\Controllers\Admin;

use App\Model\Payments;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index() {
        $payed_sum = Payments::where('amount','>',0)->sum('amount');
        $payments_count = Payments::where('amount',0)->count();
        $payments_count_today = Payments::where('amount',0)->whereDate('created_at', date('Y-m-d'))->count();
        $users_count = User::count();
        return view('admin.home', compact(
            'payed_sum',
            'payments_count',
            'payments_count_today',
            'users_count'
        ));
    }
}
