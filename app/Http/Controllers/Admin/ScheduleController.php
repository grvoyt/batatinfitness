<?php

namespace App\Http\Controllers\Admin;

use App\Model\GameSchedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = GameSchedule::getLast();
        return view('admin.schedule.index',compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schedule.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request)
    {
        $post = $request->except('_token');

        $post['date'] = Carbon::parse($request->date);

        GameSchedule::create( $post );

        return redirect()->route('admin.schedule.index')
            ->with('success','Успешно добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule = GameSchedule::find($id)->toArray();
        $schedule['date'] = Carbon::parse($schedule['date'])->timestamp * 1000;
        return view('admin.schedule.edit', compact('schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $schedule = GameSchedule::find($id);
        $schedule->date = Carbon::parse($request->date);
        $schedule->title = $request->title;
        $schedule->host = $request->host;
        $schedule->text = $request->text;
        $schedule->save();
        return redirect()->route('admin.schedule.index')
            ->with('success','Успешно изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GameSchedule::find($id)->delete();
        return redirect()->route('admin.schedule.index')
            ->with('success','Успешно удален');
    }
}
