<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\VideoUrl;
use App\Http\Requests\OnlineCreate;
use App\Model\Webinar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OnlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $webinars = Webinar::all();
        return view('admin.webinar.index',compact('webinars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.webinar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OnlineCreate $request)
    {
        $post = $request->except('_token');
        $title = $post['title'];
        $link  = VideoUrl::getYoutubeId($post['link']);

        Webinar::create(compact('title','link'));
        return $this->success('Вебинар добавлен');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $webinar = Webinar::find($id);
        return view('admin.webinar.show',compact('webinar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->except('_token');
        $webinar = Webinar::find($id);
        $webinar->title = $post['title'];
        $webinar->link  = VideoUrl::getYoutubeId($post['link']);
        $webinar->save();
        return $this->success('Успешно изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Webinar::find($id)->delete();
        return $this->success('Успешно удалено');
    }

    /**
     * @param $text
     * @return \Illuminate\Http\RedirectResponse
     */
    public function success($text)
    {
        return redirect()->route('admin.online.index')
            ->with([
                'success' => $text
            ]);
    }

    public function error($errors)
    {
        return back()->withErrors($errors);
    }
}
