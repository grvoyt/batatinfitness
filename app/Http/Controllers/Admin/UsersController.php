<?php

namespace App\Http\Controllers\Admin;

use App\Events\TasksConfirm;
use App\Model\Payments;
use App\Model\TasksAnswers;
use App\User;
use App\UserSocial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();
        $socials = UserSocial::all()->mapWithKeys(function($item) {
            return [$item->user_id => $item];
        });
        return view('admin.users.index', compact('users','socials'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tasks($id)
    {
        $user = User::find($id);
        $tasks = TasksAnswers::where('user_id',$id)->orderBy('task_id','asc')->with('info')->get();
        return view('admin.users.tasks', compact('tasks','user'));
    }

    /**
     * @param $id
     * @param $task_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function task($id,$task_id)
    {
        $task = TasksAnswers::where('task_id',$task_id)
                ->where('user_id',$id)
                ->with('info')
                ->first();
        $user = User::find($id);
        return view('admin.users.task',compact('user','task'));
    }

    /**
     * @param $id
     * @param $task_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function task_update($id,$task_id)
    {
        $task = TasksAnswers::where('task_id',$task_id)
            ->where('user_id',$id)->first();
        $task->status = 1;
        $task->save();

        event(new TasksConfirm($task_id,$id));

        return redirect()->route('admin.users.tasks', $id)
            ->with([
                'success' => 'Задание одобрено'
            ])->withErrors([
                's' => 'ss'
            ]);
    }

    public function payments(int $user_id)
    {
        $payments = Payments::where('user_id',$user_id)->get();
        $user = User::find($user_id);
        return view('admin.payments.show', compact('user','payments'));
    }
}
