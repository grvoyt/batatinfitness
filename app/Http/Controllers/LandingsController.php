<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Requests\OrderRequest;
use App\Http\Traits\Sums;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LandingsController extends Controller
{
    use Sums;

    public function agreement()
    {
        return view('agreement');
    }

    public function politics()
    {
        return view('politics');
    }

    public function otkaz()
    {
        return view('otkaz');
    }

    public function coaching()
    {
        Session::put('coaching',1);
        return view('lands.coaching');
    }

    public function coaching_reg_show()
    {
        return view('lands.coaching_register');
    }

    public function coaching_register(Request $request)
    {
        $register = new RegisterController();
        $register->registerCoaching($request);
    }

    public function av3_sale()
    {
        return view('lands.av3_sale');
    }

    public function av3_sale_reg(OrderRequest $request)
    {
        if($request->get('tarif') == '') return back()->withErrors(['tarif' => 'Erro with form']);
        $post = $request->except('_token');

        $compain = config('services.getresponse.av-3_sale');
        $tarif = $this->getSum($post['tarif'],$compain);

        $user  = $this->makeUserArray($post);

        $this->SendToGetResponse($tarif,$user);

        $email = config('others.contacts.email');

        $this->sendEmailToAdmin($email,$user,$tarif,'av3_sale');

        return $this->makeCheckout($tarif,$user);
    }

    public function av4_sale()
    {
        return view('lands.av4_sale');
    }

    public function av4_sale_reg(OrderRequest $request)
    {
        if($request->get('tarif') == '') return back()->withErrors(['tarif' => 'Erro with form']);
        $post = $request->except('_token');

        $compain = config('services.getresponse.av-4_sale');
        $tarif = $this->getSum($post['tarif'],$compain);

        $user  = $this->makeUserArray($post);

        $this->SendToGetResponse($tarif,$user);

        $email = config('others.contacts.email');

        $this->sendEmailToAdmin($email,$user,$tarif,'av4_sale');

        return $this->makeCheckout($tarif,$user);
    }
}
