<?php

namespace App\Http\Controllers;

use App\Mail\PasswordEmail;
use App\Mail\SendInfoAdmin;
use App\User;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index($id)
    {
        switch($id) {
            case 'email':
                return $this->email();
                break;

            case 'admin':
                return $this->admin();
                break;

            default:
                return abort(404);
                break;
        }
    }

    public function admin()
    {
        $data = [
            'name' => 'George',
            'email' => 'ridick@xaker.ru'
        ];
        $tarif = [
            'title' => 'gold'
        ];
        $email = new SendInfoAdmin($data,$tarif);
        return $email->render();
    }

    public function email()
    {
        $user = User::find(1);
        $email = new PasswordEmail($user,'2312');
        return $email->render();
    }
}
