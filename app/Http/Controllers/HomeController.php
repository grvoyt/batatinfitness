<?php

namespace App\Http\Controllers;

use App\Helpers\Getresponse;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Requests\ClubRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Traits\Sums;
use App\Mail\SendInfoAdmin;
use App\Model\MailsResponse;
use App\Payment\Fondy;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    use Sums;
    /**
     * main landing
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        Session::put('main_reg',1);
        return view('lands.game');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function fitness()
    {
        return redirect('/');
    }

    /**
     * for trening route
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trening()
    {
        return view('lands.trening');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkout($id)
    {
        Session::put('tarif_key', $id);
        if( Session::get('coaching',0) ) return redirect()->route('coaching.reg_show');
        return redirect()->route('register');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function webinar()
    {
        $date_now = Carbon::now();
        if($date_now->hour >= 16) $date_now = $date_now->addDay();
        $date_new = $date_now->day.' '.$date_now->getTranslatedMonthName('Do MMMM');
        return view('lands.webinar', compact('date_new'));
    }

    public function webinar1()
    {
        $date_now = Carbon::now();
        if($date_now->hour >= 16) $date_now = $date_now->addDay();
        $date_new = $date_now->day.' '.$date_now->getTranslatedMonthName('Do MMMM');
        return view('lands.webinar1', compact('date_new'));
    }

    public function webinar2()
    {
        $date_now = Carbon::now();
        if($date_now->hour >= 16) $date_now = $date_now->addDay();
        $date_new = $date_now->day.' '.$date_now->getTranslatedMonthName('Do MMMM');
        return view('lands.webinar2', compact('date_new'));
    }

    /**
     * @param Request $request
     */
    public function webinar_register(Request $request)
    {
        $register = new RegisterController();
        $register->registerConsole($request->all());
        return redirect()->route('webinar.oto');
    }

    public function webinar_oto(Request $request)
    {
        return view('lands.webinat_oto');
    }

    public function webinar_oto_post(Request $request)
    {
        $tarif = [];
        $tarif['getresp_compain'] = config('services.getresponse.w_thanks_bot');
        $this->SendToGetResponse($tarif, $request->except('_token'));

    }

    public function webinar_post(Request $request)
    {
        $type_compain = (Carbon::now()->hour < 16) ? 'before' : 'after';
        $getresponse_compain = config('services.getresponse.wp_'.$type_compain.'_16');
        $tarif = [];
        $tarif['getresp_compain'] = $getresponse_compain;
        $this->SendToGetResponse($tarif, $request->except('_token'));
        return redirect()->route('webinar.oto');
    }

    public function webinar_sale()
    {
        return view('lands.webinar_sale');
    }

    public function webinar_sale_reg(OrderRequest $request)
    {
        $post = $request->except('_token');
        $register = new RegisterController();
        return $register->registerWebinarSale($post);
    }

    public function club_register(ClubRequest $request)
    {
        dd($request->all());
    }

    public function av2_sale(Request $request)
    {
        return view('lands.av2_sale');
    }

    public function av2_sale_reg(OrderRequest $request)
    {
        if($request->get('tarif') == '') return back()->withErrors(['tarif' => 'Erro with form']);
        $post = $request->except('_token');
        $tarif = $this->getSum($post['tarif']);
        $user  = $this->makeUserArray($post);

        $this->SendToGetResponse($tarif,$user);

        $email = config('others.contacts.email');

        $this->sendEmailToAdmin($email,$user,$tarif,'av2_sale');

        return $this->makeCheckout($tarif,$user);
    }

    protected function SendToGetResponse($tarif,$user)
    {
        $data = [
            'name' => $user['name'] ?? '',
            'email' => $user['email'],
            'campaign' => $tarif['getresp_compain']
        ];

        try {
            Getresponse::addContact($data);
        } catch (\Exception $e) {
            MailsResponse::create($data);
            Log::error('GETRESPONSE ERROR'.($tarif['getresp_compain'] ?? ''),[$e->getMessage(),$e->getCode()]);
        }
    }


}
