<?php

namespace App\Http\Controllers;

use App\Helpers\Getresponse;
use App\Http\Requests\OrderRequest;
use App\Model\Orders;

class OrdersController extends Controller
{
    /**
     * @param OrderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function mail(OrderRequest $request)
    {
        $post = $request->except('_token');
        $post['phone'] = str_replace(' ', '', $post['phone']);

        Orders::create($request->except('_token','action'));

        Getresponse::addContact($post);

        return redirect()->route('thanks');
    }

    public function thanks()
    {
        return view('thanks');
    }
}
