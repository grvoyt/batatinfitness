<?php

namespace App\Http\Controllers;

use App\Model\UserInfo;
use Illuminate\Http\Request;

class ReferalController extends Controller
{
    /**
     * @param $link
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function trening($link) {
        $parent_id = UserInfo::where('referal_link',$link)->value('user_id');
        return redirect()->route('trening')
            ->withCookie(
                \cookie()->make('parent_id',$parent_id, 43200)
            );
    }

    /**
     * @param $link
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fitness($link)
    {
        $parent_id = UserInfo::where('referal_link',$link)->value('user_id');
        return redirect()->route('fitness')
            ->withCookie(
                \cookie()->make('parent_id',$parent_id, 43200)
            );
    }
}
