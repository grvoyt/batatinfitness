<?php

namespace App\Http\Controllers;

use App\Events\BalanceChange;
use App\Events\PaidCourceEvent;
use App\Model\Balance;
use App\Model\Payments;
use App\Payment\Fondy;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function get(Request $request)
    {
        return redirect()->route('trening');
        return Fondy::makeCheckout(1000,'Фитнес игра');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function post(Request $request)
    {
        $post = $request->all();

        if(!Fondy::check($post)) return redirect()->route('payment.fail');

        $redirect_url = $this->checkPayment($post);
        return response()->redirectTo($redirect_url);
    }

    /**
     * route view if payment ok
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ok()
    {
        return view('payment.ok');
    }

    /**
     * route view if payment wait
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wait()
    {
        return view('payment.wait');
    }

    /**
     * route view if payment fails
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fail() {
        return view('payment.fail');
    }

    /**
     * check payment status
     * @param $post
     * @return string
     */
    protected function checkPayment($post)
    {
        switch($post['order_status']) {
            case 'created':
            case 'processing':
                return route('payment.wait');
                break;

            case 'approved':
                return $this->addPayment($post);
                break;

            case 'declined':
                return route('payment.fail');
                break;
        }
    }

    /**
     * add payment
     * @param $post
     * @return string
     */
    protected function addPayment($post)
    {
        $this->updateBalance(
            $this->updatePayment($post)
        );
        return route('payment.ok');
    }

    /**
     * update payment status
     * @param $post
     * @return array
     */
    protected function updatePayment($post)
    {
        $payment = Payments::where('payment_id', $post['payment_id'])->first();
        $payment->order_id = $post['order_id'];
        $payment->order_time = Carbon::parse($post['order_time']);
        $payment->user_card = $post['masked_card'];
        $payment->status = 1;
        $payment->amount = $post['amount'] / 100;
        $payment->save();

        return [
            'user_id' => $payment->user_id,
            'amount'  => $post['amount'] / 100
        ];
    }

    /**
     * emmit event update balance
     * @param $found
     */
    protected function updateBalance($found)
    {
        event(
            new PaidCourceEvent(
                $found['user_id'],
                $found['amount'] * 10
            )
        );

        event(
            new BalanceChange($found['user_id'])
        );
    }


}
