<?php


namespace App\Http\Traits;


use App\Model\UserInfo;
use Exception;
use Illuminate\Support\Str;

trait GenerateReferal
{
    /**
     * @param int $user_id
     * @return bool
     */
    public function generateReferal(int $user_id)
    {
        $status = 1;
        while( $status ) {
            try {
                $referal = Str::random(8);
                $referal = Str::upper($referal);
                UserInfo::create([
                    'user_id' => $user_id,
                    'referal_link' => $referal,
                    'referal2' => $referal,
                    'promocode' => $referal
                ]);
                $status = 0;
            } catch (Exception $e) {
                continue;
            }
        }
        unset($status);
        return true;
    }
}
