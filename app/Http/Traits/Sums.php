<?php


namespace App\Http\Traits;


use App\Helpers\Getresponse;
use App\Mail\SendInfoAdmin;
use App\Model\MailsResponse;
use App\Payment\Fondy;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

trait Sums
{
    /**
     * @param $tarif
     * @throws \Cloudipsp\Exception\ApiException
     */
    protected function makeCheckout($tarif,$user)
    {
        return Fondy::makeCheckout($tarif['price'], $tarif['title'], $user['email'], $tarif['currency']);
    }

    protected function SendToGetResponse($tarif,$user)
    {
        $data = [
            'name' => $user['name'] ?? '',
            'email' => $user['email'],
            'campaign' => $tarif['getresp_compain']
        ];

        try {
            Getresponse::addContact($data);
        } catch (\Exception $e) {
            MailsResponse::create($data);
            Log::error('GETRESPONSE ERROR'.($tarif['getresp_compain'] ?? ''),[$e->getMessage(),$e->getCode()]);
        }
    }

    protected function makeUserArray($data)
    {
        return [
            'name' => $data['name'] ?? '',
            'phone' => $data['phone'],
            'email' => $data['email'],
        ];
    }

    protected function getSum($tarif,$compain = null)
    {
        $packets = [
            'ls' => 'Быстрый старт',
            'st' => 'STANDART',
            'g' => 'GOLD',
            's' => 'SILVER',
            'p' => "PLATINA"
        ];
        $type = explode('_',$tarif);
        $price = $type[1];

        $compain_name = !isset($type[2]) ? 'av-2_sale' : 'av-2_sale_chast';
        $getresp_compain = $compain ?? config('services.getresponse.'.$compain_name);

        $title = $packets[$type[0]];
        if(isset($type[2])) $title .= ' рассрочка';
        $currency = 'USD';
        return compact('price','getresp_compain','title','currency');
    }

    protected function sendEmailToAdmin($email,$user,$tarif,$subject)
    {
        try {
            Mail::to( $email )
                ->send(new SendInfoAdmin($user,$tarif,$subject));
        } catch (\Exception $e) {
            Log::error('ADMIN EMAIL', [$e->getMessage()]);
        }
    }
}
