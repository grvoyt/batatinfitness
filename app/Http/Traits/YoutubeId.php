<?php
/**
 * Created by PhpStorm.
 * User: Georgiy Voytkevich
 * Date: 30.05.2019
 * Time: 23:17
 */

namespace App\Http\Traits;


trait YoutubeId
{
    public function getYoutubeId($url)
    {
        $regExp = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
        preg_match($regExp,$url,$match);
		return ( $match && strlen($match[7])==11) ? $match[7] : false;
    }
}