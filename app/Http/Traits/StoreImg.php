<?php


namespace App\Http\Traits;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

trait StoreImg
{
    public function storeImg($request, $filepath = 'avatars', $filename = 'avatar') {
        $requestFile = $request->file($filename);
        $fileName = md5( Auth::user()->name.Auth::user()->id);
        $extension = pathinfo($requestFile->getClientOriginalName(), PATHINFO_EXTENSION);
        $file = $fileName.'.'.$extension;
        $requestFile->move(public_path($filepath), $file);
        return $filepath.'/'.$file;
    }
}
