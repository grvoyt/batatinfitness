<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInfoAdmin extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $email;
    public $tarif;
    public $phone;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data, array $tarif,$subject)
    {
        $this->subject = $subject ?? 'Купили тариф '.$tarif['title'];
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->tarif = $tarif['title'];
        $this->phone = $data['phone'] ?? '';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admin');
    }
}
