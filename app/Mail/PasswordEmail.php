<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Session;

class PasswordEmail extends Mailable
{
    use SerializesModels;

    public $user;
    public $password = 'test';

    public $subject = 'Успешная Регистрация! Бонус внутри:)';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$password = '')
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->password == '') {
            $this->password = Session::get('password','test');
        }

        return $this->view('emails.password');
    }
}
