<?php

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

if( !function_exists('is_active') ) {
    function is_active($route, $name = 'active') {
        if( Str::contains($route,'.') ) {
            $path = Route::currentRouteName();
            $pos = Str::contains($path,$route) ? 1 : 0;
        } else {
            $path = Request::path();
            $pos = strpos($path,$route);
        }
        return !!$pos ? $name : '';
    }
}

if( !function_exists('d_time') ) {
    function d_time() {
        return env('APP_DEBUG') ? time() : '';
    }
}
