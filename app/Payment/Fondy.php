<?php


namespace App\Payment;


use App\Model\Payments;
use Cloudipsp\Checkout;
use Cloudipsp\Configuration;
use App\Helpers\Signature;
use Cloudipsp\Subscription;
use Illuminate\Support\Carbon;

class Fondy
{
    /**
     * @param $amount
     * @param string $title
     * @param $user_id
     * @param string $currency
     * @throws \Cloudipsp\Exception\ApiException
     */
    public static function makeCheckout($amount,$title = '',$user_id, $currency = 'RUB')
    {
        Configuration::setMerchantId( config('services.fondy.merchant_id') );
        Configuration::setSecretKey( config('services.fondy.secret_key') );

        $data = [
            'currency' => $currency,
            'amount' => $amount * 100,
            'response_url' => route('payment.check'),
        ];

        if($title != '') $data['order_desc'] = $title;

        $url = Checkout::url($data);
        $data_url = $url->getData();

        Payments::create([
            'user_id' => $user_id,
            'payment_id' => $data_url['payment_id'],
            'url' => $data_url['checkout_url']
        ]);

        return $url->toCheckout();
    }

    /**
     * @param $response
     * @return int
     */
    public static function check($response)
    {
        Signature::merchant( config('services.fondy.merchant_id') );
        Signature::password( config('services.fondy.secret_key') );
        return Signature::check($response) ? 1 : 0;
    }

    /**
     * @param int $amount
     * @param string $title
     * @param int $user_id
     * @param string $currency
     * @param int $days
     * @throws \Cloudipsp\Exception\ApiException
     */
    public static function makeRecurent(int $amount,string $title = '', int $user_id, $currency = 'RUB', $days = 30)
    {
        Configuration::setMerchantId( config('services.fondy.merchant_id') );
        Configuration::setSecretKey( config('services.fondy.secret_key') );
        Configuration::setApiVersion('2.0');

        $countingAmount = $amount * 100;

        $data = [
            'currency' => $currency,
            'amount' => $countingAmount,
            'recurring_data' => [
                'start_time' => Carbon::now()->format('Y-m-d'),
                'amount' => $countingAmount,
                'every' => $days,
                'period' => 'day',
                'state' => 'y',
                'readonly' => 'y'
            ]
        ];

        if($title != '') $data['order_desc'] = $title;

        $url = Subscription::url($data);
        $data_url = $url->getData();

        Payments::create([
            'user_id' => $user_id,
            'payment_id' => $data_url['payment_id'],
            'url' => $data_url['checkout_url']
        ]);

        return $url->toCheckout();
    }
}
