<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PointsUsers extends Model
{
    protected $fillable = ['task_id','user_id','ball'];
}
