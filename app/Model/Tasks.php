<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    protected $table = 'tasks';
    protected $fillable = [
        'is_opened',
        'name',
        'video_url',
        'text',
    ];

    public function scopeWithAnswers($query,$user_id)
    {
        return $query->select([
            'tasks.*',
            'ta.status as is_done'
        ])->where('user_id',$user_id)
            ->orWhere('is_opened',1)
            ->leftJoin('tasks_answers as ta','ta.task_id','tasks.id')
            ->get();
    }

    public function scopeIsOpened($query)
    {
        return $query->where('is_opened',1)->get();
    }
}
