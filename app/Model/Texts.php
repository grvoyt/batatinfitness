<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Texts extends Model
{
    protected $table = 'texts';
    protected $fillable = [
        'key',
        'lang',
        'text',
    ];
    public $timestamps = false;
}
