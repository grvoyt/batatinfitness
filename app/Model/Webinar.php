<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Webinar extends Model
{
    protected $fillable = ['title','link'];
}
