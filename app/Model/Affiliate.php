<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Affiliate extends Model
{
    protected $table = 'affiliate';
    protected $fillable = [
        'user_id',
        'affiliate_user_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeAffiliateCounts($query) {
        return $query->select(
                DB::raw('count(user_id) as count'),
                'affiliate.user_id',
                'u.name',
                'u.email',
                'u.phone',
                'u.id'
                )
                 ->groupBy('affiliate.user_id')
                 ->leftJoin('users as u','u.id','=','affiliate.user_id')->get();

    }
}
