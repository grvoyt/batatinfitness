<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Points extends Model
{
    protected $fillable = ['task_id','value'];
}
