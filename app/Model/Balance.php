<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Balance extends Model
{
    protected $table = 'balance';
    protected $fillable = [
        'user_id',
        'type',
        'amount',
        'child_id',
        'withdraw_id'
    ];

    public function scopeByUser($query,$user_id)
    {
        $balances = $query->select(DB::raw('SUM(amount) as amount'),'type','child_id')
                    ->where('user_id',$user_id)
                    ->groupBy('type')->get();
        $balance = 0;
        $partners = 0;
        foreach($balances as $bal) {
            if( $bal->type < 200 && $bal->type >= 100 ) {
                $balance += $bal->amount;
                if($bal->type == 101) $partners += $bal->amount;
            }

            if( $bal->type >= 200 ) {
                $balance -= $bal->amount;
            }
        }

        return compact('balance','partners');
    }

    public function scopeNotPaid($query,$user_id)
    {
        $getDB = $query->whereIn('user_id', User::where('parent_id',$user_id)->get('id'))
                     ->select('user_id')
                     ->where('type',100)
                     ->groupBy('user_id')
                     ->get();
        return $getDB->count();
    }

    public function scopeGetFirstFound($query,$partner_id,$user_id)
    {
        return $query->where('user_id',$partner_id)
                     ->where('child_id', $user_id);
    }

    public function withdraw() {
        return $this->hasOne('App\Model\Withdraw','id','withdraw_id');
    }
}
