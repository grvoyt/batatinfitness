<?php

namespace App\Model;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class GameSchedule extends Model
{
    protected $fillable = ['date','title','host','text'];

    public function scopeGetLast($query) {
        return $query->where('date','>', Carbon::now())->orderBy('date','asc')->get();
    }
}
