<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mails extends Model
{
    protected $table = 'mails';
    protected $fillable = [
        'email',
        'user_id',
        'password'
    ];
}
