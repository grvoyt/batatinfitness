<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $table = 'user_info';
    protected $fillable = [
        'user_id',
        'referal_link',
        'referal2',
        'promocode',
        'img',
    ];
}
