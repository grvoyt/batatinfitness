<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MailsResponse extends Model
{
    protected $fillable = ['email','name','status'];
}
