<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BalanceType extends Model
{
    protected $table = 'balance_type';
    protected $fillable = ['ru','us'];
    public $timestamps = false;
}
