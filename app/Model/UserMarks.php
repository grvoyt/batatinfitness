<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserMarks extends Model
{
    protected $table = 'user_marks';
    protected $fillable = [
        'user_id',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
        'other'
    ];
}
