<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $fillable = [
        'user_id',
        'status',
        'amount',
        'message'
    ];

    public function user() {
        return $this->hasOne('App\User','id','user_id');
    }
}
