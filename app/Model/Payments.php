<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $fillable = [
        'user_id',
        'payment_id',
        'status',
        'url',
        'order_id',
        'order_time',
        'user_card',
        'amount',
    ];
}
