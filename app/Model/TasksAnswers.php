<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TasksAnswers extends Model
{
    protected $table = 'tasks_answers';
    protected $fillable = [
        'task_id',
        'user_id',
        'status',
        'answer',
        'img'
    ];

    public function info() {
        return $this->belongsTo('App\Model\Tasks','task_id','id');
    }
}
