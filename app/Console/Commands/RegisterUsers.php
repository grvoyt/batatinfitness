<?php

namespace App\Console\Commands;

use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class RegisterUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:register {show=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register users by email.txt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = 0;
        $show = (int) $this->argument('show');

        try {
            $file = Storage::disk('local')->get('emails.txt');
        } catch(Exception $e) {
            $this->comment('Error: no file at '.storage_path('app'));
            return false;
        }

        $users = $this->transformFile($file);
        if($show) {
            dd($users);
            return true;
        }
        
        foreach($users as $user) {
            $register = new RegisterController();
            $regUser = $register->registerConsole($user);
            if(!$regUser) continue;
            $count++;
            $password = Session::get('password');
            $this->comment("User registered id:$regUser->id $regUser->name $regUser->email $password");
        }
        $this->comment("Users registered: $count");
        return true;
    }

    /**
     * Get from file users and transform to array
     * @param $file
     * @return array $users
     */
    protected function transformFile($file)
    {
        $lines = explode(PHP_EOL,$file);
        $users = [];
        foreach($lines as $line) {
            $data = explode(",",$line);
            $users[] = [
                'name' => $data[0],
                'email' => $data[1]
            ];
        }
        return $users;
    }
}
