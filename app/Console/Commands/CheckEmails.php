<?php

namespace App\Console\Commands;

use App\Mail\PasswordEmail;
use App\Model\Mails;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CheckEmails extends Command
{
    protected $show;
    protected $count;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:check {show=0} {count=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resend emails if is erros';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $counter = 0;
        $this->show = $this->argument('show');
        $this->count = ((int)$this->argument('count') !== 0) ? (int) $this->argument('count') : 'all';

        if($this->count !== 'all'){
            $emails = Mails::where('status',0)->limit(1)->get();
        } else {
            $emails = Mails::where('status',0)->get();
        }

        foreach($emails as $email) {
            try {
                $user = User::find($email->user_id);
                Mail::to($user->email)
                    ->send(new PasswordEmail($user, $email->password));
                $this->messageToConsole('Email sended '.$email->email);
            } catch ( \Exception $e ) {
                Log::error('CRON EMAIL NOT SEND',[$e->getCode(),$e->getMessage()]);
                $this->messageToConsole('Email NOT sended '.$email->email);
                $this->messageToConsole('ERRORS '.$e->getMessage());
                $this->messageToConsole('=======');
                continue;
            }
            $email->status = 1;
            $email->save();
            $counter++;
        }
        $this->messageToConsole('Emails sended '.$counter);
    }

    protected function messageToConsole($message)
    {
        if(!!$this->show) {
            $this->comment($message);
        }
    }
}
