<?php

namespace App\Console\Commands;

use App\User;
use App\Mail\PasswordEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class TestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:test {email=grvoyt@ya.ru}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send test after register email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');

        try {
            $user = User::where('email',$email)->first();
            Mail::to($user->email)
                ->send(new PasswordEmail($user, '11111'));
            $this->messageToConsole('Email sended '.$email);
        } catch ( \Exception $e ) {
            Log::error('CRON EMAIL NOT SEND',[$e->getCode(),$e->getMessage()]);
            $this->messageToConsole('Email NOT sended '.$email);
            $this->messageToConsole('ERRORS '.$e->getMessage());
            $this->messageToConsole('=======');
        }
    }

    protected function messageToConsole($message)
    {
        if(1) {
            $this->comment($message);
        }
    }
}
