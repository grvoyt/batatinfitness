<?php

namespace App\Console\Commands;

use App\Helpers\Getresponse;
use App\Model\MailsResponse;
use Illuminate\Console\Command;

class CheckGetResponses extends Command
{
    protected $show;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resp:email {show=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send to get response if error';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $counter = 0;
        $this->show = (int) $this->argument('show');

        $emails = MailsResponse::where('status',0)->get();

        if(count($emails) == 0) {
            $this->messageToConsole('GetResponseEmails resended '.$counter);
            return;
        }

        foreach($emails as $email) {
            try {
                Getresponse::addContact([
                   'name'  => $email->name,
                   'email' => $email->email
                ]);
                $this->messageToConsole('GetResponseEmails resended '.$email->email);
            } catch ( \Exception $e ) {
                Log::error('CRON RESP_EMAIL NOT SEND',[$e->getCode(),$e->getMessage()]);
                $this->messageToConsole('GetResponseEmails NOT resended '.$email->email);
                continue;
            }
            $email->status = 1;
            $email->save();
            $counter++;
        }
        $this->messageToConsole('GetResponseEmails resended '.$counter);
    }

    protected function messageToConsole($message)
    {
        if(!!$this->show) {
            $this->comment($message);
        }
    }
}
