<?php

namespace App\Listeners;

use App\Events\BalanceChange;
use App\Model\Balance;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ReCacheBalance
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BalanceChange  $event
     * @return void
     */
    public function handle(BalanceChange $event)
    {
        //recache balance user
        $getBalance = Balance::byUser($event->user_id);
        Cache::put('balance_'.$event->user_id, $getBalance['balance'],86400);

        //recache parent user if exists
        if( ($parent_id = User::where('id', $event->user_id)->value('parent_id')) == 0 ) return false;
        $getParentBalance = Balance::byUser($parent_id);
        Cache::put('balance_'.$parent_id, $getParentBalance['balance'],86400);
    }
}
