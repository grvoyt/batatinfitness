<?php

namespace App\Listeners;

use App\Events\PaidCourceEvent;
use App\Model\Balance;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefoundReferal
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaidCourceEvent  $event
     * @return void
     */
    public function handle(PaidCourceEvent $event)
    {
        $user_id = $event->user_id;
        $amount = $event->amount;

        $user = User::find($user_id);

        //add user amount to balance
        Balance::create([
            'user_id' => $user_id,
            'type' => 100,
            'amount' => $amount
        ]);

        if( $user->parent_id == 0 ) return true;

        //add partner referal to balance
        $persent = 20;
        if( !Balance::getFirstFound($user->parent_id, $user->id)->first() ) $persent = 40;

        Balance::create([
            'user_id' => $user->parent_id,
            'type' => 101,
            'amount' => $amount * ($persent/100),
            'child_id' => $user->id
        ]);
    }
}
