<?php

namespace App\Listeners;

use App\Events\TasksConfirm;
use App\Model\Points;
use App\Model\PointsUsers;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PointsAdd
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TasksConfirm  $event
     * @return void
     */
    public function handle(TasksConfirm $event)
    {
        $bal = Points::where('task_id', $event->task_id)->value('value');
        PointsUsers::create([
            'task_id' => $event->task_id,
            'user_id' => $event->user_id,
            'ball'    => $bal
        ]);
    }
}
