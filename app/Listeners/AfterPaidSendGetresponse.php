<?php

namespace App\Listeners;

use App\Events\PaidCourceEvent;
use App\Helpers\Getresponse;
use App\Model\MailsResponse;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AfterPaidSendGetresponse
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaidCourceEvent  $event
     * @return void
     */
    public function handle(PaidCourceEvent $event)
    {
        $user = User::find($event->user_id);

        $data = [
            'name' => $user->name ?? '',
            'email' => $user->email,
            'campaign' => config('services.getresponse.coaching_paid')
        ];

        try {
            Getresponse::addContact($data);
        } catch (\Exception $e) {
            MailsResponse::create($data);
            Log::error('GETRESPONSE ERROR',[$e->getMessage(),$e->getCode()]);
        }
    }
}
