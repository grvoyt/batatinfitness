<?php

namespace App\Listeners;

use App\Model\UserMarks;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Registered;
use Illuminate\Support\Facades\Cookie;

class SaveUserMarks
{
    protected $utms = [
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
    ];
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $user = $event->user;
        $marks = json_decode( Cookie::get('marks'), true );

        $result = [
            'user_id' => $user->id
        ];

        if( !empty($marks) ) {
            $other = [];

            foreach ($marks as $k => $v) {
                in_array($k, $this->utms) ? $result[$k] = $v : $other[] = $k.'='.$v;
            }

            if (count($other) >= 1) $result['other'] = implode('&', $other);
        }

        UserMarks::create($result);
    }
}
