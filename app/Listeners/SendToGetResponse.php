<?php

namespace App\Listeners;

use App\Helpers\Getresponse;
use App\Model\MailsResponse;
use App\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendToGetResponse
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $data = [
            'name' => $event->user->name ?? '',
            'email' => $event->user->email,
            'campaign' => $event->getresponse_campain
        ];

        try {
            Getresponse::addContact($data);
        } catch (\Exception $e) {
            MailsResponse::create($data);
            Log::error('GETRESPONSE ERROR'.($event->getresponse_campain ?? ''),[$e->getMessage(),$e->getCode()]);
        }
    }
}
