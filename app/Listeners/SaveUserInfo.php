<?php

namespace App\Listeners;

use App\Http\Traits\GenerateReferal;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Registered;

class SaveUserInfo
{
    use GenerateReferal;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $this->generateReferal($event->user->id);
    }
}
