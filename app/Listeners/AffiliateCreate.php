<?php

namespace App\Listeners;

use App\Model\Affiliate;
use App\User;
use App\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cookie;

class AffiliateCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $user = $event->user;
        if(!Cookie::has('affiliate_parent')) return false;

        Affiliate::create([
            'user_id' => User::where('id', Cookie::get('affiliate_parent'))->value('id'),
            'affiliate_user_id' => $user->id
        ]);
    }
}
