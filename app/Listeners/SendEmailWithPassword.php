<?php

namespace App\Listeners;

use App\Mail\PasswordEmail;
use App\Model\Mails;
use App\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class SendEmailWithPassword
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        if(Session::get('main_reg',0)) {
            try {
                Mail::to($event->user->email)
                    ->send(new PasswordEmail($event->user));
            } catch(\Exception $e) {
                Mails::create([
                    'email' => $event->user->email,
                    'user_id' => $event->user->id,
                    'password'  => Session::pull('password','test')
                ]);
            }
        }

    }
}
