<?php

namespace App\Providers;

use App\Events\PaidCourceEvent;
use App\Listeners\SaveUserInfo;
use App\Listeners\SaveUserMarks;
use App\Events\Registered;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            //SendEmailVerificationNotification::class,
            SaveUserMarks::class,
            SaveUserInfo::class,
            'App\Listeners\SendEmailWithPassword',
            'App\Listeners\SendToGetResponse',
        ],
        PaidCourceEvent::class => [
            'App\Listeners\RefoundReferal',
        ],
        'App\Events\TasksConfirm' => [
            'App\Listeners\PointsAdd',
        ],
        'App\Events\BalanceChange' => [
            'App\Listeners\ReCacheBalance',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
