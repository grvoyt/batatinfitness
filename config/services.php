<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'api_key' => env('MAILGUN_KEY'),
        'api_url' => env('MAILGUN_URL'),
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'fondy' => [
        'secret_key' => env('FONDY_SECRET','test'),
        'merchant_id' => env('FONDY_TEST_ID',1396424)
    ],

    'getresponse' => [
        'api_key' => env('GETRESPONSE_API'),
        'campaignId' => '8tzgS',
        'av1_before_16' => 'y9THz',
        'av1_after_16' => 'y9Tdk',
        'coaching_paid' => 'y96Eh',
        'webinar_sale' => 'y9Clg',
        'av-2_sale'    => 'ySWTS',
        'av-2_sale_chast' => 'ySW8c',
        'av-3_sale' => 'yUWts',
        'av-4_sale' => 'yUWDB',
        'wp_before_16' => 'ybr2j',
        'wp_after_16' => 'ybr0d',
        'w_thanks_bot' => 'ybizS'
    ]

];
