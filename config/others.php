<?php
/**
 * Other configs, dates, webinars
 */

return [
    'dates' => [
        'game' => '17 июня',
        'online' => '24 июня',
        'webinar' => '27 июня'
    ],
    'contacts' => [
        'email' => 'batatinfitness@gmail.com'
    ]
];